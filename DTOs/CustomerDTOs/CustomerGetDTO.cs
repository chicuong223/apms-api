﻿namespace DTOs.CustomerDTOs;

public record CustomerGetDTO
{
    public CustomerGetDTO()
    {
    }
    public string Id { get; init; }
    public string PhoneNumber { get; init; }
    public string FullName { get; init; }
    public decimal AccountBalance { get; init; }
}