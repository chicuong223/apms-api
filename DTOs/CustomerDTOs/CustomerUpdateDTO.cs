﻿using System.ComponentModel.DataAnnotations;

namespace DTOs.CustomerDTOs
{
    public record CustomerUpdateDTO(
        [RegularExpression("^\\d{10,14}$")] string PhoneNumber,
        [StringLength(200)] string FullName
        // string Password
        );
}