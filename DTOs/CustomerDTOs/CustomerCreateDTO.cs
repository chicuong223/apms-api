﻿using System.ComponentModel.DataAnnotations;

namespace DTOs.CustomerDTOs
{
    public record CustomerCreateDTO(
        [RegularExpression("^\\d{10,14}$")][Required] string PhoneNumber,
        [StringLength(200)][Required] string FullName,
        [Required] string Password
        );
}