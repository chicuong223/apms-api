namespace DTOs.AccountDTOs;

public record AccountGetMinimumDTO
{
    public string Id { get; init; }
    public string PhoneNumber { get; init; }
    public string RoleTitle { get; init; }
    public bool CanEditTicket { get; init; }
    public bool CanEditCarPark { get; init; }
}