using System.ComponentModel.DataAnnotations;

namespace DTOs.AccountDTOs;

public record AccountCreateDTO(
    [StringLength(32, MinimumLength = 8)] string Password,
    [RegularExpression("^\\d{10,14}$")] string PhoneNumber,
    int RoleId
);