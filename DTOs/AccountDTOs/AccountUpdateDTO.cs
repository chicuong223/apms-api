using System.ComponentModel.DataAnnotations;

namespace DTOs.AccountDTOs;

public record AccountUpdateDTO(
    [Required] String Id,
    [Required][RegularExpression("^\\d{10,14}$")] string PhoneNumber,
    // [StringLength(64)] string Password,
    int RoleId
);