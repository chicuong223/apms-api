﻿using DTOs.CarParkDTOs;

namespace DTOs.AccountDTOs
{
    public record AccountGetDTO
    {
        public string Id { get; init; }
        public string PhoneNumber { get; init; }
        public string RoleTitle { get; init; }
        public int RoleId { get; init; }
        public bool Active { get; init; }
        // public UserInfoDTO Info { get; init; }
        public Guid? CarParkId { get; init; }
        public CarParkNameAddressDTO CarPark { get; init; }
        public bool CanEditTicket { get; init; }
        public bool CanEditCarPark { get; init; }
        public AccountGetDTO()
        {
        }
    }
}