﻿namespace DTOs.AccountDTOs
{
    public record UserInfoDTO(
        string FullName,
        decimal AccountBalance);
}