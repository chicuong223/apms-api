using System.ComponentModel.DataAnnotations;

namespace DTOs.AccountDTOs;

public record ForgotPasswordDTO(
    [RegularExpression("^\\d{10,14}$")] string PhoneNumber,
    [StringLength(32, MinimumLength = 8)] string Password
);