namespace DTOs.AccountDTOs;

public record AccountLoginDTO(
    string Id,
    string Password
);