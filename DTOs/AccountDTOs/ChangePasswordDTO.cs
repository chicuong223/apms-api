using System.ComponentModel.DataAnnotations;

namespace DTOs.AccountDTOs;

public record ChangePasswordDTO(
    [Required] string OldPassword,
    [Required][StringLength(32, MinimumLength = 8)] string NewPassword,
    [Required] string ConfirmPassword
);