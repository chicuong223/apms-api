using System.ComponentModel.DataAnnotations;

namespace DTOs.AccountDTOs;

public record CustomerLoginDTO(
    [RegularExpression("^\\d{10,14}$")] string PhoneNumber,
    string Password
);