using System.ComponentModel.DataAnnotations;

namespace DTOs.CarParkDTOs;

public record CarParkCreateDTO(
    string Name,
    [Required] string AddressNumber,
    [Required] string Street,
    [Required] string Ward,
    [Required] string District,
    [Required] string City,
    [Required][RegularExpression("^[0-9]{10,14}$")] string PhoneNumber,
    [Required] double Latitude,
    [Required] double Longitude,
    [Required][Range(1, int.MaxValue)] int MaxCapacity,
    [Required] int ProvinceId,
    string OwnerPhoneNumber,
    string StaffPhoneNumber
);