namespace DTOs.CarParkDTOs;

public record CarParkGetWithDistanceDTO : CarParkGetDTO
{
    public CarParkGetWithDistanceDTO() : base()
    {
    }

    public double Distance { get; set; }
    // public Guid Id { get; init; }
    // public string Name { get; init; }
    // public string AddressNumber { get; init; }
    // public string Ward { get; init; }
    // public string District { get; init; }
    // public string City { get; init; }
    // public int ProvinceId { get; init; }
    // public string PhoneNumber { get; init; }
    // public double Latitude { get; init; }
    // public double Longitude { get; init; }
    // public int AvailableSlotsCount { get; init; }
    // public CarParkStatus Status { get; init; }
    // public PriceTableGetDTO PriceTable { get; init; }
    // public ICollection<AccountGetMinimumDTO> Accounts { get; init; }
}