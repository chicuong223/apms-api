using System.ComponentModel.DataAnnotations;

namespace DTOs.CarParkDTOs;

public record SystemConfigUpdateDTO(
    [Required] decimal ParkingFeePerHour,
    [Required] decimal ReservationFee
);