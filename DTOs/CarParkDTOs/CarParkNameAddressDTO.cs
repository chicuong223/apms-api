namespace DTOs.CarParkDTOs;

public record CarParkNameAddressDTO
{
    public CarParkNameAddressDTO()
    {
    }
    public string Name { get; init; }
    public string Street { get; init; }
    public string AddressNumber { get; init; }
    public string Ward { get; init; }
    public string District { get; init; }
    public string City { get; init; }
    public int ProvinceId { get; init; }
    public string PhoneNumber { get; set; }
}