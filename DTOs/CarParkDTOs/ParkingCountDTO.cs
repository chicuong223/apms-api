using DTOs.PriceTableDTO;
using Utilities.Enums;

namespace DTOs.CarParkDTOs;

public record ParkingCountDTO
{
    public ParkingCountDTO()
    {
    }
    public Guid Id { get; init; }
    public string Name { get; init; }
    // public string Address { get; init; }
    public string Street { get; init; }
    public string AddressNumber { get; init; }
    public string Ward { get; init; }
    public string District { get; init; }
    public string City { get; init; }
    public int ProvinceId { get; init; }
    public string PhoneNumber { get; init; }
    public double Latitude { get; init; }
    public double Longitude { get; init; }
    public int AvailableSlotsCount { get; init; }
    public CarParkStatus Status { get; init; }
    public PriceTableGetDTO PriceTable { get; init; }
    public int VisitCount { get; set; }
    public DateTime? LastVisited { get; set; }
}