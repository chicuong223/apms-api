using System.ComponentModel.DataAnnotations;

namespace DTOs.CarParkDTOs;

public record CarParkUpdateDTO(
    Guid Id,
    string Name,
    string AddressNumber,
    string Street,
    string Ward,
    string District,
    string City,
    [RegularExpression("^[0-9]{10,14}$")] string PhoneNumber,
    double? Latitude,
    double? Longitude,
    int? ProvinceId,
    [RegularExpression("^[0-9]{10,14}$")] string OwnerPhoneNumber,
    [RegularExpression("^[0-9]{10,14}$")] string StaffPhoneNumber,
    [Range(0, int.MaxValue)] int? MaxCapacity
);