namespace DTOs.TicketDTOs;

public record CheckinDTO(
    string PlateNumber,
    string PlateNumberImageUrl,
    Guid CarParkId
);