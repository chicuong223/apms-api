using System.ComponentModel.DataAnnotations;

namespace DTOs.TicketDTOs;

public record TicketManualCreateDTO(
    [Required][RegularExpression("^\\d{10,14}$")] string PhoneNumber,
    [Required] string PlateNumber,
    [Required] DateTime StartTime,
    decimal? TotalFee,
    DateTime? EndTime
);