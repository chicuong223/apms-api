﻿namespace DTOs.TicketDTOs;
public record CheckoutDTO(
    Guid CarParkId,
    string PlateNumber,
    string PicOutUrl
    );