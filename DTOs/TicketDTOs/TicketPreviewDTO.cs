using DTOs.PriceTableDTO;

namespace DTOs.TicketDTOs;

public record TicketPreviewDTO
{
    public TicketPreviewDTO()
    {
    }

    public string PhoneNumber { get; init; }
    public DateTime ArriveTime { get; init; }
    public string PlateNumber { get; init; }
    public decimal ReservationFee { get; init; }
    public decimal ReservationFeePercentage { get; set; }
    public decimal? FeePerHour { get; set; }
    public string CarParkName { get; init; }
    public string CarParkAddress { get; init; }
    public decimal AccountBalance { get; set; }
    public IEnumerable<PriceTableDetailDTO> PriceTable { get; init; }
}