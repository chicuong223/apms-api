﻿namespace DTOs.TicketDTOs
{
    public record CheckinWithAccountDTO(
        string AccountId,
        string PlateNumber,
        string PicUrl,
        Guid CarParkId);
}