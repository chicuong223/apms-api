namespace DTOs.TicketDTOs;

public record CheckoutManualDTO(
    string PlateNumber,
    string PicOutUrl
);