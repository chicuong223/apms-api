﻿using System.ComponentModel.DataAnnotations;

namespace DTOs.TicketDTOs;

public record TicketCreateDTO(
    [Required(ErrorMessage = "Vehicle's plate number is required")]
    string PlateNumber,
    Guid CarParkId,
    [Required(ErrorMessage = "Arrive time is required")]
    DateTime ArriveTime
    );