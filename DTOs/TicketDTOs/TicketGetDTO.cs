using DTOs.CarParkDTOs;
using DTOs.PriceTableDTO;
using Utilities.Enums;

namespace DTOs.TicketDTOs;

public record TicketGetDTO
{
    public TicketGetDTO()
    {
    }
    public Guid Id { get; init; }
    public DateTime? StartTime { get; init; }
    public DateTime? EndTime { get; init; }
    public decimal ReservationFee { get; set; }
    public decimal OverdueFee { get; set; }
    public DateTime? BookTime { get; init; }
    public DateTime? ArriveTime { get; init; }
    public decimal TotalFee { get; init; }
    public decimal? FeePerHour { get; init; }
    public string PicInUrl { get; init; }
    public string PicOutUrl { get; init; }
    public Guid CarParkId { get; init; }
    public string PlateNumber { get; init; }
    public string AccountId { get; init; }
    public string PhoneNumber { get; init; }
    public string FullName { get; init; }
    public TicketStatus Status { get; init; }
    public CarParkNameAddressDTO CarPark { get; init; }
    public IEnumerable<PriceTableDetailDTO> PriceTable { get; init; }
}