using Utilities.Enums;

namespace DTOs.TransactionDTOs;

public record TransactionGetDTO
{
    public TransactionGetDTO()
    {
    }

    public Guid Id { get; init; }
    public decimal Amount { get; init; }
    public DateTime Time { get; init; }
    public string PhoneNumber { get; init; }
    public string FullName { get; init; }
    public string AccountId { get; init; }
    public Guid? TicketId { get; init; }
    public TransactionType TransactionType { get; init; }
}