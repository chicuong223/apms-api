namespace DTOs.TransactionDTOs;

public record TransactionCreateDTO(
    decimal Amount
);