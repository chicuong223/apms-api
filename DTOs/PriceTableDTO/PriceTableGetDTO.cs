namespace DTOs.PriceTableDTO;

public record PriceTableGetDTO
{
    public Guid CarParkId { get; set; }
    public decimal ReservationFeePercentage { get; init; }
    public decimal FeePerHour { get; set; }
    public DateTime LastModified { get; set; }
    public ICollection<PriceTableDetailDTO> Details { get; set; }
}