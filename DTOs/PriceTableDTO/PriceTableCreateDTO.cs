using System.ComponentModel.DataAnnotations;

namespace DTOs.PriceTableDTO;

public record PriceTableCreateDTO
(
    [Range(0, 100)]
    decimal ReservationFeePercentage,
    [Range(0, int.MaxValue)]
    decimal FeePerHour,
    ICollection<PriceTableDetailCreateDTO> Details
);