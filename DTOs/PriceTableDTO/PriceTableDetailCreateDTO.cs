using System.ComponentModel.DataAnnotations;

namespace DTOs.PriceTableDTO;

public record PriceTableDetailCreateDTO(
    [Required(ErrorMessage = "Starting time is required")][RegularExpression("^([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$")] string FromTime,
    [Required(ErrorMessage = "Ending time is required")][RegularExpression("^([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$")] string ToTime,
    decimal Fee
);