using System.ComponentModel.DataAnnotations;

namespace DTOs.FeedbackDTOs;

public record FeedbackCreateDTO(
    [Required(ErrorMessage = "Description is required!")] string Description
);