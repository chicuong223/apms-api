namespace DTOs.FeedbackDTOs;

public record FeedbackUpdateDTO(Guid Id, string Description) : FeedbackCreateDTO(Description);