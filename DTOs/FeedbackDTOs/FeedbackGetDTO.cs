namespace DTOs.FeedbackDTOs;

public record FeedbackGetDTO
{
    public FeedbackGetDTO()
    {
    }
    public Guid Id { get; init; }
    public string Description { get; init; }
    public DateTime Time { get; init; }
    public string PhoneNumber { get; set; }
}