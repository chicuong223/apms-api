namespace DTOs.StatisticDTOs;

public record BookingStatisticDTO(
    Guid CarParkId,
    string Name,
    string Address,
    int TotalBookingsCount,
    int ArrivedBookingsCount
);