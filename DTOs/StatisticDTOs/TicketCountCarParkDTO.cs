namespace DTOs.StatisticDTOs;

public record TicketCountCarParkDTO(
    Guid CarParkId,
    string Name,
    string Address,
    int TicketsCount
);