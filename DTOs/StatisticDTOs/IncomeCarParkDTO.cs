namespace DTOs.StatisticDTOs;

public record IncomeCarParkDTO(
    Guid CarParkId,
    string Name,
    string Address,
    decimal Income
);