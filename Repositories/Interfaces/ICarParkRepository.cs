using Models;

namespace Repositories.Interfaces;

public interface ICarParkRepository
{
    IQueryable<CarPark> GetCarParks(bool includeConfig = false, bool trackChanges = false);

    Task<CarPark> GetCarParkByIdAsync(Guid id, bool includeConfig = false, bool trackChanges = false);

    Task<CarPark> CreateCarParkAsync(CarPark carPark);

    CarPark UpdateCarPark(CarPark carPark);
}