using Models;

namespace Repositories.Interfaces;

public interface ITicketRepository : IRepositoryBase<Ticket>
{
    IQueryable<Ticket> GetTickets(bool includeCarPark = false);

    Task<Ticket> GetTicketByIdAsync(Guid id, bool includeCarPark = false);

    Task<Ticket> CreateTicketAsync(Ticket Ticket);

    void DeleteTicket(Ticket Ticket);

    Ticket UpdateTicket(Ticket Ticket);
}