using Models;

namespace Repositories.Interfaces;

public interface ILicensePlateRepository
{
    IQueryable<LicensePlate> GetLicensePlates();

    IQueryable<LicensePlate> GetLicensePlatesByAccountId(string accountId);

    Task<LicensePlate> GetLicensePlate(string plateNumber, string accountId);

    Task<LicensePlate> CreateLicensePlate(LicensePlate plate);

    IQueryable<string> GetPlateNumbersByAccountId(string accountId);

    LicensePlate UpdateLicensePlate(LicensePlate plate);

    void RemoveLicensePlate(LicensePlate plate);
}