using Models;

namespace Repositories.Interfaces;

public interface ITransactionRepository : IRepositoryBase<Transaction>
{
    IQueryable<Transaction> GetTransactions(bool includeAccount = false);

    Task<Transaction> GetTransactionByIdAsync(Guid id, bool includeAccount = false);

    Task<Transaction> CreateTransaction(Transaction transaction);
}