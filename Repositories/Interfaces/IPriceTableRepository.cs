using Models;

namespace Repositories.Interfaces;

public interface IPriceTableRepository
{
    IQueryable<PriceTable> GetPriceTables(bool includeDetails = false);

    Task<PriceTable> GetPriceTableByCarParkId(Guid carParkId, bool includeDetails = false, bool trackChanges = false);

    Task<PriceTable> CreatePriceTable(PriceTable table);

    PriceTable UpdatePriceTable(PriceTable table);
}