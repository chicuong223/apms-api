﻿using Models;

namespace Repositories.Interfaces
{
    public interface IUserInfoRepository
    {
        Task<UserInfo> GetUserInfoByAccountIdAsync(string id, bool trackChanges = false);

        void UpdateInfo(UserInfo info);
    }
}