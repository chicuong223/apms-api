using Models;

namespace Repositories.Interfaces;

public interface IParkingRepository
{
    IQueryable<Parking> GetParkings();

    Task<Parking> GetParking(string accountId, Guid carParkId);

    Task<Parking> CreateParking(Parking parking);

    void DeleteParking(Parking parking);

    Parking UpdateParking(Parking parking);
}