using Models;

namespace Repositories.Interfaces;

public interface IPriceTableDetailRepository : IRepositoryBase<PriceTableDetail>
{
    IQueryable<PriceTableDetail> GetPriceTableDetailsByCarParkId(Guid carParkId);

    Task<PriceTableDetail> GetPriceTableDetailById(int id);

    Task<PriceTableDetail> CreatePriceTableDetail(PriceTableDetail detail);

    PriceTableDetail UpdatePriceTableDetail(PriceTableDetail detail);

    void RemovePriceTableDetail(PriceTableDetail detail);
}