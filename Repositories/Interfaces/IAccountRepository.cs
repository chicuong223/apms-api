using Models;

namespace Repositories.Interfaces;

public interface IAccountRepository
{
    IQueryable<Account> GetAccounts(bool includeCarPark = false);

    Task<Account> GetAccountByIdAsync(string id, bool includeRole = true);

    Task<Account> CreateAccountAsync(Account account);

    Account UpdateAccount(Account account);

    void RemoveAccount(Account account);

    Task<Account> GetAccountByPhoneNumber(string phoneNumber, bool trackChanges);
}