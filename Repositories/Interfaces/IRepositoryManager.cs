namespace Repositories.Interfaces;

public interface IRepositoryManager
{
    IAccountRepository Account { get; }
    ICarParkRepository CarPark { get; }
    ITicketRepository Ticket { get; }
    IUserInfoRepository UserInfo { get; }
    IFeedbackRepository Feedback { get; }
    ITransactionRepository Transaction { get; }
    IPriceTableRepository PriceTable { get; }
    IPriceTableDetailRepository PriceTableDetail { get; }
    ILicensePlateRepository LicensePlate { get; }
    IParkingRepository Parking { get; }

    Task<int> Save();

    void DetachEntity(object entity);
}