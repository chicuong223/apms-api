using System.Linq.Expressions;

namespace Repositories.Interfaces;

public interface IRepositoryBase<T>
{
    IQueryable<T> FindAll(bool trackChanges);

    IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, bool trackChanges);

    Task<T> CreateAsync(T entity);

    T Update(T entity);

    void Delete(T entity);

    IQueryable<T> GetFromSql(string sqlCommand, bool trackChanges = false);

    // Task<T?> FindByIdAsync(K id);
}