using Models;

namespace Repositories.Interfaces;

public interface IFeedbackRepository
{
    IQueryable<Feedback> GetFeedbacks(bool include = false);

    Task<Feedback> GetFeedbackByIdAsync(Guid id, bool include = false);

    Task<Feedback> CreateFeedbackAsync(Feedback feedback);

    Feedback UpdateFeedback(Feedback feedback);

    void RemoveFeedback(Feedback feedback);
}