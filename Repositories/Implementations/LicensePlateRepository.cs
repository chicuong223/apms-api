using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class LicensePlateRepository : RepositoryBase<LicensePlate>, ILicensePlateRepository
{
    public LicensePlateRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<LicensePlate> CreateLicensePlate(LicensePlate plate) => await CreateAsync(plate);

    public async Task<LicensePlate> GetLicensePlate(string plateNumber, string accountId)
        => await FindByCondition(plate => plate.AccountId.Equals(accountId)
             && plate.PlateNumber.Equals(plateNumber), false).SingleOrDefaultAsync();

    public IQueryable<LicensePlate> GetLicensePlates()
        => FindAll(false);

    public IQueryable<LicensePlate> GetLicensePlatesByAccountId(string accountId)
        => FindByCondition(plate => plate.AccountId.Equals(accountId), false).Include(plate => plate.Account);

    public IQueryable<string> GetPlateNumbersByAccountId(string accountId)
        => FindByCondition(plate => plate.AccountId.Equals(accountId), false)
            .OrderByDescending(plate => plate.UsageCount).Select(plate => plate.PlateNumber);

    public void RemoveLicensePlate(LicensePlate plate) => Delete(plate);

    public LicensePlate UpdateLicensePlate(LicensePlate plate)
        => Update(plate);
}