using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using Utilities;

namespace Repositories.Implementations;

public class AccountRepository : RepositoryBase<Account>, IAccountRepository
{
    public AccountRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<Account> CreateAccountAsync(Account account)
    {
        Tuple<string, string> passwordSaltPair = AccountUtils.CreateHashedPassword(account.Password);
        account.Salt = passwordSaltPair.Item2;
        account.Password = passwordSaltPair.Item1;
        account.Active = true;
        var result = await CreateAsync(account);
        return result;
    }

    public async Task<Account> GetAccountByIdAsync(string id, bool includeRole = true)
        => includeRole ? await FindByCondition(acc => acc.Id.Equals(id), false)
            .Include(acc => acc.Info).Include(acc => acc.Role).SingleOrDefaultAsync()
        : await FindByCondition(acc => acc.Id.Equals(id), false)
            .Include(acc => acc.Info).SingleOrDefaultAsync();

    public async Task<Account> GetAccountByPhoneNumber(string phoneNumber, bool trackChanges)
        => await FindAll(trackChanges)
            // .Include(acc => acc.Info)
            .Include(acc => acc.Role)
            .FirstOrDefaultAsync(acc => acc.PhoneNumber.Equals(phoneNumber) && acc.Active == true);

    public IQueryable<Account> GetAccounts(bool includeCarPark = false)
        => !includeCarPark ? FindAll(false) : FindAll(false).Include(acc => acc.CarPark);

    public void RemoveAccount(Account account) => Delete(account);

    public Account UpdateAccount(Account account)
    {
        var result = Update(account);
        return result;
    }
}