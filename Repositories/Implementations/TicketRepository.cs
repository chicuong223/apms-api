using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class TicketRepository : RepositoryBase<Ticket>, ITicketRepository
{
    public TicketRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<Ticket> CreateTicketAsync(Ticket ticket) => await CreateAsync(ticket);

    public void DeleteTicket(Ticket Ticket) => Delete(Ticket);

    public async Task<Ticket> GetTicketByIdAsync(Guid id, bool includeCarPark = false)
        => !includeCarPark ? await FindByCondition(ticket => ticket.Id.Equals(id), trackChanges: false)
            .SingleOrDefaultAsync()
        : await FindByCondition(ticket => ticket.Id.Equals(id), trackChanges: false)
            .Include(ticket => ticket.CarPark)
            .SingleOrDefaultAsync();

    public IQueryable<Ticket> GetTickets(bool includeCarPark = false)
        => !includeCarPark ? FindAll(trackChanges: false) : FindAll(trackChanges: false).Include(t => t.CarPark);

    public Ticket UpdateTicket(Ticket Ticket) => Update(Ticket);
}