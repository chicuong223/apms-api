using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class PriceTableRepository : RepositoryBase<PriceTable>, IPriceTableRepository
{
    public PriceTableRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<PriceTable> CreatePriceTable(PriceTable table) => await CreateAsync(table);

    public async Task<PriceTable> GetPriceTableByCarParkId(Guid carParkId, bool includeDetails = false, bool trackChanges = false)
        => !includeDetails ? await FindByCondition(t => t.CarParkId.Equals(carParkId), trackChanges)
                .SingleOrDefaultAsync()
            : await FindByCondition(t => t.CarParkId.Equals(carParkId), trackChanges)
                .Include(t => t.Details).SingleOrDefaultAsync();

    public IQueryable<PriceTable> GetPriceTables(bool includeDetails = false)
        => !includeDetails ? FindAll(false) : FindAll(false).Include(t => t.Details);

    public PriceTable UpdatePriceTable(PriceTable table) => Update(table);
}