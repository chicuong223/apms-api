using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class PriceTableDetailRepository : RepositoryBase<PriceTableDetail>, IPriceTableDetailRepository
{
    public PriceTableDetailRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<PriceTableDetail> CreatePriceTableDetail(PriceTableDetail detail)
        => await CreateAsync(detail);

    public async Task<PriceTableDetail> GetPriceTableDetailById(int id)
        => await FindByCondition(d => d.Id == id, false).SingleOrDefaultAsync();

    public IQueryable<PriceTableDetail> GetPriceTableDetailsByCarParkId(Guid carParkId)
        => FindByCondition(d => d.CarParkId.Equals(carParkId), false);

    public void RemovePriceTableDetail(PriceTableDetail detail)
        => Delete(detail);

    public PriceTableDetail UpdatePriceTableDetail(PriceTableDetail detail)
        => Update(detail);
}