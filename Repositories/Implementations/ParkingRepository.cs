using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class ParkingRepository : RepositoryBase<Parking>, IParkingRepository
{
    public ParkingRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<Parking> CreateParking(Parking parking) => await CreateAsync(parking);

    public void DeleteParking(Parking parking) => Delete(parking);

    public Task<Parking> GetParking(string accountId, Guid carParkId)
    => FindByCondition(p => p.AccountId.Equals(accountId) && p.CarParkId.Equals(carParkId), false)
        .SingleOrDefaultAsync();

    public IQueryable<Parking> GetParkings()
    => FindAll(false);

    public Parking UpdateParking(Parking parking) => Update(parking);
}