using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class FeedbackRepository : RepositoryBase<Feedback>, IFeedbackRepository
{
    public FeedbackRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<Feedback> CreateFeedbackAsync(Feedback feedback) => await CreateAsync(feedback);

    public async Task<Feedback> GetFeedbackByIdAsync(Guid id, bool include = false) => !include ?
        await FindByCondition(f => f.Id.Equals(id), trackChanges: false).SingleOrDefaultAsync()
        : await FindByCondition(f => f.Id.Equals(id), trackChanges: false).Include(f => f.Account).SingleOrDefaultAsync();

    public IQueryable<Feedback> GetFeedbacks(bool include = false) => !include ? FindAll(trackChanges: false)
        : FindAll(trackChanges: false).Include(f => f.Account);

    public void RemoveFeedback(Feedback feedback) => Delete(feedback);

    public Feedback UpdateFeedback(Feedback feedback) => Update(feedback);
}