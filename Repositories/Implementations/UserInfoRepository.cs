﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations
{
    public class UserInfoRepository : RepositoryBase<UserInfo>, IUserInfoRepository
    {
        public UserInfoRepository(APMSDbContext context) : base(context)
        {
        }

        public async Task<UserInfo> GetUserInfoByAccountIdAsync(string id, bool trackChanges = false) => await FindByCondition(info => info.Id.Equals(id), trackChanges: trackChanges).SingleOrDefaultAsync();

        public void UpdateInfo(UserInfo info) => Update(info);
    }
}