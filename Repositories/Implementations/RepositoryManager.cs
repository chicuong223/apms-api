using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class RepositoryManager : IRepositoryManager
{
    private APMSDbContext _context;
    private IAccountRepository _accountRepository;
    private ICarParkRepository _carParkRepository;
    private ITicketRepository _TicketRepository;
    private IUserInfoRepository _userInfoRepository;
    private IFeedbackRepository _feedbackRepository;
    private ITransactionRepository _transactionRepository;
    private IPriceTableRepository _priceTableRepository;
    private IPriceTableDetailRepository _priceTableDetailRepository;
    private ILicensePlateRepository _licensePlateRepository;
    private IParkingRepository _parkingRepository;

    public IAccountRepository Account
    {
        get
        {
            if (_accountRepository == null)
            {
                _accountRepository = new AccountRepository(_context);
            }
            return _accountRepository;
        }
    }

    public ICarParkRepository CarPark
    {
        get
        {
            if (_carParkRepository == null)
            {
                _carParkRepository = new CarParkRepository(_context);
            }
            return _carParkRepository;
        }
    }

    public ITicketRepository Ticket
    {
        get
        {
            if (_TicketRepository == null)
            {
                _TicketRepository = new TicketRepository(_context);
            }
            return _TicketRepository;
        }
    }

    public IUserInfoRepository UserInfo
    {
        get
        {
            if (_userInfoRepository == null)
            {
                _userInfoRepository = new UserInfoRepository(_context);
            }
            return _userInfoRepository;
        }
    }

    public IFeedbackRepository Feedback
    {
        get
        {
            if (_feedbackRepository == null)
            {
                _feedbackRepository = new FeedbackRepository(_context);
            }
            return _feedbackRepository;
        }
    }

    public ITransactionRepository Transaction
    {
        get
        {
            if (_transactionRepository == null)
            {
                _transactionRepository = new TransactionRepository(_context);
            }
            return _transactionRepository;
        }
    }

    public IPriceTableRepository PriceTable
    {
        get
        {
            if (_priceTableRepository == null)
            {
                _priceTableRepository = new PriceTableRepository(_context);
            }
            return _priceTableRepository;
        }
    }

    public IPriceTableDetailRepository PriceTableDetail
    {
        get
        {
            if (_priceTableDetailRepository == null)
            {
                _priceTableDetailRepository = new PriceTableDetailRepository(_context);
            }
            return _priceTableDetailRepository;
        }
    }

    public ILicensePlateRepository LicensePlate
    {
        get
        {
            if (_licensePlateRepository == null)
                _licensePlateRepository = new LicensePlateRepository(_context);
            return _licensePlateRepository;
        }
    }

    public IParkingRepository Parking
    {
        get
        {
            if (_parkingRepository == null)
                _parkingRepository = new ParkingRepository(_context);
            return _parkingRepository;
        }
    }

    public RepositoryManager(APMSDbContext context)
    {
        _context = context;
    }

    public async Task<int> Save() => await _context.SaveChangesAsync();

    public void DetachEntity(object entity)
    {
        _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
    }
}