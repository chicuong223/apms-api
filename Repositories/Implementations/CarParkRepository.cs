using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class CarParkRepository : RepositoryBase<CarPark>, ICarParkRepository
{
    public CarParkRepository(APMSDbContext context) : base(context)
    {
    }

    public async Task<CarPark> CreateCarParkAsync(CarPark carPark) => await CreateAsync(carPark);

    public async Task<CarPark> GetCarParkByIdAsync(Guid id, bool includeConfig = false, bool trackChanges = false)
        => !includeConfig ? await FindByCondition(c => c.Id.Equals(id), trackChanges).SingleOrDefaultAsync()
            : await FindByCondition(c => c.Id.Equals(id), trackChanges)
                .Include(c => c.PriceTable).ThenInclude(t => t.Details).SingleOrDefaultAsync();

    // : await FindByCondition(c => c.Id.Equals(id), trackChanges: false).Include(c => c.Config).SingleOrDefaultAsync();

    public IQueryable<CarPark> GetCarParks(bool includeConfig = false, bool trackChanges = false)
        => includeConfig == false ? FindAll(trackChanges)
            : FindAll(trackChanges).Include(p => p.PriceTable).ThenInclude(t => t.Details);

    // : FindAll(trackChanges: false).Include(c => c.Config);

    public CarPark UpdateCarPark(CarPark carPark) => Update(carPark);
}