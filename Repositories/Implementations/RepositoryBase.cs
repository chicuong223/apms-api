using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.Linq.Expressions;

namespace Repositories.Implementations;

public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
{
    protected APMSDbContext Context;

    public RepositoryBase(APMSDbContext context)
    {
        Context = context;
    }

    public async Task<T> CreateAsync(T entity)
    {
        await Context.Set<T>().AddAsync(entity);
        return entity;
    }

    public void Delete(T entity)
    {
        Context.Set<T>().Remove(entity);
        // Context.Entry(entity).State = EntityState.Detached;
    }

    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, bool trackChanges) =>
        !trackChanges ?
            Context.Set<T>()
                .Where(expression)
                .AsNoTracking() :
            Context.Set<T>()
                .Where(expression);

    public IQueryable<T> FindAll(bool trackChanges) =>
        !trackChanges ?
            Context.Set<T>()
                .AsNoTracking() :
            Context.Set<T>();

    public T Update(T entity)
    {
        Context.Entry(entity).State = EntityState.Modified;
        return entity;
    }

    public IQueryable<T> GetFromSql(string sqlCommand, bool trackChanges = false)
    => trackChanges ? Context.Set<T>().FromSqlRaw(sqlCommand)
        : Context.Set<T>().FromSqlRaw(sqlCommand).AsNoTracking();
}