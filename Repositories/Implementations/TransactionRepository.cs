using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;

namespace Repositories.Implementations;

public class TransactionRepository : RepositoryBase<Transaction>, ITransactionRepository
{
    public TransactionRepository(Models.APMSDbContext context) : base(context)
    {
    }

    public async Task<Transaction> CreateTransaction(Models.Transaction transaction) => await CreateAsync(transaction);

    public Task<Transaction> GetTransactionByIdAsync(Guid id, bool includeAccount = false) => !includeAccount ?
        FindByCondition(t => t.Id.Equals(id), trackChanges: false).SingleOrDefaultAsync()
        : FindByCondition(t => t.Id.Equals(id), trackChanges: false)
            .Include(t => t.Account).ThenInclude(acc => acc.Info)
            .SingleOrDefaultAsync();

    public IQueryable<Transaction> GetTransactions(bool includeAccount = false) => !includeAccount
        ? FindAll(trackChanges: false)
        : FindAll(trackChanges: false).Include(t => t.Account).ThenInclude(acc => acc.Info);
}