# Build Stage
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ENV DOTNET_URLS=http://+:6000
WORKDIR /src

# Copy everything
COPY . .
# Restore as distinct layers
RUN dotnet restore "./API/API.csproj" 
# Build and publish a release
RUN dotnet publish "./API/API.csproj" -c release -o /publish 

# Serve Stage
# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0 as final
WORKDIR /app
ENV TZ=Asia/Ho_Chi_Minh
COPY --from=build /publish .

EXPOSE 6000
ENTRYPOINT ["dotnet", "API.dll"]