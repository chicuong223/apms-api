﻿using DTOs.TicketDTOs;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

public class Program
{
    private static Guid carParkId = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454");
    private static List<string> bookedPlateNumbers = new List<string>();
    private static List<string> checkedInPlateNumbers = new List<string>();

    private static readonly string[] jwtArray = {
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMDIiLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE1NTYsImV4cCI6MTY3Mzg2MzU1NiwiaWF0IjoxNjcxMjcxNTU2fQ.I55yZ7EBhuUorUDXnoxz_0VXK9sWWCRqESWNB8gewR8",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMDQiLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE1NzksImV4cCI6MTY3Mzg2MzU3OSwiaWF0IjoxNjcxMjcxNTc5fQ.H29XSpn_wZ7Z7WyW7iaqIKLY56WqJYimvqQ2WvDWviY",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMDUiLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE1OTQsImV4cCI6MTY3Mzg2MzU5NCwiaWF0IjoxNjcxMjcxNTk0fQ.ABXPcoKVX9EE2AzKNO5tT4BeTNiAi-6K8Vk0WudUgUI",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMDYiLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE2MDksImV4cCI6MTY3Mzg2MzYwOSwiaWF0IjoxNjcxMjcxNjA5fQ.mnGvk5qTFmhP_X7_tFMew0JE8wjKFnFnetBIO2LbBh8",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMDciLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE2MjcsImV4cCI6MTY3Mzg2MzYyNywiaWF0IjoxNjcxMjcxNjI3fQ.aKCABWGBWxEZebvwPbEiHk8GHkPEpZ7Pph3zO_cYiz4",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMDgiLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE2NDksImV4cCI6MTY3Mzg2MzY0OSwiaWF0IjoxNjcxMjcxNjQ5fQ.pykGZ83VXTze7N7pUpFeolKdDQMz9TpwcjqXTZvrIiU",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJDVVMwMDAwMTAiLCJyb2xlIjoiQ3VzdG9tZXIiLCJDYXJQYXJrSWQiOiIiLCJuYmYiOjE2NzEyNzE2NzAsImV4cCI6MTY3Mzg2MzY3MCwiaWF0IjoxNjcxMjcxNjcwfQ.jVzfHwzFlEDVDKwH5VMimep-SKUF52Uf-Wp3z7rgnPM"
    };

    private static List<LicensePlate> licensePlates = new List<LicensePlate>();
    private static List<LicensePlate> bookedLicensePlates = new List<LicensePlate>();

    private static async Task Main(string[] args)
    {
        int choice;
        do
        {
            Console.WriteLine("1. Check-in");
            Console.WriteLine("2. Check-out");
            Console.WriteLine("3. Booking");
            Console.WriteLine("Other. Exit");
            Console.Write("Enter your choice: ");
            choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    await CheckIn();
                    break;

                case 2:
                    await CheckOut();
                    break;

                case 3:
                    await Booking();
                    break;

                default:
                    break;
            }
        } while (choice >= 1 && choice <= 3);
    }

    private static HttpContent ConvertObjectToHttpContent(object obj)
    {
        var content = JsonSerializer.Serialize(obj);
        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
        var byteContent = new ByteArrayContent(buffer);
        byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        return byteContent;
    }

    private class ResponseData
    {
        public string PlateNumber { get; set; }
        public string Message { get; set; }
    }

    private class LicensePlate
    {
        public string PlateNumber { get; set; }

        // public string AccountId { get; set; }
        public string Token { get; set; }
    }

    private static async Task CheckIn()
    {
        //check-in;
        const string checkInUrl = "http://apms.ga:6001/api/check-in/create-ticket?openBarrier=false";
        int success = 0;
        int failed = 0;
        using (var client = new HttpClient())
        {
            if (bookedLicensePlates.Count > 0)
            {
                var bookedArray = bookedLicensePlates.ToArray();
                foreach (var booking in bookedArray)
                {
                    CheckinDTO dto
                        = new CheckinDTO(booking.PlateNumber, "imageUrl", carParkId);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, checkInUrl);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", booking.Token);
                    request.Content = ConvertObjectToHttpContent(dto);
                    var response = await client.SendAsync(request);
                    var result = new ResponseData
                    {
                        PlateNumber = dto.PlateNumber,
                        Message = "Check-in failed: " + response.Content.ReadAsStringAsync().Result
                    };
                    if (response.IsSuccessStatusCode)
                    {
                        result.Message = "Checked in successfully!";
                        // checkedInPlateNumbers.Add(plateNumber);
                        licensePlates.Add(new LicensePlate { PlateNumber = booking.PlateNumber, Token = booking.Token });
                        success++;
                    }
                    else
                        failed++;
                    bookedLicensePlates.Remove(booking);
                    System.Console.WriteLine($"{DecodeJWT(booking.Token)} - {result.PlateNumber} - {result.Message} (booked)");
                }
            }
            for (int k = 0; k < jwtArray.Count(); k++)
            {
                for (int i = 1; i <= 3; i++)
                {
                    string plateNumber = GenerateRandomPlateNumber();
                    // if (!bookedArray.Contains(plateNumber))
                    // {
                    CheckinDTO dto
                        = new CheckinDTO(plateNumber, "imageUrl", carParkId);

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, checkInUrl);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", jwtArray[k]);
                    request.Content = ConvertObjectToHttpContent(dto);
                    var response = await client.SendAsync(request);
                    var result = new ResponseData
                    {
                        PlateNumber = dto.PlateNumber,
                        Message = "Check-in failed: " + response.Content.ReadAsStringAsync().Result
                    };
                    if (response.IsSuccessStatusCode)
                    {
                        result.Message = "Checked in successfully!";
                        checkedInPlateNumbers.Add(plateNumber);
                        licensePlates.Add(new LicensePlate { PlateNumber = plateNumber, Token = jwtArray[k] });
                        success++;
                    }
                    else
                        failed++;
                    System.Console.WriteLine($"{DecodeJWT(jwtArray[k])} - {result.PlateNumber} - {result.Message}");
                    // }
                }
            }

            //bookedPlateNumbers.Clear();
        }
        System.Console.WriteLine("SUCCESS: " + success);
        System.Console.WriteLine("FAILURE: " + failed);
    }

    private static async Task CheckOut()
    {
        //check-out
        const string checkOutUrl = "http://apms.ga:6001/api/check-out?openBarrier=false";
        int success = 0;
        int failed = 0;
        using (var client = new HttpClient())
        {
            // var tempPlateNumbers = checkedInPlateNumbers.ToArray();
            var tempArray = licensePlates.ToArray();
            for (int i = 0; i < tempArray.Count(); i++)
            {
                // string plateNumber = 1230 + i;
                CheckoutDTO dto = new CheckoutDTO(new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), tempArray[i].PlateNumber, "imageUrl");

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, checkOutUrl);
                request.Content = ConvertObjectToHttpContent(dto);
                request.Headers.Add("Authorization", $"Bearer {tempArray[i].Token}");

                var response = await client.SendAsync(request);
                var result = new ResponseData
                {
                    PlateNumber = dto.PlateNumber,
                    Message = "Check-out failed: " + response.Content.ReadAsStringAsync().Result
                };
                if (response.IsSuccessStatusCode)
                {
                    result.Message = "Checked out successfully!";
                    success++;
                    // checkedInPlateNumbers.Remove(number);
                    licensePlates.Remove(tempArray[i]);
                }
                else
                {
                    failed++;
                }
                System.Console.WriteLine($"{DecodeJWT(tempArray[i].Token)} - {result.PlateNumber} - {result.Message}");
            }
        }
        System.Console.WriteLine("SUCCESS: " + success);
        System.Console.WriteLine("FAILURE: " + failed);
    }

    private static async Task Booking()
    {
        const string bookingUrl = "http://apms.ga:6001/api/tickets";
        int success = 0;
        int failed = 0;
        //Create 20 bookings
        List<TicketCreateDTO> bookings = new List<TicketCreateDTO>()
        {
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(1)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(2)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(2)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(3)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(24)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(-24)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(3)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(3)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(3)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(-1)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(5)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(-24)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(24)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(8)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddDays(5)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(3)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(2)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(5)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(6)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(8)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(8)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(8)),
            new TicketCreateDTO(GenerateRandomPlateNumber(), carParkId, DateTime.Now.AddHours(8))
        };
        using (var client = new HttpClient())
        {
            foreach (var token in jwtArray)
            {
                int count = 0;
                var temp = bookings.ToArray();
                foreach (var booking in temp)
                {
                    if (count >= 3)
                        break;
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, bookingUrl);
                    request.Headers.Add("Authorization", $"Bearer {token}");
                    request.Content = ConvertObjectToHttpContent(booking);

                    var response = await client.SendAsync(request);
                    var result = new ResponseData
                    {
                        PlateNumber = booking.PlateNumber,
                        Message = "Booking failed: " + response.Content.ReadAsStringAsync().Result
                    };
                    if (response.IsSuccessStatusCode)
                    {
                        bookedPlateNumbers.Add(result.PlateNumber);
                        result.Message = "Booking successfully!";
                        success++;
                        bookedLicensePlates.Add(new LicensePlate { Token = token, PlateNumber = booking.PlateNumber });
                    }
                    else
                    {
                        failed++;
                    }
                    System.Console.WriteLine($"{DecodeJWT(token)} - {result.PlateNumber} - {booking.ArriveTime} - {result.Message}");
                    count++;
                    bookings.Remove(booking);
                }
            }
        }
        System.Console.WriteLine("SUCCESS: " + success);
        System.Console.WriteLine("FAILURE: " + failed);
    }

    private static string GenerateRandomPlateNumber()
    {
        Random rand = new Random();
        int[] prefixArray = {
            11, 12, 14, 15, 16, 17, 18, 19, 20,
            21, 22,23,24,25,26,27,28,29,30,31,32,
            33,34,35,36,37,38,39,40,41,43,47,48,49,50,
            51,52,53,54,55,56,57,58,59,60,61,62,63,64,
            65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,81,82,
            83,84,85,86,88,89,90,92,93,94,95,97,98,99
        };
        // int prefix = rand.Next(11, 99);
        int plateNumber = rand.Next(10000, 99999);
        string chars = "ABCDEFGHKLMNPSTUVXYZ";
        int letterIndex = rand.Next(0, chars.Length - 1);
        int prefixIndex = rand.Next(0, prefixArray.Length - 1);
        return $"{prefixArray[prefixIndex]}{chars[letterIndex]}-{plateNumber}";
    }

    private static string DecodeJWT(string jwt)
    {
        var token = new JwtSecurityToken(jwtEncodedString: jwt);
        string id = token.Claims.First(c => c.Type == "nameid").Value;
        // var id = jwtSecurityToken.Claims.First(claim => claim.Type.Equals(ClaimTypes.NameIdentifier)).Value;
        return id;
    }
}