using System.Text.Json;
using System.Text.Json.Serialization;

namespace Utilities;

public class SerializationUtils
{
    public static string SerializeObject(object obj, System.Type type)
    {
        string result = "";
        result = JsonSerializer.Serialize(obj, type, new JsonSerializerOptions { ReferenceHandler = ReferenceHandler.IgnoreCycles });
        return result;
    }

    public static T DeserializeObject<T>(string str)
    {
        try
        {
            T result = JsonSerializer.Deserialize<T>(str);
            return result;
        }
        catch
        {
            throw;
        }
    }
}