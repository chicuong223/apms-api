using System.ComponentModel;

namespace Utilities.Enums;

public enum TransactionType
{
    [Description("Top-up")]
    TopUp = 0,

    [Description("Booking")]
    Booking = 1,

    [Description("Parking fee")]
    ParkingFee = 2
}