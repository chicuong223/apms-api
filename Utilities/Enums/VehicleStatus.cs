using System.ComponentModel;

namespace Utilities.Enums;

public enum VehicleStatus
{
    [Description("Active")]
    Active = 1,

    [Description("Removed")]
    Removed = -1,

    [Description("Unavailable")]
    Unavailable = 0
}