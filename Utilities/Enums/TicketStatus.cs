using System.ComponentModel;

namespace Utilities.Enums;

public enum TicketStatus
{
    [Description("Pending")]
    Pending = 0,

    [Description("Checked in")]
    CheckedIn = 1,

    [Description("Checked out")]
    CheckedOut = 2,

    [Description("Cancelled")]
    Cancelled = -1
}