using System.ComponentModel;

namespace Utilities.Enums;

public enum CarParkStatus
{
    [Description("Active")]
    Active = 0,

    [Description("Removed")]
    Removed = -1
}