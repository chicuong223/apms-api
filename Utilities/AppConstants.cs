namespace Utilities;

public static class AppConstants
{
    public const string AdminRole = "Admin";
    public const string OwnerRole = "Owner";
    public const string StaffRole = "Staff";
    public const string CustomerRole = "Customer";
    public const int AdminRoleId = 1;
    public const int OwnerRoleId = 2;
    public const int StaffRoleId = 3;
    public const int CustomerRoleId = 4;
    public const decimal DefaultParkingFeePerHour = 0;
    public const decimal DefaultReservationFee = 0;
}