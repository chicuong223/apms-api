﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;

namespace Utilities
{
    public static class AccountUtils
    {
        public static Tuple<string, string> CreateHashedPassword(string rawPassword)
        {
            byte[] saltByte = RandomNumberGenerator.GetBytes(128 / 8);
            string salt = Convert.ToBase64String(saltByte);
            string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: rawPassword!,
                salt: saltByte,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8
                ));
            return Tuple.Create(hashedPassword, salt);
        }

        public static string HashPasswordWithProvidedSalt(string rawPassword, string salt)
        {
            byte[] saltByte = Convert.FromBase64String(salt);
            string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: rawPassword!,
                salt: saltByte,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8
                ));
            return hashedPassword;
        }

        public static string GenerateAccountID(int roleId)
        {
            string result = "";
            switch (roleId)
            {
                case AppConstants.AdminRoleId:
                    result += "adm";
                    break;

                case AppConstants.OwnerRoleId:
                    result += "own";
                    break;

                case AppConstants.StaffRoleId:
                    result += "stf";
                    break;

                case AppConstants.CustomerRoleId:
                    result += "cus";
                    break;

                default:
                    throw new ArgumentException("Invalid role");
            }
            Random rand = new Random();
            int randInt = rand.Next(1, 999999);
            result += randInt.ToString("D6");
            return result.ToUpper();
        }
    }
}