using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.CarParkDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.Security.Claims;
using Utilities;
using Utilities.Enums;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CarParksController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    private readonly IMapper _mapper;

    public CarParksController(IRepositoryManager repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [Authorize(Roles = $"{AppConstants.AdminRole},{AppConstants.CustomerRole}")]
    public IActionResult GetCarParks(string name, CarParkStatus? status, string city,
        string district, string ward, string street,
        double? latitude, double? longitude,
        int? pageSize, int pageIndex = 1, bool includeConfig = false, bool includeAccounts = false)
    {
        try
        {
            #region Filter data

            var carParksFromDb = !includeAccounts ? _repository.CarPark.GetCarParks(includeConfig)
                : _repository.CarPark.GetCarParks(includeConfig)
                    .Include(c => c.Accounts.Where(acc => acc.Active == true))
                    .ThenInclude(acc => acc.Role);

            if (!string.IsNullOrWhiteSpace(name))
            {
                carParksFromDb = carParksFromDb.Where(c => c.Name.ToLower().Contains(name.ToLower()));
            }
            if (status.HasValue)
            {
                carParksFromDb = carParksFromDb.Where(c => c.Status == status.Value);
            }
            if (!string.IsNullOrWhiteSpace(city))
            {
                carParksFromDb = carParksFromDb.Where(c => c.City.ToLower().Contains(city.ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(district))
            {
                carParksFromDb = carParksFromDb.Where(c => c.District.ToLower().Contains(district.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(ward))
            {
                carParksFromDb = carParksFromDb.Where(c => c.Ward.ToLower().Contains(ward.ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(street))
            {
                carParksFromDb = carParksFromDb.Where(c => c.Street.ToLower().Contains(street.ToLower()));
            }

            #endregion Filter data

            #region Pagination

            if (!pageSize.HasValue)
            {
                pageSize = carParksFromDb.Count();
            }
            var pagedList = PaginatedList<CarPark>.Create(carParksFromDb, pageSize.Value, pageIndex);

            #endregion Pagination

            #region Mapping result

            //if latitude and longitude exist, map to CarParkGetWithDistanceDTO
            //else map to CarParkGetDTO
            if (latitude.HasValue && longitude.HasValue)
            {
                List<CarParkGetWithDistanceDTO> resultDist = new List<CarParkGetWithDistanceDTO>();
                foreach (var carPark in pagedList)
                {
                    // double distance = Math
                    //     .Sqrt((carPark.Latitude - latitude.Value) * (carPark.Latitude - latitude.Value) + (carPark.Longitude - longitude.Value) * (carPark.Longitude - longitude.Value));
                    double distance = MathUtils.CalculateDistanctFromCoordinationInKm(latitude.Value, longitude.Value, carPark.Latitude, carPark.Longitude);
                    CarParkGetWithDistanceDTO dto = _mapper.Map<CarParkGetWithDistanceDTO>(carPark);
                    dto.Distance = distance;
                    resultDist.Add(dto);
                }

                var resultObj = resultDist.OrderBy(c => c.Distance);
                return new SuccessResponse(SuccessStatusCode.Ok, new { CarParks = resultObj, TotalPage = pagedList.TotalPage }, "Loaded car parks successfully!");
            }
            var result = _mapper.Map<IEnumerable<CarParkGetDTO>>(pagedList);

            #endregion Mapping result

            #region If latitude and longitude is provided, calculate distance then return in result

            if (latitude.HasValue && longitude.HasValue)
            {
                // Dictionary<CarParkGetDTO, double> resultDict = new Dictionary<CarParkGetDTO, double>();
                List<CarParkGetWithDistanceDTO> resultDict = new List<CarParkGetWithDistanceDTO>();
            }

            #endregion If latitude and longitude is provided, calculate distance then return in result

            return new SuccessResponse(SuccessStatusCode.Ok, new { CarParks = result, TotalPage = pagedList.TotalPage }, "Loaded car parks successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetCarParkByIdAsync(Guid id, bool includeConfig = false, bool includeAccounts = false)
    {
        try
        {
            CarPark carPark = !includeAccounts ? await _repository.CarPark.GetCarParkByIdAsync(id, includeConfig)
                : await _repository.CarPark.GetCarParks(includeConfig)
                    .Include(c => c.Accounts).ThenInclude(acc => acc.Role)
                    .SingleOrDefaultAsync(p => p.Id.Equals(id));
            if (carPark == null)
            {
                return NotFound();
            }
            var result = _mapper.Map<CarParkGetDTO>(carPark);
            return new SuccessResponse(SuccessStatusCode.Ok, result, "Loaded car park successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost]
    [Authorize(Roles = AppConstants.AdminRole)]
    public async Task<IActionResult> CreateCarPark([FromBody] CarParkCreateDTO carPark)
    {
        if (!ModelState.IsValid)
        {
            // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
            return BadRequest(ModelState);
        }
        try
        {
            #region Check valid data

            //Check address
            var existCarParkAddress = await _repository.CarPark.GetCarParks()
                .AnyAsync(park => park.AddressNumber.ToLower().Equals(carPark.AddressNumber.ToLower())
                    && park.Street.ToLower().Equals(carPark.Street.ToLower())
                    && park.Ward.ToLower().Equals(carPark.Ward.ToLower())
                    && park.District.ToLower().Equals(carPark.District.ToLower())
                    && park.City.ToLower().Equals(carPark.City.ToLower())
                    && park.Status == CarParkStatus.Active);
            if (existCarParkAddress)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This address is used!", true);
                return BadRequest("Address is used!");
            }

            //Check coordination
            var existCarParkCoordination = await _repository.CarPark.GetCarParks()
                .AnyAsync(park => park.Latitude == carPark.Latitude
                    && park.Longitude == carPark.Longitude);
            if (existCarParkCoordination)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This coordination is used!", true);
                return BadRequest("Coordination is used!");
            }

            #endregion Check valid data

            #region Map entity

            var entity = new CarPark
            {
                AddressNumber = carPark.AddressNumber,
                Street = carPark.Street,
                Ward = carPark.Ward,
                District = carPark.District,
                City = carPark.City,
                Latitude = carPark.Latitude,
                Longitude = carPark.Longitude,
                Name = carPark.Name,
                MaxCapacity = carPark.MaxCapacity,
                AvailableSlotsCount = carPark.MaxCapacity,
                PhoneNumber = carPark.PhoneNumber,
                ProvinceId = carPark.ProvinceId,
                Status = CarParkStatus.Active
            };

            entity = await _repository.CarPark.CreateCarParkAsync(entity);

            #endregion Map entity

            #region Check owner and staff account

            if (!string.IsNullOrEmpty(carPark.OwnerPhoneNumber))
            {
                var ownerAccount = await _repository.Account.GetAccountByPhoneNumber(carPark.OwnerPhoneNumber, true);
                if (ownerAccount == null || !ownerAccount.Active.Value || ownerAccount.RoleId != AppConstants.OwnerRoleId)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Owner account not found!", true);
                    return BadRequest("Owner account not found!");
                }
                if (ownerAccount.CarParkId != null)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "This owner account belongs to another car park!", true);
                    return BadRequest("This owner account belongs to another car park!");
                }

                ownerAccount.CarParkId = entity.Id;
                _repository.Account.UpdateAccount(ownerAccount);
            }

            if (!string.IsNullOrEmpty(carPark.StaffPhoneNumber))
            {
                var staffAccount = await _repository.Account.GetAccountByPhoneNumber(carPark.StaffPhoneNumber, true);
                if (staffAccount == null || !staffAccount.Active.Value || staffAccount.RoleId != AppConstants.StaffRoleId)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Staff account not found!", true);
                    return BadRequest("Staff account not found!");
                }
                if (staffAccount.CarParkId != null)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "This staff account belongs to another car park!", true);
                    return BadRequest("This staff account belongs to another car park!");
                }
                staffAccount.CarParkId = entity.Id;
                _repository.Account.UpdateAccount(staffAccount);
            }

            #endregion Check owner and staff account

            #region Create a default Price table

            PriceTable priceTable = new PriceTable
            {
                CarParkId = entity.Id,
                FeePerHour = AppConstants.DefaultParkingFeePerHour,
                ReservationFeePercentage = AppConstants.DefaultReservationFee,
                LastModified = DateTime.UtcNow
            };

            await _repository.PriceTable.CreatePriceTable(priceTable);

            #endregion Create a default Price table

            #region Add car park to database

            await _repository.Save();

            #endregion Add car park to database

            var result = _mapper.Map<CarParkGetDTO>(entity);
            return new SuccessResponse(SuccessStatusCode.Ok, result, "Created car park successfully");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("slots/{id}")]
    public async Task<IActionResult> CountAvailableSlotsByCarParkId(Guid id)
    {
        try
        {
            var carPark = await _repository.CarPark.GetCarParkByIdAsync(id);
            if (carPark == null)
            {
                return NotFound();
            }
            if (carPark.Status == CarParkStatus.Removed)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not active", true);
                return BadRequest("Car park not active!");
            }
            return new SuccessResponse(SuccessStatusCode.Ok, carPark.AvailableSlotsCount, "Count slots successfully");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPatch("{id}")]
    [Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.OwnerRole}, {AppConstants.StaffRole}")]
    public async Task<IActionResult> UpdateCarPark(Guid id, [FromBody] CarParkUpdateDTO dTO)
    {
        if (!ModelState.IsValid)
        {
            // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
            return BadRequest(ModelState);
        }

        if (!id.Equals(dTO.Id))
        {
            // return new ErrorResponse(ErrorStatusCode.BadRequest, "Invalid ID", true);
            return BadRequest("Invalid ID");
        }
        try
        {
            #region check user active

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null
                || currentUser.Active == false
                || (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.StaffRole)
                    && currentUser.CanEditCarPark == false))
            {
                return Forbid();
            }

            #endregion check user active

            var carParkToUpdate = await _repository.CarPark.GetCarParkByIdAsync(id);
            if (carParkToUpdate == null)
            {
                return NotFound();
            }

            #region Check if user is allowed to update car park

            if (!User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.AdminRole))
            {
                if (!carParkToUpdate.Id.Equals(currentUser.CarParkId))
                    return Forbid();
            }

            #endregion Check if user is allowed to update car park

            if (!string.IsNullOrEmpty(dTO.AddressNumber))
            {
                carParkToUpdate.AddressNumber = dTO.AddressNumber;
            }
            if (!string.IsNullOrEmpty(dTO.Street))
            {
                carParkToUpdate.Street = dTO.Street;
            }
            if (!string.IsNullOrEmpty(dTO.Ward))
            {
                carParkToUpdate.Ward = dTO.Ward;
            }
            if (!string.IsNullOrEmpty(dTO.District))
            {
                carParkToUpdate.District = dTO.District;
            }
            if (!string.IsNullOrEmpty(dTO.City))
            {
                carParkToUpdate.City = dTO.City;
            }
            //Check address
            var existCarParkAddress = await _repository.CarPark.GetCarParks()
                .AnyAsync(park => park.AddressNumber.ToLower().Equals(carParkToUpdate.AddressNumber.ToLower())
                    && park.Street.ToLower().Equals(carParkToUpdate.Street.ToLower())
                    && park.Ward.ToLower().Equals(carParkToUpdate.Ward.ToLower())
                    && park.District.ToLower().Equals(carParkToUpdate.District.ToLower())
                    && park.City.ToLower().Equals(carParkToUpdate.City.ToLower())
                    && !park.Id.Equals(id)
                    && park.Status == CarParkStatus.Active);
            if (existCarParkAddress)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This address is used!", true);
                return BadRequest("Address is used!");
            }

            if (dTO.Latitude.HasValue)
            {
                carParkToUpdate.Latitude = dTO.Latitude.Value;
            }
            if (dTO.Longitude.HasValue)
            {
                carParkToUpdate.Longitude = dTO.Longitude.Value;
            }
            //Check coordination
            var existCarParkCoordination = await _repository.CarPark.GetCarParks()
                .AnyAsync(park => park.Latitude == carParkToUpdate.Latitude
                    && park.Longitude == carParkToUpdate.Longitude
                    && !park.Id.Equals(id));
            if (existCarParkCoordination)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This coordination is used!", true);
                return BadRequest("Coordination is used!");
            }

            //Check staff and owner account, only execute when user is admin
            if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.AdminRole))
            {
                if (!string.IsNullOrEmpty(dTO.OwnerPhoneNumber))
                {
                    var ownerAccount = await _repository.Account.GetAccountByPhoneNumber(dTO.OwnerPhoneNumber, true);
                    if (ownerAccount == null || !ownerAccount.Active.Value || ownerAccount.RoleId != AppConstants.OwnerRoleId)
                    {
                        // return new ErrorResponse(ErrorStatusCode.BadRequest, "Owner account not found!", true);
                        return BadRequest("Owner account not found!");
                    }
                    if (ownerAccount.CarParkId != null && !ownerAccount.CarParkId.Equals(dTO.Id))
                    {
                        // return new ErrorResponse(ErrorStatusCode.BadRequest, "This owner account belongs to another car park!", true);
                        return BadRequest("Owner account belongs to another car park!");
                    }
                    //Get old owner, if different ID, set carPark = null
                    var oldOwner = await _repository.Account.GetAccounts()
                        .FirstOrDefaultAsync(acc => acc.CarParkId.Equals(id) && acc.RoleId == AppConstants.OwnerRoleId);
                    if (oldOwner != null && !oldOwner.Id.Equals(ownerAccount.Id))
                    {
                        oldOwner.CarParkId = null;
                        _repository.Account.UpdateAccount(oldOwner);
                    }

                    //Update car park ID of new owner
                    ownerAccount.CarParkId = carParkToUpdate.Id;
                    _repository.Account.UpdateAccount(ownerAccount);
                }

                if (!string.IsNullOrEmpty(dTO.StaffPhoneNumber))
                {
                    var staffAccount = await _repository.Account.GetAccountByPhoneNumber(dTO.StaffPhoneNumber, true);
                    if (staffAccount == null || !staffAccount.Active.Value || staffAccount.RoleId != AppConstants.StaffRoleId)
                    {
                        // return new ErrorResponse(ErrorStatusCode.BadRequest, "Staff account not found!", true);
                        return BadRequest("Staff account not found!");
                    }
                    if (staffAccount.CarParkId != null && !staffAccount.CarParkId.Equals(dTO.Id))
                    {
                        // return new ErrorResponse(ErrorStatusCode.BadRequest, "This staff account belongs to another car park!", true);
                        return BadRequest("Staff account belongs to another car park!");
                    }

                    //Get old staff, if oldStaff.Id != staffAccount.Id, set oldStaff.CarPark = null
                    var oldStaff = await _repository.Account.GetAccounts()
                        .FirstOrDefaultAsync(acc => acc.CarParkId.Equals(id) && acc.RoleId == AppConstants.StaffRoleId);
                    if (oldStaff != null && !oldStaff.Id.Equals(staffAccount.Id))
                    {
                        oldStaff.CarParkId = null;
                        _repository.Account.UpdateAccount(oldStaff);
                    }

                    //Update car park ID of the new staff
                    staffAccount.CarParkId = carParkToUpdate.Id;
                    _repository.Account.UpdateAccount(staffAccount);
                }
            }

            if (!string.IsNullOrEmpty(dTO.Name))
            {
                carParkToUpdate.Name = dTO.Name;
            }
            if (!string.IsNullOrEmpty(dTO.PhoneNumber))
            {
                carParkToUpdate.PhoneNumber = dTO.PhoneNumber;
            }
            if (dTO.MaxCapacity.HasValue)
            {
                carParkToUpdate.MaxCapacity = dTO.MaxCapacity.Value;
                if (carParkToUpdate.AvailableSlotsCount > dTO.MaxCapacity.Value)
                {
                    carParkToUpdate.AvailableSlotsCount = dTO.MaxCapacity.Value;
                }
                else
                {
                    //Update available slots count by comparing parking or booking slots and max capacity
                    var ticketsCount = await _repository.Ticket
                        .FindByCondition(t => t.CarParkId.Equals(carParkToUpdate.Id)
                            && (t.Status == TicketStatus.Pending || t.Status == TicketStatus.CheckedIn), false).CountAsync();
                    int availableSlotsCount = dTO.MaxCapacity.Value - ticketsCount;
                    if (availableSlotsCount < 0)
                        availableSlotsCount = 0;
                    carParkToUpdate.AvailableSlotsCount = availableSlotsCount;
                }
            }

            _repository.CarPark.UpdateCarPark(carParkToUpdate);
            await _repository.Save();
            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<CarParkGetDTO>(carParkToUpdate), "Updated car park successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(Roles = AppConstants.AdminRole)]
    public async Task<IActionResult> DeactivateCarPark(Guid id)
    {
        try
        {
            var carPark = await _repository.CarPark.GetCarParkByIdAsync(id);
            if (carPark == null) return NotFound();

            #region Cancel and refund pending tickets and deactivate car park if car park is active

            if (carPark.Status == CarParkStatus.Active)
            {
                carPark.Status = CarParkStatus.Removed;

                var bookings = await _repository.Ticket
                                .FindByCondition(t => t.Status == TicketStatus.Pending && t.CarParkId.Equals(id), false)
                                .ToListAsync();
                foreach (var booking in bookings)
                {
                    booking.Status = TicketStatus.Cancelled;
                    // booking.CancellerAccountId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                    _repository.Ticket.Update(booking);

                    //Refund
                    var userInfo = await _repository.UserInfo.GetUserInfoByAccountIdAsync(booking.AccountId, true);
                    if (userInfo != null)
                    {
                        userInfo.AccountBalance += booking.ReservationFee;
                        _repository.UserInfo.UpdateInfo(userInfo);
                    }
                }
            }

            #endregion Cancel and refund pending tickets and deactivate car park if car park is active

            else
            {
                carPark.Status = CarParkStatus.Active;
            }
            _repository.CarPark.UpdateCarPark(carPark);

            await _repository.Save();
            return new SuccessResponse(SuccessStatusCode.NoContent, null, "");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("most-used")]
    [Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> GetCarParksByUseCount(CarParkStatus? status, int? pageSize, int pageIndex = 1, bool includeConfig = false, bool sortByLastVisited = false)
    {
        try
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var carParks = _repository.CarPark.GetCarParks(includeConfig).ToList();
            if (status.HasValue)
            {
                carParks = carParks.Where(p => p.Status == status.Value).ToList();
            }
            // var mappedCarParks = _mapper.Map<IEnumerable<CarParkGetDTO>>(carParks);
            List<ParkingCountDTO> lst = new List<ParkingCountDTO>();
            foreach (var park in carParks)
            {
                var parking = await _repository.Parking.GetParking(userId, park.Id);
                int count = 0;
                DateTime? lastVisited = null;
                if (parking != null)
                {
                    count = parking.UseCount;
                    lastVisited = parking.LastVisited;
                }
                // ParkingCountDTO dto = new ParkingCountDTO(park, count);
                ParkingCountDTO dto = _mapper.Map<ParkingCountDTO>(park);
                dto.VisitCount = count;
                dto.LastVisited = lastVisited;
                lst.Add(dto);
            }
            var result = sortByLastVisited
                ? lst.OrderByDescending(p => p.LastVisited).ThenByDescending(p => p.VisitCount).AsQueryable()
                : lst.OrderByDescending(p => p.VisitCount).AsQueryable();

            #region Pagination

            if (!pageSize.HasValue)
                pageSize = result.Count();
            PaginatedList<ParkingCountDTO> pagedList = PaginatedList<ParkingCountDTO>.Create(result, pageSize.Value, pageIndex);

            #endregion Pagination

            return new SuccessResponse(SuccessStatusCode.Ok, new { CarParks = pagedList, TotalPage = pagedList.TotalPage }, "Loaded car parks successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}