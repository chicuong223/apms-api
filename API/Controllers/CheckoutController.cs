﻿using API.Helpers;
using API.Responses.SuccessResponses;
using API.SignalRHubs;
using AutoMapper;
using DTOs.TicketDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Utilities;
using Utilities.Enums;

namespace API.Controllers
{
    [Route("api/check-out")]
    [ApiController]
    public class CheckoutController : ControllerBase
    {
        private readonly IRepositoryManager _repository;

        private readonly IMapper _mapper;

        private readonly IHubContext<CheckinHub> _hub;

        private readonly FirebaseService _firebaseService;

        public CheckoutController(IRepositoryManager repository, IMapper mapper, IHubContext<CheckinHub> hub, FirebaseService firebaseService)
        {
            _repository = repository;
            _firebaseService = firebaseService;
            _mapper = mapper;
            _hub = hub;
        }

        [HttpPost]
        public async Task<IActionResult> ScanPlateNumber([Required] string plateNumber, [Required] string picOutUrl, [Required] Guid carParkId)
        {
            try
            {
                var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId);
                if (carPark == null)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not found", true);
                    return NotFound("Car park not found!");
                }

                var ticketExist = await _repository.Ticket.GetTickets()
                    .FirstOrDefaultAsync(t => t.PlateNumber.Equals(plateNumber)
                        && t.CarParkId.Equals(carParkId)
                        && t.Status == TicketStatus.CheckedIn);

                if (ticketExist == null)
                {
                    return new SuccessResponse(SuccessStatusCode.Ok, new { PlateNumber = plateNumber, CarParkId = carParkId, Pic = picOutUrl, Exist = false }, "Scan plate number successfully");
                }

                return new SuccessResponse(SuccessStatusCode.Ok, new { PlateNumber = plateNumber, CarParkId = carParkId, Pic = picOutUrl, Exist = true }, "Scan plate number successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut]
        [Authorize(Roles = AppConstants.CustomerRole)]
        public async Task<IActionResult> CheckOut([FromBody] CheckoutDTO dto, [FromQuery] bool openBarrier = true)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            try
            {
                string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var ticket = await _repository.Ticket.GetTickets()
                    .Include(b => b.Account)
                    .ThenInclude(acc => acc.Info)
                    .FirstOrDefaultAsync(b => b.CarParkId.Equals(dto.CarParkId)
                        && b.AccountId.Equals(currentUserId)
                        && b.PlateNumber.Equals(dto.PlateNumber)
                        && b.Status == TicketStatus.CheckedIn);
                if (ticket == null)
                {
                    return BadRequest("Ticket not found!");
                }

                #region Calculate parking fee

                var checkoutTime = DateTime.UtcNow;
                decimal totalFee = 0;

                #region Get price table from PriceTable string

                PriceTable priceTable = null;
                var priceTableString = ticket.PriceTable;
                if (!string.IsNullOrEmpty(priceTableString))
                {
                    priceTable = SerializationUtils.DeserializeObject<PriceTable>(priceTableString);
                }

                #endregion Get price table from PriceTable string

                if (priceTable != null && priceTable.Details.Count > 0)
                {
                    totalFee = ParkingFeeUtilities.CalculateParkingFee(priceTable.Details, ticket.StartTime.Value, checkoutTime);
                }
                else if (priceTable != null && priceTable.FeePerHour.HasValue)
                {
                    double totalHours = Math.Ceiling(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                    // : Math.Floor(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                    totalFee = priceTable.FeePerHour.Value * (decimal)totalHours;
                }
                ticket.TotalFee = totalFee;
                ticket.EndTime = checkoutTime;
                ticket.PicOutUrl = dto.PicOutUrl;

                #endregion Calculate parking fee

                #region Parking payment

                var userInfo = ticket.Account.Info;
                var feeToPay = totalFee + ticket.OverdueFee;

                // //if this ticket is booked before hand, exclude the reservation fee from fee to pay
                // if (ticket.BookTime.HasValue)
                // {
                //     feeToPay -= ticket.ReservationFee;
                // }

                if (userInfo == null)
                {
                    throw new Exception("User Info not found!");
                }
                if (userInfo.AccountBalance < feeToPay)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Account balance is not enough", true);
                    return BadRequest("Account balance not enough!");
                }
                userInfo.AccountBalance -= feeToPay;
                _repository.UserInfo.UpdateInfo(userInfo);

                ticket.Status = TicketStatus.CheckedOut;

                #endregion Parking payment

                _repository.Ticket.UpdateTicket(ticket);

                var carPark = await _repository.CarPark.GetCarParkByIdAsync(dto.CarParkId);
                carPark.AvailableSlotsCount++;
                if (carPark.AvailableSlotsCount > carPark.MaxCapacity)
                {
                    carPark.AvailableSlotsCount = carPark.MaxCapacity;
                }
                _repository.CarPark.UpdateCarPark(carPark);

                #region Increase parking count

                var parking = await _repository.Parking.GetParking(currentUserId, carPark.Id);
                if (parking == null)
                {
                    parking = new Parking
                    {
                        AccountId = currentUserId,
                        CarParkId = carPark.Id,
                        LastVisited = checkoutTime,
                        UseCount = 1
                    };
                    await _repository.Parking.CreateParking(parking);
                }
                else
                {
                    parking.UseCount++;
                    parking.LastVisited = checkoutTime;
                    _repository.Parking.UpdateParking(parking);
                }

                #endregion Increase parking count

                #region Create transaction

                Transaction transaction = new Transaction
                {
                    AccountId = currentUserId,
                    Amount = feeToPay,
                    TicketId = ticket.Id,
                    Time = DateTime.UtcNow,
                    TransactionType = TransactionType.ParkingFee
                };

                await _repository.Transaction.CreateTransaction(transaction);

                #endregion Create transaction

                await _repository.Save();

                #region Send SignalR GIGABYTE

                await _hub.Clients.All.SendAsync("NewCheckout", dto.PlateNumber, dto.CarParkId,
                    ticket.PicInUrl, dto.PicOutUrl);

                #endregion Send SignalR GIGABYTE

                if (openBarrier == true)
                {
                    #region Open Barrier

                    var prevServoExit = _firebaseService.GetServoValue("servoExit_prev");
                    if (!prevServoExit.Equals("\"OPEN\""))
                        _firebaseService.OpenServo("servoExit");

                    #endregion Open Barrier
                }

                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<TicketGetDTO>(ticket), "Checked out successfully!");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("manual")]
        [Authorize(Roles = AppConstants.StaffRole)]
        public async Task<IActionResult> CheckoutManually([FromBody] CheckoutManualDTO dto)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            try
            {
                var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var account = await _repository.Account.GetAccountByIdAsync(currentUserId);
                if (account == null || !account.Active.Value || account.CarParkId == null)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Customer not found", true);
                    return Forbid();
                }

                var ticket = await _repository.Ticket.FindAll(false)
                    .Include(t => t.Account)
                    .ThenInclude(acc => acc.Info)
                    .FirstOrDefaultAsync(t => t.PlateNumber.Equals(dto.PlateNumber)
                        && t.CarParkId.Equals(account.CarParkId) && t.Status == TicketStatus.CheckedIn);
                if (ticket == null)
                {
                    return BadRequest($"Ticket with plate number {dto.PlateNumber} could not be found!");
                }

                #region Calculate parking fee

                var checkoutTime = DateTime.UtcNow;
                decimal totalFee = 0;

                #region Get price table from PriceTable string

                PriceTable priceTable = null;
                var priceTableString = ticket.PriceTable;
                if (!string.IsNullOrEmpty(priceTableString))
                {
                    priceTable = SerializationUtils.DeserializeObject<PriceTable>(priceTableString);
                }

                #endregion Get price table from PriceTable string

                if (priceTable != null && priceTable.Details.Count > 0)
                {
                    totalFee = ParkingFeeUtilities.CalculateParkingFee(priceTable.Details, TimeZoneInfo.ConvertTimeFromUtc(ticket.StartTime.Value, timeZone), TimeZoneInfo.ConvertTimeFromUtc(checkoutTime, timeZone));
                }
                else if (priceTable != null && priceTable.FeePerHour.HasValue)
                {
                    double totalHours = Math.Ceiling(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                    // : Math.Floor(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                    totalFee = priceTable.FeePerHour.Value * (decimal)totalHours;
                }
                ticket.TotalFee = totalFee;
                ticket.EndTime = checkoutTime;
                ticket.PicOutUrl = dto.PicOutUrl;

                #endregion Calculate parking fee

                #region Parking payment

                var userInfo = ticket.Account.Info;
                var feeToPay = totalFee;

                if (userInfo == null)
                {
                    throw new Exception("User Info not found!");
                }
                if (userInfo.AccountBalance < feeToPay)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Account balance is not enough", true);
                    return BadRequest("Account balance not enough!");
                }
                userInfo.AccountBalance -= feeToPay;
                _repository.UserInfo.UpdateInfo(userInfo);

                ticket.Status = TicketStatus.CheckedOut;

                #endregion Parking payment

                _repository.Ticket.UpdateTicket(ticket);

                var carPark = await _repository.CarPark.GetCarParkByIdAsync(ticket.CarParkId);
                carPark.AvailableSlotsCount++;
                if (carPark.AvailableSlotsCount > carPark.MaxCapacity)
                {
                    carPark.AvailableSlotsCount = carPark.MaxCapacity;
                }
                _repository.CarPark.UpdateCarPark(carPark);

                #region Increase parking count

                var parking = await _repository.Parking.GetParking(currentUserId, carPark.Id);
                if (parking == null)
                {
                    parking = new Parking
                    {
                        AccountId = currentUserId,
                        CarParkId = carPark.Id,
                        LastVisited = checkoutTime,
                        UseCount = 1
                    };
                    await _repository.Parking.CreateParking(parking);
                }
                else
                {
                    parking.UseCount++;
                    parking.LastVisited = checkoutTime;
                    _repository.Parking.UpdateParking(parking);
                }

                #endregion Increase parking count

                #region Create transaction

                Transaction transaction = new Transaction
                {
                    AccountId = currentUserId,
                    Amount = feeToPay,
                    TicketId = ticket.Id,
                    Time = DateTime.UtcNow,
                    TransactionType = TransactionType.ParkingFee
                };

                await _repository.Transaction.CreateTransaction(transaction);

                #endregion Create transaction

                await _repository.Save();

                #region Open Barrier

                var prevServoExit = _firebaseService.GetServoValue("servoExit_prev");
                if (!prevServoExit.Equals("\"OPEN\""))
                    _firebaseService.OpenServo("servoExit");

                #endregion Open Barrier

                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<TicketGetDTO>(ticket), "Checked out successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}