using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.PriceTableDTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Utilities;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class PriceTableDetailsController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    public readonly IMapper _mapper;

    public PriceTableDetailsController(IRepositoryManager repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [Authorize]
    public async Task<IActionResult> GetPriceTableDetailsByCarParkId([Required] Guid carParkId)
    {
        try
        {
            #region Check user active

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || currentUser.Active == false)
            {
                return Forbid();
            }

            #endregion Check user active

            var detailsFromDb = await _repository.PriceTableDetail.GetPriceTableDetailsByCarParkId(carParkId).ToListAsync();
            var result = _mapper.Map<IEnumerable<PriceTableDetailDTO>>(detailsFromDb);
            return new SuccessResponse(Helpers.SuccessStatusCode.Ok, result, "Loaded price table details successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPut("{id}")]
    [Authorize(Roles = $"{AppConstants.OwnerRole}, {AppConstants.StaffRole}")]
    public async Task<IActionResult> UpdatePriceTableDetail(int id, [FromBody] PriceTableDetailUpdateDTO detail)
    {
        if (id != detail.Id)
        {
            return BadRequest("Invalid ID!");
        }
        try
        {
            #region Check user active

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || currentUser.Active == false || !currentUser.CarParkId.HasValue)
                return Forbid();

            #endregion Check user active

            #region Check car park active

            var carPark = await _repository.CarPark.GetCarParkByIdAsync(currentUser.CarParkId.Value);
            if (carPark.Status == Utilities.Enums.CarParkStatus.Removed)
                return BadRequest("Car park not active!");

            #endregion Check car park active

            #region Map entity

            var detailToUpdate = await _repository.PriceTableDetail.GetPriceTableDetailById(id);
            if (detailToUpdate == null)
                return NotFound();

            DateTime fromTime = DateTime.Parse(detail.FromTime);
            DateTime toTime = DateTime.Parse(detail.ToTime);
            if (fromTime > toTime)
                return BadRequest("Invalid time interval");

            detailToUpdate.Fee = detail.Fee;
            detailToUpdate.FromTime = fromTime.TimeOfDay;
            detailToUpdate.ToTime = toTime.TimeOfDay;

            #endregion Map entity

            #region Check duplicated time interval

            var duplicatedDetail = await _repository.PriceTableDetail
                .FindByCondition(d => d.FromTime.CompareTo(detailToUpdate.FromTime) == 0
                    && d.ToTime.CompareTo(detailToUpdate.ToTime) == 0
                    && d.CarParkId.Equals(detailToUpdate.CarParkId)
                    && d.Id != detailToUpdate.Id, false)
                .FirstOrDefaultAsync();

            if (duplicatedDetail != null)
                return BadRequest("Another price table detail with the same time interval exists!");

            #endregion Check duplicated time interval

            #region Update data

            _repository.PriceTableDetail.UpdatePriceTableDetail(detailToUpdate);
            var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carPark.Id);
            priceTable.LastModified = DateTime.UtcNow;
            _repository.PriceTable.UpdatePriceTable(priceTable);

            await _repository.Save();

            #endregion Update data

            return new SuccessResponse(Helpers.SuccessStatusCode.Ok, _mapper.Map<PriceTableDetailDTO>(detailToUpdate), "Updated price table detail successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost, Authorize(Roles = $"{AppConstants.OwnerRole}, {AppConstants.StaffRole}")]
    public async Task<IActionResult> CreatePriceTableDetail([FromBody] PriceTableDetailCreateDTO detail)
    {
        try
        {
            #region Check user active

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || currentUser.Active == false || !currentUser.CarParkId.HasValue)
                return Forbid();

            #endregion Check user active

            #region Check car park active

            var carPark = await _repository.CarPark.GetCarParkByIdAsync(currentUser.CarParkId.Value);
            if (carPark.Status == Utilities.Enums.CarParkStatus.Removed)
                return BadRequest("Car park not active!");

            #endregion Check car park active

            #region Get Car park price table

            var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carPark.Id);
            if (priceTable == null)
                return BadRequest("Price table has not been created!");

            #endregion Get Car park price table

            #region Map entity

            DateTime fromTime = DateTime.Parse(detail.FromTime);
            DateTime toTime = DateTime.Parse(detail.ToTime);
            if (fromTime > toTime)
                return BadRequest("Invalid time interval");

            PriceTableDetail entity = new PriceTableDetail
            {
                CarParkId = carPark.Id,
                Fee = detail.Fee,
                FromTime = fromTime.TimeOfDay,
                ToTime = toTime.TimeOfDay
            };

            #endregion Map entity

            #region Check duplicated time interval

            var duplicatedDetail = await _repository.PriceTableDetail
                .FindByCondition(d => d.FromTime.CompareTo(entity.FromTime) == 0
                    && d.ToTime.CompareTo(entity.ToTime) == 0
                    && d.CarParkId.Equals(entity.CarParkId), false)
                .FirstOrDefaultAsync();

            if (duplicatedDetail != null)
                return BadRequest("A Price table detail with the same time interval exists!");

            #endregion Check duplicated time interval

            entity = await _repository.PriceTableDetail.CreateAsync(entity);
            await _repository.Save();
            return new SuccessResponse(Helpers.SuccessStatusCode.Created, _mapper.Map<PriceTableDetailDTO>(entity), "Created price table detail successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(Roles = $"{AppConstants.OwnerRole}, {AppConstants.StaffRole}")]
    public async Task<IActionResult> DeletePriceTableDetail(int id)
    {
        try
        {
            #region Check user active

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || currentUser.Active == false || !currentUser.CarParkId.HasValue)
                return Forbid();

            #endregion Check user active

            #region Get Price Table Detail

            var detail = await _repository.PriceTableDetail.GetPriceTableDetailById(id);
            if (detail == null)
                return NotFound();
            if (!detail.CarParkId.Equals(currentUser.CarParkId))
            {
                return Forbid("This detail is not in your car park's price table");
            }

            #endregion Get Price Table Detail

            _repository.PriceTableDetail.Delete(detail);
            await _repository.Save();
            return NoContent();
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}