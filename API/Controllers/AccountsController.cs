﻿using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.AccountDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private readonly IMapper _mapper;

        public AccountsController(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = AppConstants.AdminRole)]
        public IActionResult GetAccounts(int? role, bool? active, Guid? carParkID, int? pageSize, int pageIndex = 1, bool includeCarPark = false)
        {
            try
            {
                IQueryable<Account> accountsFromDb = _repository.Account.GetAccounts(includeCarPark).Include(acc => acc.Role);

                #region Filter Data

                if (role.HasValue)
                {
                    accountsFromDb = accountsFromDb.Where(acc => acc.RoleId == role.Value);
                }

                if (active.HasValue)
                {
                    accountsFromDb = accountsFromDb.Where(acc => acc.Active == active);
                }
                if (carParkID.HasValue)
                {
                    accountsFromDb = accountsFromDb.Where(acc => acc.CarParkId.Equals(carParkID.Value));
                }

                #endregion Filter Data

                #region Pagination

                if (!pageSize.HasValue)
                {
                    pageSize = accountsFromDb.Count();
                }
                PaginatedList<Account> pagedList = PaginatedList<Account>.Create(accountsFromDb, pageSize.Value, pageIndex);

                #endregion Pagination

                var result = _mapper.Map<IEnumerable<AccountGetDTO>>(pagedList);
                return new SuccessResponse(SuccessStatusCode.Ok, new { accounts = result, totalPage = pagedList.TotalPage }, "Accounts loaded successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("employees")]
        [Authorize(Roles = AppConstants.AdminRole)]
        public IActionResult GetAccountsNotCustomer(Guid? carParkId, bool? active, int? role, int? pageSize, int pageIndex = 1, bool includeCarPark = false)
        {
            try
            {
                IQueryable<Account> accountsFromDb
                    = _repository.Account.GetAccounts(includeCarPark)
                        .Include(acc => acc.Role)
                        .Where(acc => acc.RoleId != AppConstants.CustomerRoleId);

                #region Filter Data

                if (carParkId.HasValue)
                {
                    accountsFromDb = accountsFromDb.Where(acc => acc.CarParkId.Equals(carParkId.Value));
                }

                if (active.HasValue)
                {
                    accountsFromDb = accountsFromDb.Where(acc => acc.Active == active);
                }

                if (role.HasValue)
                {
                    accountsFromDb = accountsFromDb.Where(acc => acc.RoleId == role);
                }

                #endregion Filter Data

                #region Pagination

                if (!pageSize.HasValue)
                {
                    pageSize = accountsFromDb.Count();
                }
                PaginatedList<Account> pagedList = PaginatedList<Account>.Create(accountsFromDb, pageSize.Value, pageIndex);

                #endregion Pagination

                var result = _mapper.Map<IEnumerable<AccountGetDTO>>(pagedList);
                return new SuccessResponse(SuccessStatusCode.Ok, new { accounts = result, totalPage = pagedList.TotalPage }, "Accounts loaded successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = AppConstants.AdminRole)]
        public async Task<IActionResult> GetAccountByIdAsync(string id)
        {
            try
            {
                var acc = await _repository.Account.GetAccountByIdAsync(id);
                if (acc == null)
                {
                    return NotFound();
                }
                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<AccountGetDTO>(acc), "Account loaded successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = AppConstants.AdminRole)]
        public async Task<IActionResult> CreateAccountAsync([FromBody] AccountCreateDTO account)
        {
            if (!ModelState.IsValid)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
                return BadRequest(ModelState);
            }

            try
            {
                //check duplicated phone number
                bool duplicatedPhoneNumber = await _repository.Account.GetAccounts(false)
                 .AnyAsync(acc => acc.PhoneNumber.Equals(account.PhoneNumber) && acc.Active == true);
                if (duplicatedPhoneNumber)
                {
                    return BadRequest("Phone number is used!");
                }

                //check if role is customer
                if (account.RoleId == AppConstants.CustomerRoleId)
                {
                    return BadRequest("Creating a customer account is not permitted!");
                }

                #region Map entity

                Account entity = new Account
                {
                    PhoneNumber = account.PhoneNumber,
                    RoleId = account.RoleId,
                    Password = account.Password,
                    CanEditCarPark = false,
                    CanEdit = false,
                    Active = true
                };

                #endregion Map entity

                #region Generate ID

                string id = AccountUtils.GenerateAccountID(entity.RoleId);
                var existAccountId = await _repository.Account.GetAccountByIdAsync(id);
                while (existAccountId != null)
                {
                    id = AccountUtils.GenerateAccountID(entity.RoleId);
                    existAccountId = await _repository.Account.GetAccountByIdAsync(id);
                }
                entity.Id = id;
                if (entity.RoleId == AppConstants.OwnerRoleId)
                {
                    entity.CanEdit = true;
                    entity.CanEditCarPark = true;
                }
                else if (entity.RoleId == AppConstants.AdminRoleId)
                {
                    entity.CanEdit = false;
                    entity.CanEditCarPark = true;
                }

                #endregion Generate ID

                var result = await _repository.Account.CreateAccountAsync(entity);
                await _repository.Save();

                return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<AccountGetDTO>(result), "Account created successfully!");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
                // return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = AppConstants.AdminRole)]
        public async Task<IActionResult> ToggleActiveAccount(string id)
        {
            try
            {
                #region Check current account deactivated

                string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
                if (currentUser == null || currentUser.Active == false)
                {
                    return Forbid();
                }

                #endregion Check current account deactivated

                var account = await _repository.Account.GetAccountByIdAsync(id);
                if (account == null) return NotFound();

                if (account.Id.Equals(currentUser.Id))
                {
                    return BadRequest("Deactivating own account is not permitted!");
                }

                if (account.RoleId == AppConstants.AdminRoleId)
                {
                    return BadRequest("Deactivating admin account is not permitted!");
                }

                if (account.Active == true)
                {
                    account.Active = false;
                    account.CarParkId = null;
                    account.CanEdit = false;
                    account.CanEditCarPark = false;
                }
                else
                {
                    account.Active = true;
                }
                _repository.Account.UpdateAccount(account);
                await _repository.Save();
                return new SuccessResponse(SuccessStatusCode.NoContent, null, "");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = AppConstants.AdminRole)]
        public async Task<IActionResult> UpdateAccount([Required] string id, [FromBody] AccountUpdateDTO account)
        {
            if (!ModelState.IsValid)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
                return BadRequest(ModelState);
            }
            if (!id.Equals(account.Id))
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Invalid ID", true);
                return BadRequest("Invalid ID");
            }
            try
            {
                var accountToUpdate = await _repository.Account.GetAccountByIdAsync(id, false);
                if (accountToUpdate == null)
                {
                    return NotFound();
                }
                if (accountToUpdate.RoleId == AppConstants.CustomerRoleId)
                {
                    return Forbid("You are not allowed to update a customer account");
                    // return new ErrorResponse(ErrorStatusCode.Forbidden, "You are not allowed to update a customer account", true);
                }
                if (account.RoleId == AppConstants.CustomerRoleId || account.RoleId == AppConstants.AdminRoleId)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Account role cannot be set to customer", true);
                    return BadRequest("Account role cannot be set to customer or admin");
                }
                //check duplicated phoneNumber
                var duplicatedPhoneNumber
                    = await _repository.Account.GetAccountByPhoneNumber(account.PhoneNumber, false);
                if (duplicatedPhoneNumber != null && !duplicatedPhoneNumber.Id.Equals(id))
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "This phone number is used!", true);
                    return BadRequest("Phone number is used!");
                }

                // accountToUpdate.Password = AccountUtils.HashPasswordWithProvidedSalt(account.Password, accountToUpdate.Salt);
                accountToUpdate.PhoneNumber = account.PhoneNumber;
                accountToUpdate.RoleId = account.RoleId;
                if (accountToUpdate.RoleId == AppConstants.OwnerRoleId)
                {
                    accountToUpdate.CanEdit = true;
                }
                _repository.Account.UpdateAccount(accountToUpdate);
                await _repository.Save();
                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<AccountGetDTO>(accountToUpdate), "Updated account successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("profile")]
        [Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.StaffRole}, {AppConstants.OwnerRole}")]
        public async Task<IActionResult> GetProfile()
        {
            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            try
            {
                var profile = await _repository.Account.GetAccountByIdAsync(currentUserId);
                if (profile == null)
                {
                    // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                    return Forbid();
                }
                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<AccountGetDTO>(profile), "Loaded profile successfully!");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPatch("update-password")]
        [Authorize]
        public async Task<IActionResult> UpdatePassword([FromBody] ChangePasswordDTO dTO)
        {
            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            try
            {
                var account = await _repository.Account.GetAccountByIdAsync(currentUserId);
                if (account == null || !account.Active.Value)
                {
                    // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                    return Forbid();
                }

                #region Check if old password matches

                var hashedOldPassword = AccountUtils.HashPasswordWithProvidedSalt(dTO.OldPassword, account.Salt);
                if (!hashedOldPassword.Equals(account.Password))
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Old password does not match!", true);
                    return BadRequest("Old password does not match");
                }

                #endregion Check if old password matches

                #region Check new password and confirm password match

                if (!dTO.NewPassword.Equals(dTO.ConfirmPassword))
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Confirm password does not match!", true);
                    return BadRequest("Confirm password does not match!");
                }

                #endregion Check new password and confirm password match

                #region Update password

                account.Password = AccountUtils.HashPasswordWithProvidedSalt(dTO.NewPassword, account.Salt);
                _repository.Account.UpdateAccount(account);
                await _repository.Save();

                #endregion Update password

                return new SuccessResponse(SuccessStatusCode.NoContent, "", "");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("toggle-can-edit/{id}")]
        [Authorize(Roles = AppConstants.OwnerRole)]
        public async Task<IActionResult> ToggleCanEdit(string id)
        {
            try
            {
                #region Check current account disabled

                string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
                if (currentUser == null || currentUser.Active.Value == false || currentUser.CarParkId == null)
                {
                    return Forbid();
                }

                #endregion Check current account disabled

                #region Find staff by phone number

                var staff = await _repository.Account.GetAccountByIdAsync(id);
                if (staff == null)
                    return BadRequest("Staff not found!");
                if (!staff.Active.Value)
                    return BadRequest("Staff account is disabled!");
                if (staff.RoleId != AppConstants.StaffRoleId)
                    return BadRequest("This account is not a staff");
                if (staff.CarParkId == null || !staff.CarParkId.Equals(currentUser.CarParkId))
                    return BadRequest("Staff does not belong to your car park");

                #endregion Find staff by phone number

                #region Toggle can edit

                staff.CanEdit = !staff.CanEdit;
                _repository.Account.UpdateAccount(staff);
                await _repository.Save();

                #endregion Toggle can edit

                return new SuccessResponse(SuccessStatusCode.Ok, new { CanEdit = staff.CanEdit }, "Toggled successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("toggle-can-edit-car-park/{id}")]
        [Authorize(Roles = AppConstants.OwnerRole)]
        public async Task<IActionResult> ToggleCanEditCarPark(string id)
        {
            try
            {
                #region Check current account disabled

                string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
                if (currentUser == null || currentUser.Active.Value == false || currentUser.CarParkId == null)
                {
                    return Forbid();
                }

                #endregion Check current account disabled

                #region Find staff by Id

                var staff = await _repository.Account.GetAccountByIdAsync(id);
                if (staff == null)
                    return BadRequest("Staff not found!");
                if (!staff.Active.Value)
                    return BadRequest("Staff account is disabled!");
                if (staff.RoleId != AppConstants.StaffRoleId)
                    return BadRequest("This account is not a staff");
                if (staff.CarParkId == null || !staff.CarParkId.Equals(currentUser.CarParkId))
                    return BadRequest("Staff does not belong to your car park");

                #endregion Find staff by Id

                #region Toggle can edit

                staff.CanEditCarPark = !staff.CanEditCarPark;
                _repository.Account.UpdateAccount(staff);
                await _repository.Save();

                #endregion Toggle can edit

                return new SuccessResponse(SuccessStatusCode.Ok, new { CanEditCarPark = staff.CanEditCarPark }, "Toggled successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("forgot-password/{phoneNumber}")]
        public async Task<IActionResult> CheckPhoneNumberExists([RegularExpression("^\\d{10,14}$")] string phoneNumber)
        {
            try
            {
                var phoneNumberExists = await _repository.Account.GetAccounts()
                    .AnyAsync(acc => acc.PhoneNumber.Equals(phoneNumber)
                        && acc.Active == true);
                return new SuccessResponse(SuccessStatusCode.Ok, phoneNumberExists, "Checked phone number successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("forgot-password/update-password")]
        public async Task<IActionResult> UpdateForgotPassword([FromBody] ForgotPasswordDTO dTO)
        {
            try
            {
                var account = await _repository.Account.GetAccountByPhoneNumber(dTO.PhoneNumber, false);
                if (account == null)
                {
                    return BadRequest("Phone number not found!");
                }
                account.Password = AccountUtils.HashPasswordWithProvidedSalt(dTO.Password, account.Salt);
                _repository.Account.UpdateAccount(account);
                await _repository.Save();
                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<AccountGetDTO>(account), "Updated password successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}