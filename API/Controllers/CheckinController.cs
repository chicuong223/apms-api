﻿using API.Helpers;
using API.Responses.SuccessResponses;
using API.SignalRHubs;
using AutoMapper;
using DTOs.TicketDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Utilities;
using Utilities.Enums;

namespace API.Controllers
{
    [Route("api/check-in")]
    [ApiController]
    public class CheckinController : ControllerBase
    {
        private readonly IRepositoryManager _repository;

        private readonly IHubContext<CheckinHub> _checkinHub;

        private readonly IMapper _mapper;

        private readonly FirebaseService _firebaseService;

        public CheckinController(IRepositoryManager repository, IHubContext<CheckinHub> checkinHub, IMapper mapper, FirebaseService firebaseService)
        {
            _repository = repository;
            _checkinHub = checkinHub;
            _mapper = mapper;
            _firebaseService = firebaseService;
        }

        [HttpPost]
        public async Task<IActionResult> ScanPlateNumber([Required] string plateNumber, [Required] string picInUrl, [Required] Guid carParkId)
        {
            try
            {
                var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId);
                if (carPark == null)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not found", true);
                    return BadRequest("Car Park not found!");
                }
                bool ticketExist = await _repository.Ticket.GetTickets()
                    .AnyAsync(t => t.PlateNumber.Equals(plateNumber)
                        && t.CarParkId.Equals(carParkId)
                        && t.Status == TicketStatus.CheckedIn);

                return new SuccessResponse(SuccessStatusCode.Ok, new { PlateNumber = plateNumber, CarParkId = carParkId, Pic = picInUrl, Exist = ticketExist }, "Scan plate number successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("create-ticket")]
        [Authorize(Roles = AppConstants.CustomerRole)]
        public async Task<IActionResult> CheckIn([FromBody] CheckinDTO dto, [FromQuery] bool openBarrier = true)
        {
            Ticket result;
            try
            {
                var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var account = await _repository.Account.GetAccounts()
                                .Include(acc => acc.Info)
                                .Where(acc => acc.Id.Equals(currentUserId)
                                    && acc.RoleId == AppConstants.CustomerRoleId)
                                .SingleOrDefaultAsync();
                if (account == null || !account.Active.Value)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Customer not found", true);
                    return Forbid();
                }

                // #region Check if account is checked in

                // var accountCheckedIn = await _repository.Ticket.GetTickets()
                //     .AnyAsync(t => t.AccountId.Equals(currentUserId) && t.Status == TicketStatus.CheckedIn);
                // if (accountCheckedIn)
                // {
                //     // return new ErrorResponse(ErrorStatusCode.BadRequest, "This customer is checked in at a car park!");
                //     return BadRequest("This customer has checked in and not checked out");
                // }

                // #endregion Check if account is checked in

                #region Check if vehicle is having a Ticket that paid = false

                var ticketExist = await _repository.Ticket.GetTickets()
                     .AnyAsync(b => b.PlateNumber.Equals(dto.PlateNumber)
                         && (b.Status == TicketStatus.CheckedIn));
                if (ticketExist)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "This vehicle is having another unpaid ticket", true);
                    return BadRequest("This vehicle is checked in and not checked out");
                }

                #endregion Check if vehicle is having a Ticket that paid = false

                #region Create Ticket

                /**
                 * if booking exists: update booking status to CheckedIn
                 * else: create a ticket and decrement available slots
                 */;
                var booking = await _repository.Ticket.GetTickets()
                    .Where(booking => booking.CarParkId.Equals(dto.CarParkId)
                        && booking.PlateNumber.Equals(dto.PlateNumber)
                        && booking.AccountId.Equals(currentUserId)
                        && booking.Status == TicketStatus.Pending)
                    .OrderBy(booking => booking.ArriveTime)
                    .FirstOrDefaultAsync();
                var carPark = await _repository.CarPark.GetCarParkByIdAsync(dto.CarParkId);
                var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carPark.Id, true);

                if (carPark == null || carPark.Status == CarParkStatus.Removed)
                {
                    return BadRequest("Car park not found!");
                }
                if (booking != null)
                {
                    booking.Status = TicketStatus.CheckedIn;
                    booking.StartTime = DateTime.UtcNow;
                    booking.PicInUrl = dto.PlateNumberImageUrl;

                    //Check if is late, add overdue fee
                    double overdueMinutes = DateTime.UtcNow.Subtract(booking.ArriveTime.Value).TotalMinutes;
                    if (overdueMinutes > 5 && overdueMinutes <= 30)
                    {
                        decimal overdueFee = 0;
                        if (priceTable != null)
                        {
                            if (priceTable.Details.Count > 0)
                            {
                                var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                                foreach (var detail in priceTable.Details)
                                {
                                    if (ParkingFeeUtilities
                                        .CheckTimeBetweenAnInterval(detail.FromTime, detail.ToTime,
                                            TimeZoneInfo.ConvertTimeFromUtc(booking.ArriveTime.Value, timeZone)
                                            .TimeOfDay) == true)
                                    {
                                        overdueFee = detail.Fee;
                                        break;
                                    }
                                }
                            }
                            else if (priceTable.FeePerHour.HasValue)
                            {
                                overdueFee = priceTable.FeePerHour.Value;
                            }
                        }

                        booking.OverdueFee = overdueFee;
                    }

                    _repository.Ticket.UpdateTicket(booking);
                    result = booking;
                }
                else
                {
                    if (carPark.AvailableSlotsCount <= 0)
                    {
                        return BadRequest("There are no slots left in this car park!");
                    }

                    #region Create Ticket

                    //Get price table
                    string priceTableStr = "";
                    if (priceTable != null)
                    {
                        priceTableStr = SerializationUtils.SerializeObject(priceTable, typeof(PriceTable));
                    }
                    var ticket = new Ticket
                    {
                        AccountId = currentUserId,
                        CarParkId = dto.CarParkId,
                        Status = TicketStatus.CheckedIn,
                        PicInUrl = dto.PlateNumberImageUrl,
                        StartTime = DateTime.UtcNow,
                        PlateNumber = dto.PlateNumber,
                        PriceTable = priceTableStr
                    };
                    await _repository.Ticket.CreateTicketAsync(ticket);

                    #endregion Create Ticket

                    #region decrement available slots

                    carPark.AvailableSlotsCount--;
                    _repository.CarPark.UpdateCarPark(carPark);

                    #endregion decrement available slots

                    result = ticket;
                }

                #endregion Create Ticket

                #region Check license plate exists, if not add to database

                var licensePlate = await _repository.LicensePlate.GetLicensePlate(result.PlateNumber, currentUserId);
                if (licensePlate == null)
                {
                    licensePlate = new LicensePlate
                    {
                        PlateNumber = result.PlateNumber,
                        AccountId = currentUserId,
                        UsageCount = 1
                    };
                    await _repository.LicensePlate.CreateLicensePlate(licensePlate);
                }
                else
                {
                    licensePlate.UsageCount++;
                    _repository.LicensePlate.UpdateLicensePlate(licensePlate);
                }

                #endregion Check license plate exists, if not add to database

                await _repository.Save();

                #region Send SignalR

                await _checkinHub.Clients.All.SendAsync("NewCheckin", dto.PlateNumber, dto.CarParkId, dto.PlateNumberImageUrl);

                #endregion Send SignalR

                if (openBarrier == true)
                {
                    #region Open barrier

                    var prevServo = _firebaseService.GetServoValue("servo_prev");
                    if (!prevServo.Equals("\"OPEN\""))
                    {
                        _firebaseService.OpenServo("servo");
                    }

                    #endregion Open barrier
                }

                #region Send SignalR

                await _checkinHub.Clients.All.SendAsync("NewCheckin", dto.PlateNumber, carPark.Id, dto.PlateNumberImageUrl, dto.PlateNumberImageUrl);

                #endregion Send SignalR

                result.Account = await _repository.Account.GetAccountByIdAsync(currentUserId);
                return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<TicketGetDTO>(result), "Created Ticket successfully!");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                // return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}