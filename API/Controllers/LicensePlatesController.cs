using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Repositories.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Utilities;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class LicensePlatesController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    private readonly IMapper _mapper;

    public LicensePlatesController(IRepositoryManager repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [Authorize(Roles = AppConstants.AdminRole)]
    public async Task<IActionResult> GetLicensePlates(int? pageSize, int pageIndex = 1)
    {
        try
        {
            #region Check account

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
                return Forbid();

            #endregion Check account

            var licensePlatesFromDb = _repository.LicensePlate.GetLicensePlates();

            #region Pagination

            if (!pageSize.HasValue)
                pageSize = licensePlatesFromDb.Count();
            PaginatedList<LicensePlate> pagedList = PaginatedList<LicensePlate>.Create(licensePlatesFromDb, pageSize.Value, pageIndex);

            #endregion Pagination

            return new SuccessResponse(SuccessStatusCode.Ok, new { LicensePlates = pagedList, TotalPage = pagedList.TotalPage }, "Loaded license plates successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("used-plate-numbers")]
    [Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> GetOwnPlateNumbers(int? pageSize, int pageIndex = 1)
    {
        try
        {
            #region Get account

            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
                return Forbid();

            #endregion Get account

            #region Get Plate numbers

            var plateNumbers = _repository.LicensePlate.GetPlateNumbersByAccountId(currentUser.Id);

            #endregion Get Plate numbers

            #region Pagination

            if (!pageSize.HasValue)
                pageSize = plateNumbers.Count();
            PaginatedList<string> pagedList = PaginatedList<string>.Create(plateNumbers, pageSize.Value, pageIndex);

            #endregion Pagination

            return new SuccessResponse(SuccessStatusCode.Ok, new { PlateNumbers = plateNumbers, TotalPage = pagedList.TotalPage }, "Loaded plate numbers successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpDelete]
    [Authorize(Roles = AppConstants.AdminRole)]
    public async Task<IActionResult> RemoveLicensePlate([FromQuery][Required] string plateNumber, [FromQuery][Required] string accountId)
    {
        try
        {
            #region Get account

            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
                return Forbid();

            #endregion Get account

            #region Get and delete license plate

            var licensePlate = await _repository.LicensePlate.GetLicensePlate(plateNumber, accountId);
            if (licensePlate == null)
                return NotFound();
            _repository.LicensePlate.RemoveLicensePlate(licensePlate);
            await _repository.Save();

            #endregion Get and delete license plate

            return new SuccessResponse(SuccessStatusCode.NoContent, null, "");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}