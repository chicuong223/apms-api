﻿using API.Helpers;
using API.Responses.SuccessResponses;
using DTOs.AccountDTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Repositories.Interfaces;
using Utilities;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IRepositoryManager repository;
        private JwtService jwtService;

        public AuthenticationController(IRepositoryManager repository, IOptions<JwtSettings> jwtSettings)
        {
            this.repository = repository;
            jwtService = new JwtService(jwtSettings);
        }

        [HttpPost("customer")]
        public async Task<IActionResult> CustomerLogin([FromBody] CustomerLoginDTO account)
        {
            if (!ModelState.IsValid)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
                return BadRequest(ModelState);
            }
            try
            {
                var entity = await repository.Account.GetAccountByPhoneNumber(account.PhoneNumber, false);
                if (entity == null)
                {
                    throw new ArgumentException("Incorrect phone number or password!");
                }
                string hashedPassword = AccountUtils.HashPasswordWithProvidedSalt(account.Password, entity.Salt);
                if (!string.Equals(hashedPassword, entity.Password))
                {
                    throw new ArgumentException("Incorrect phone number or password!");
                }
                if (!entity.Active.Value)
                {
                    throw new ArgumentException("Account is inactive!");
                }
                var token = jwtService.GenerateJwtToken(entity);
                return new SuccessResponse(SuccessStatusCode.Ok, token, "Logged in successfully!");
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                {
                    return Unauthorized(ex.Message);
                    // return new ErrorResponse(ErrorStatusCode.Unauthorized, "Bad credentials!", true);
                }
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, "Error logging in");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] AccountLoginDTO account)
        {
            if (!ModelState.IsValid)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
                return BadRequest(ModelState);
            }
            try
            {
                var entity = await repository.Account.GetAccountByIdAsync(account.Id);
                if (entity == null)
                {
                    throw new ArgumentException("Incorrect ID or password!");
                }
                if (entity.RoleId == AppConstants.CustomerRoleId)
                {
                    throw new ArgumentException("Incorrect ID or password!");
                }
                string hashedPassword = AccountUtils.HashPasswordWithProvidedSalt(account.Password, entity.Salt);
                if (!string.Equals(hashedPassword, entity.Password))
                {
                    throw new ArgumentException("Incorrect ID or password!");
                }
                if (!entity.Active.Value)
                {
                    throw new ArgumentException("Account is inactive!");
                }
                if ((entity.RoleId == AppConstants.StaffRoleId || entity.RoleId == AppConstants.OwnerRoleId)
                    && entity.CarParkId == null)
                {
                    throw new ArgumentException("This account does not belong to any car park!");
                }
                var token = jwtService.GenerateJwtToken(entity);
                return new SuccessResponse(SuccessStatusCode.Ok, token, "Logged in successfully!");
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                {
                    return Unauthorized(ex.Message);
                    // return new ErrorResponse(ErrorStatusCode.Unauthorized, "Bad credentials!", true);
                }
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, "Error logging in");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}