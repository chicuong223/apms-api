using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.FeedbackDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Repositories.Interfaces;
using System.Security.Claims;
using Utilities;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FeedbacksController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    private readonly IMapper _mapper;

    public FeedbacksController(IRepositoryManager repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [Authorize]
    public IActionResult GetFeedbacks(int? pageSize, int pageIndex = 1)
    {
        try
        {
            string role = User.FindFirstValue(ClaimTypes.Role);
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            IQueryable<Feedback> feedbacksFromDb;
            switch (role)
            {
                case AppConstants.CustomerRole:
                    feedbacksFromDb = _repository.Feedback.GetFeedbacks(include: true)
                        .Where(feedback => feedback.AccountId.Equals(currentUserId));
                    break;

                default:
                    feedbacksFromDb = _repository.Feedback.GetFeedbacks(include: true);
                    break;
            };

            #region Pagination

            if (!pageSize.HasValue)
            {
                pageSize = feedbacksFromDb.Count();
            }
            PaginatedList<Feedback> pagedList = PaginatedList<Feedback>.Create(feedbacksFromDb, pageSize.Value, pageIndex);

            #endregion Pagination

            return new SuccessResponse(SuccessStatusCode.Ok,
                new { Feedbacks = _mapper.Map<IEnumerable<FeedbackGetDTO>>(pagedList), TotalPage = pagedList.TotalPage },
                "Loaded feedbacks successfully");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("{id}")]
    [Authorize]
    public async Task<IActionResult> GetFeedbackByIdAsync(Guid id)
    {
        var currentRole = User.FindFirstValue(ClaimTypes.Role);
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var feedback = await _repository.Feedback.GetFeedbackByIdAsync(id, include: true);
            if (feedback == null)
            {
                return NotFound();
            }
            if (currentRole.Equals(AppConstants.CustomerRole) && !feedback.AccountId.Equals(currentUserId))
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<FeedbackGetDTO>(feedback), "Get feedback successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost]
    [Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> CreateFeedback([FromBody] FeedbackCreateDTO dTO)
    {
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            var feedback = new Feedback
            {
                AccountId = currentUserId,
                Description = dTO.Description,
                Time = DateTime.UtcNow
            };
            await _repository.Feedback.CreateFeedbackAsync(feedback);
            await _repository.Save();
            feedback.Account = currentUser;
            return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<FeedbackGetDTO>(feedback), "Created feedback successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPut("{id}")]
    [Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> UpdateFeedback(Guid id, [FromBody] FeedbackUpdateDTO dTO)
    {
        if (!id.Equals(dTO.Id))
        {
            // return new ErrorResponse(ErrorStatusCode.BadRequest, "Invalid ID", true);
            return BadRequest("Invalid ID");
        }
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            var feedback = await _repository.Feedback.GetFeedbackByIdAsync(id, include: true);
            if (feedback == null)
            {
                return NotFound();
            }
            if (!feedback.AccountId.Equals(currentUserId))
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            feedback.Description = dTO.Description;
            feedback.Time = DateTime.UtcNow;
            _repository.Feedback.UpdateFeedback(feedback);
            await _repository.Save();
            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<FeedbackGetDTO>(feedback), "Updated feedback successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.CustomerRole}")]
    public async Task<IActionResult> DeleteFeedback(Guid id)
    {
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            var feedback = await _repository.Feedback.GetFeedbackByIdAsync(id);
            if (feedback == null)
            {
                return NotFound();
            }
            var currentRole = User.FindFirstValue(ClaimTypes.Role);
            if (currentRole.Equals(AppConstants.CustomerRole) && !feedback.AccountId.Equals(currentUserId))
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            _repository.Feedback.RemoveFeedback(feedback);
            await _repository.Save();
            return new SuccessResponse(SuccessStatusCode.NoContent, "", "");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}