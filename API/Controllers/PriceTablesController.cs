using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.PriceTableDTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.Collections.ObjectModel;
using System.Security.Claims;
using Utilities;
using Utilities.Enums;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class PriceTablesController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    private readonly IMapper _mapper;

    public PriceTablesController(IRepositoryManager repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet, Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.CustomerRole}")]
    public IActionResult GetPriceTables(int? pageSize, int pageIndex = 1, bool includeDetails = false)
    {
        try
        {
            IQueryable<PriceTable> tablesFromDb = _repository.PriceTable.GetPriceTables(includeDetails);

            #region Pagination

            if (!pageSize.HasValue)
            {
                pageSize = tablesFromDb.Count();
            }

            PaginatedList<PriceTable> pagedList = PaginatedList<PriceTable>.Create(tablesFromDb, pageSize.Value, pageIndex);

            #endregion Pagination

            #region Mapping to DTO

            var result = _mapper.Map<IEnumerable<PriceTableGetDTO>>(pagedList);

            #endregion Mapping to DTO

            return new SuccessResponse(SuccessStatusCode.Ok, new { PriceTables = result, TotalPage = pagedList.TotalPage }, "Loaded price tables successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("{carParkId}")]
    [Authorize]
    public async Task<IActionResult> GetPriceTableByCarParkId(Guid carParkId, bool includeDetails = false)
    {
        try
        {
            var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carParkId, includeDetails);
            if (priceTable == null)
            {
                return NotFound();
            }

            #region Mapping

            var result = _mapper.Map<PriceTableGetDTO>(priceTable);

            #endregion Mapping

            return new SuccessResponse(SuccessStatusCode.Ok, result, "Loaded price table successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost]
    [Authorize(Roles = $"{AppConstants.OwnerRole}, {AppConstants.StaffRole}")]
    public async Task<IActionResult> CreatePriceTable([FromBody] PriceTableCreateDTO priceTable)
    {
        try
        {
            #region Check account

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var account = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (account == null || !account.Active.Value || account.CarParkId == null)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }

            #endregion Check account

            #region Check Car park active

            Guid carParkId = new Guid(User.FindFirstValue("CarParkId"));
            var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId);
            if (carPark == null || carPark.Status == CarParkStatus.Removed)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not found!", true);
                return BadRequest("Car park not found!");
            }

            #endregion Check Car park active

            #region Check Price Table exists

            var existsTable = await _repository.PriceTable
                .GetPriceTables().AnyAsync(t => t.CarParkId.Equals(carParkId));
            if (existsTable)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Price Table for this car park exists!", true);
                return BadRequest("Price table for your car park exists!");
            }

            #endregion Check Price Table exists

            #region Map Entity

            //map list of details
            ICollection<PriceTableDetail> details = new Collection<PriceTableDetail>();
            if (priceTable.Details != null && priceTable.Details.Count > 0)
            {
                foreach (var detail in priceTable.Details)
                {
                    //convert FromTime and ToTime to TimeSpan
                    DateTime fromTime = DateTime.Parse(detail.FromTime);
                    DateTime toTime = DateTime.Parse(detail.ToTime);
                    // if (fromTime > toTime)
                    // {
                    //     return BadRequest("Invalid time interval!");
                    // }
                    PriceTableDetail d = new PriceTableDetail
                    {
                        CarParkId = carParkId,
                        FromTime = fromTime.TimeOfDay,
                        ToTime = toTime.TimeOfDay,
                        Fee = detail.Fee
                    };
                    details.Add(d);
                }
            }

            var entity = new PriceTable
            {
                CarParkId = carParkId,
                ReservationFeePercentage = priceTable.ReservationFeePercentage,
                FeePerHour = priceTable.FeePerHour,
                Details = details,
                LastModified = DateTime.UtcNow
            };

            #endregion Map Entity

            #region Add price table to the database

            var result = await _repository.PriceTable.CreatePriceTable(entity);
            await _repository.Save();

            #endregion Add price table to the database

            #region Mapping

            var mappedResult = _mapper.Map<PriceTableGetDTO>(result);

            #endregion Mapping

            return new SuccessResponse(SuccessStatusCode.Created, mappedResult, "Created price table successfully");
        }
        catch (Exception ex)
        {
            // return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPut]
    [Authorize(Roles = $"{AppConstants.StaffRole}, {AppConstants.OwnerRole}")]
    public async Task<IActionResult> UpdatePriceTable([FromBody] PriceTableCreateDTO dTO)
    {
        try
        {
            #region Check account

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var account = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (account == null || !account.Active.Value || account.CarParkId == null)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }

            if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.StaffRole) && account.CanEditCarPark == false)
            {
                return Forbid();
            }

            #endregion Check account

            #region Check Car park active

            var carParkId = account.CarParkId;
            if (!carParkId.HasValue)
            {
                return Forbid();
            }
            var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId.Value);
            if (carPark == null || carPark.Status == CarParkStatus.Removed)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not found!", true);
                return BadRequest("Car park not found!");
            }

            #endregion Check Car park active

            var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carParkId.Value, true);
            if (priceTable == null)
            {
                return BadRequest("This car park does not have a price table yet!");
            }

            #region Update details

            if (dTO.Details.Count > 0)
            {
                //Clear price table details
                foreach (var detail in priceTable.Details)
                {
                    _repository.PriceTableDetail.RemovePriceTableDetail(detail);
                }

                //Add new details
                ICollection<PriceTableDetail> details = new Collection<PriceTableDetail>();
                foreach (var detail in dTO.Details)
                {
                    //convert FromTime and ToTime to TimeSpan
                    DateTime fromTime = DateTime.Parse(detail.FromTime);
                    DateTime toTime = DateTime.Parse(detail.ToTime);
                    // if (fromTime > toTime)
                    // {
                    //     return BadRequest($"Invalid time interval: {fromTime} - {toTime}");
                    // }
                    PriceTableDetail d = new PriceTableDetail
                    {
                        CarParkId = carParkId.Value,
                        FromTime = fromTime.TimeOfDay,
                        ToTime = toTime.TimeOfDay,
                        Fee = detail.Fee
                    };
                    details.Add(d);
                };

                priceTable.Details = details;
            }

            #endregion Update details

            priceTable.ReservationFeePercentage = dTO.ReservationFeePercentage;
            priceTable.FeePerHour = dTO.FeePerHour;

            _repository.PriceTable.UpdatePriceTable(priceTable);

            await _repository.Save();

            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<PriceTableGetDTO>(priceTable), "Updated price table successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            // throw;
        }
    }
}