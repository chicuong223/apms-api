using API.Helpers;
using API.Responses.SuccessResponses;
using API.SignalRHubs;
using AutoMapper;
using DTOs.TicketDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.Security.Claims;
using Utilities;
using Utilities.Enums;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TicketsController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    private readonly IMapper _mapper;
    private readonly IHubContext<CheckinHub> _checkinHub;
    // private readonly FirebaseService _firebaseService;

    public TicketsController(IRepositoryManager repository, IMapper mapper, IHubContext<CheckinHub> checkinHub)
    {
        _repository = repository;
        _mapper = mapper;
        _checkinHub = checkinHub;
        // _firebaseService = firebaseService;
    }

    [HttpGet]
    [Authorize]
    public async Task<IActionResult> GetTickets(string accountId, DateTime? from, DateTime? to,
        Guid? carParkId, string plateNumber, TicketStatus? status,
        int? pageSize, int pageIndex = 1, bool includeCarPark = false,
        bool includePriceTable = false)
    {
        try
        {
            IQueryable<Ticket> ticketsFromDb;
            var role = User.FindFirstValue(ClaimTypes.Role);
            switch (role)
            {
                case AppConstants.AdminRole:
                    ticketsFromDb = _repository.Ticket.GetTickets(includeCarPark)
                        .Include(t => t.Account)
                        .ThenInclude(acc => acc.Info)
                        .OrderByDescending(t => t.StartTime).ThenByDescending(t => t.BookTime);
                    break;

                case AppConstants.CustomerRole:
                    var currentUserID = User.FindFirstValue(ClaimTypes.NameIdentifier);
                    ticketsFromDb = _repository.Ticket.GetTickets(includeCarPark)
                        .Include(t => t.Account)
                        .ThenInclude(acc => acc.Info)
                        .OrderByDescending(t => t.StartTime).ThenByDescending(t => t.BookTime)
                        .Where(b => b.AccountId.Equals(currentUserID));
                    break;

                default:
                    var currentCarParkId = new Guid(User.FindFirstValue("CarParkId"));
                    ticketsFromDb = _repository.Ticket.GetTickets(includeCarPark)
                        .Include(t => t.Account)
                        .ThenInclude(acc => acc.Info)
                        .Where(b => b.CarParkId.Equals(currentCarParkId));
                    break;
            }

            #region Filter Data

            if (!string.IsNullOrEmpty(accountId))
            {
                ticketsFromDb = ticketsFromDb.Where(Ticket => Ticket.AccountId.Equals(accountId));
            }
            if (from.HasValue)
            {
                ticketsFromDb = ticketsFromDb.Where(Ticket => Ticket.StartTime.Value >= from.Value.ToUniversalTime() || Ticket.BookTime.Value >= from.Value.ToUniversalTime());
            }
            if (to.HasValue)
            {
                ticketsFromDb = ticketsFromDb.Where(Ticket => Ticket.StartTime.Value <= to.Value.ToUniversalTime() || Ticket.BookTime.Value <= to.Value.ToUniversalTime());
            }
            if (carParkId.HasValue)
            {
                ticketsFromDb = ticketsFromDb.Where(Ticket => Ticket.CarParkId.Equals(carParkId.Value));
            }
            if (!string.IsNullOrWhiteSpace(plateNumber))
            {
                ticketsFromDb = ticketsFromDb.Where(Ticket => Ticket.PlateNumber.Equals(plateNumber));
            }
            if (status.HasValue)
            {
                ticketsFromDb = ticketsFromDb.Where(Ticket => Ticket.Status == status.Value);
            }

            #endregion Filter Data

            #region Calculate total Fee if ticket is checked in

            var list = await ticketsFromDb.ToListAsync();
            foreach (var ticket in list)
            {
                if (ticket.Status == TicketStatus.CheckedIn)
                {
                    PriceTable priceTable = null;
                    if (!string.IsNullOrEmpty(ticket.PriceTable))
                    {
                        priceTable = SerializationUtils.DeserializeObject<PriceTable>(ticket.PriceTable);
                    }
                    decimal totalFee = 0;
                    if (priceTable != null)
                    {
                        if (priceTable.Details.Count > 0)
                        {
                            totalFee = ParkingFeeUtilities.CalculateParkingFee(priceTable.Details, ticket.StartTime.Value, DateTime.UtcNow);
                        }
                        else if (priceTable.FeePerHour.HasValue)
                        {
                            double totalHours = Math.Ceiling(DateTime.UtcNow.Subtract(ticket.StartTime.Value).TotalHours);
                            // : Math.Floor(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                            totalFee = priceTable.FeePerHour.Value * (decimal)totalHours;
                        }
                    }
                    ticket.TotalFee = totalFee;
                }
            }

            #endregion Calculate total Fee if ticket is checked in

            #region Pagination

            if (!pageSize.HasValue)
            {
                pageSize = ticketsFromDb.Count();
            }
            PaginatedList<Ticket> pagedList = PaginatedList<Ticket>.Create(list.AsQueryable(), pageSize.Value, pageIndex);

            #endregion Pagination

            #region Mapping to DTO

            var result = _mapper.Map<IEnumerable<TicketGetDTO>>(pagedList);

            #endregion Mapping to DTO

            return new SuccessResponse(SuccessStatusCode.Ok, new { Tickets = result, totalPage = pagedList.TotalPage }, "Loaded Tickets successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("{id}")]
    [Authorize]
    public async Task<IActionResult> GetTicketByIdAsync(Guid id)
    {
        try
        {
            var ticket = await _repository.Ticket.GetTicketByIdAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }
            string role = User.FindFirstValue(ClaimTypes.Role);
            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentAccount = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentAccount == null || currentAccount.Active == false)
            {
                return Forbid();
            }
            if (role.Equals(AppConstants.CustomerRole))
            {
                if (!ticket.AccountId.Equals(currentUserId))
                {
                    return NotFound();
                }
            }
            if (role.Equals(AppConstants.OwnerRole) || role.Equals(AppConstants.StaffRole))
            {
                if (!ticket.CarParkId.Equals(currentAccount.CarParkId))
                {
                    return NotFound();
                }
            }

            //Calculate current total fee if ticket is checked in
            if (ticket.Status == TicketStatus.CheckedIn)
            {
                decimal totalFee = 0;
                PriceTable priceTable = null;
                if (!string.IsNullOrEmpty(ticket.PriceTable))
                {
                    priceTable = SerializationUtils.DeserializeObject<PriceTable>(ticket.PriceTable);
                }
                if (priceTable != null)
                {
                    if (priceTable.Details.Count > 0)
                    {
                        totalFee = ParkingFeeUtilities.CalculateParkingFee(priceTable.Details, ticket.StartTime.Value, DateTime.UtcNow);
                    }
                    else if (priceTable.FeePerHour.HasValue)
                    {
                        double totalHours = Math.Ceiling(DateTime.UtcNow.Subtract(ticket.StartTime.Value).TotalHours);
                        // : Math.Floor(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                        totalFee = priceTable.FeePerHour.Value * (decimal)totalHours;
                    }
                }
                ticket.TotalFee = totalFee;
            }
            var result = _mapper.Map<TicketGetDTO>(ticket);
            return new SuccessResponse(SuccessStatusCode.Ok, result, "Loaded Ticket successfully");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, $"Error loading Ticket with ID {id}");
        }
    }

    [HttpPost("preview")]
    [Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> PreviewTicket([FromBody] TicketCreateDTO ticket)
    {
        try
        {
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var account = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (account == null || !account.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "Account not found", true);
                return Forbid();
            }

            if (ticket.ArriveTime.ToUniversalTime() < DateTime.UtcNow
                || ticket.ArriveTime.ToUniversalTime().Subtract(DateTime.UtcNow).TotalHours > 24)
            {
                return BadRequest("Invalid arrival time!");
            }

            // var pendingTicketExists = await _repository.Ticket.GetTickets()
            //     .AnyAsync(t => t.PlateNumber.Equals(ticket.PlateNumber)
            //         && (t.Status == TicketStatus.Pending || t.Status == TicketStatus.CheckedIn));
            // if (pendingTicketExists)
            // {
            //     return BadRequest("This plate number has a pending ticket or is already checked in");
            //     // return new ErrorResponse(ErrorStatusCode.BadRequest,
            //     //     "This plate number has a pending ticket or is already checked in", true);
            // }
            var duplicatedTicket = await _repository.Ticket.GetTickets()
                .AnyAsync(t => t.PlateNumber.Equals(ticket.PlateNumber)
                    // && t.AccountId.Equals(currentUserId)
                    && DateTime.Compare(ticket.ArriveTime.ToUniversalTime(), t.ArriveTime.Value) == 0
                    && t.Status == TicketStatus.Pending);
            if (duplicatedTicket)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Duplicated bookings", true);
                return BadRequest($"License plate {ticket.PlateNumber} has been booked for this arrival time {ticket.ArriveTime}");
            }

            var carPark = await _repository.CarPark.GetCarParkByIdAsync(ticket.CarParkId);
            if (carPark == null || carPark.Status == CarParkStatus.Removed)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not found", true);
                return BadRequest("Car park not found!");
            }
            if (carPark.AvailableSlotsCount <= 0)
            {
                return BadRequest("There are not slots left in this car park!");
            }
            var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carPark.Id, true);
            decimal reservationFee = 0;
            decimal reservationFeePercentage = 0;

            #region serialize price table to save into ticket

            string priceTableString = "";
            if (priceTable != null)
            {
                priceTableString = SerializationUtils.SerializeObject(priceTable, typeof(PriceTable));

                //Calculate reservation fee
                if (priceTable.Details.Count > 0)
                {
                    reservationFee =
                        ParkingFeeUtilities
                        .CalculateParkingFee(priceTable.Details, DateTime.UtcNow, ticket.ArriveTime.ToUniversalTime())
                         * priceTable.ReservationFeePercentage / 100;
                    // TimeSpan arriveTimeSpan = ticket.ArriveTime.TimeOfDay;
                    // foreach (var detail in priceTable.Details)
                    // {
                    //     if (ParkingFeeUtilities.CheckTimeBetweenAnInterval(detail.FromTime, detail.ToTime, arriveTimeSpan))
                    //     {
                    //         reservationFee = detail.Fee * priceTable.ReservationFeePercentage / 100;
                    //         break;
                    //     }
                    // }
                }
                else if (priceTable.FeePerHour.HasValue)
                {
                    // reservationFee = priceTable.FeePerHour.Value * priceTable.ReservationFeePercentage / 100;
                    double totalHours = Math.Ceiling(ticket.ArriveTime.ToUniversalTime().Subtract(DateTime.UtcNow).TotalHours);
                    // : Math.Floor(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                    reservationFee = priceTable.FeePerHour.Value * (decimal)totalHours * priceTable.ReservationFeePercentage / 100;
                }

                reservationFeePercentage = priceTable.ReservationFeePercentage;
            }

            #endregion serialize price table to save into ticket

            Ticket entity = new Ticket
            {
                AccountId = currentUserId,
                Account = account,
                ArriveTime = ticket.ArriveTime.ToUniversalTime(),
                BookTime = DateTime.UtcNow,
                CarParkId = ticket.CarParkId,
                CarPark = carPark,
                ReservationFee = reservationFee,
                Status = TicketStatus.Pending,
                PlateNumber = ticket.PlateNumber,
                PriceTable = priceTableString
            };
            var currentUserInfo = account.Info;
            if (currentUserInfo == null)
            {
                throw new Exception("User info not found!");
            }

            // currentUserInfo.AccountBalance -= entity.ReservationFee;
            // _repository.UserInfo.UpdateInfo(currentUserInfo);

            // await _repository.Ticket.CreateTicketAsync(entity);

            // carPark.AvailableSlotsCount--;
            // _repository.CarPark.UpdateCarPark(carPark);

            // await _repository.Save();
            // entity.Account = account;
            var result = _mapper.Map<TicketPreviewDTO>(entity);
            result.AccountBalance = currentUserInfo.AccountBalance;
            result.ReservationFeePercentage = reservationFeePercentage;

            return new SuccessResponse(SuccessStatusCode.Ok, result, "");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost]
    [Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> CreateTicket([FromBody] TicketCreateDTO ticket)
    {
        try
        {
            string currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var account = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (account == null || !account.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "Account not found", true);
                return Forbid();
            }
            if (ticket.ArriveTime.ToUniversalTime() < DateTime.UtcNow
                || ticket.ArriveTime.ToUniversalTime().Subtract(DateTime.UtcNow).TotalHours > 24)
            {
                return BadRequest("Invalid arrival time!");
            }

            // #region check if this plate number is booked or checked in

            // var pendingTicketExists = await _repository.Ticket.GetTickets()
            //     .AnyAsync(t => t.PlateNumber.Equals(ticket.PlateNumber)
            //         && (t.Status == TicketStatus.Pending || t.Status == TicketStatus.CheckedIn));
            // if (pendingTicketExists)
            // {
            //     return BadRequest("This plate number has a pending ticket or is already checked in");
            //     // return new ErrorResponse(ErrorStatusCode.BadRequest,
            //     //     "This plate number has a pending ticket or is already checked in", true);
            // }

            // #endregion

            var duplicatedTicket = await _repository.Ticket.GetTickets()
                .AnyAsync(t => t.PlateNumber.Equals(ticket.PlateNumber)
                    // && t.AccountId.Equals(currentUserId)
                    && DateTime.Compare(ticket.ArriveTime.ToUniversalTime(), t.ArriveTime.Value) == 0
                    && t.Status == TicketStatus.Pending);
            // && (t.Status != TicketStatus.Cancelled && t.Status != TicketStatus.CheckedOut));
            if (duplicatedTicket)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Duplicated bookings", true);
                return BadRequest($"License plate {ticket.PlateNumber} has been booked for this arrival time {ticket.ArriveTime}");
            }
            var carPark = await _repository.CarPark.GetCarParkByIdAsync(ticket.CarParkId);
            if (carPark == null || carPark.Status == CarParkStatus.Removed)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Car Park not found", true);
                return BadRequest("Car park not found!");
            }
            if (carPark.AvailableSlotsCount <= 0)
            {
                return BadRequest("There are not slots left in this car park!");
            }
            var priceTable = await _repository.PriceTable.GetPriceTableByCarParkId(carPark.Id, true);
            decimal reservationFee = 0;

            #region serialize price table to save into ticket

            string priceTableString = "";
            if (priceTable != null)
            {
                priceTableString = SerializationUtils.SerializeObject(priceTable, typeof(PriceTable));

                //Calculate reservation fee
                if (priceTable.Details.Count > 0)
                {
                    reservationFee =
                        ParkingFeeUtilities
                        .CalculateParkingFee(priceTable.Details, DateTime.UtcNow, ticket.ArriveTime.ToUniversalTime())
                         * priceTable.ReservationFeePercentage / 100;
                    // TimeSpan arriveTimeSpan = ticket.ArriveTime.TimeOfDay;
                    // foreach (var detail in priceTable.Details)
                    // {
                    //     if (ParkingFeeUtilities.CheckTimeBetweenAnInterval(detail.FromTime, detail.ToTime, arriveTimeSpan))
                    //     {
                    //         reservationFee = detail.Fee * priceTable.ReservationFeePercentage / 100;
                    //         break;
                    //     }
                    // }
                }
                else if (priceTable.FeePerHour.HasValue)
                {
                    // reservationFee = priceTable.FeePerHour.Value * priceTable.ReservationFeePercentage / 100;
                    double totalHours = Math.Ceiling(ticket.ArriveTime.ToUniversalTime().Subtract(DateTime.UtcNow).TotalHours);
                    // : Math.Floor(checkoutTime.Subtract(ticket.StartTime.Value).TotalHours);
                    reservationFee = priceTable.FeePerHour.Value * (decimal)totalHours * priceTable.ReservationFeePercentage / 100;
                }
            }

            #endregion serialize price table to save into ticket

            Ticket entity = new Ticket
            {
                AccountId = currentUserId,
                ArriveTime = ticket.ArriveTime.ToUniversalTime(),
                BookTime = DateTime.UtcNow,
                CarParkId = ticket.CarParkId,
                // ReservationFee = priceTable != null ? priceTable.ReservationFee : AppConstants.DefaultReservationFee,
                ReservationFee = reservationFee,
                Status = TicketStatus.Pending,
                PlateNumber = ticket.PlateNumber,
                PriceTable = priceTableString
            };
            var currentUserInfo = account.Info;
            if (currentUserInfo == null)
            {
                throw new Exception("User info not found!");
            }
            if (currentUserInfo.AccountBalance < entity.ReservationFee)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "Account balance is not enough", true);
                return BadRequest("Account balance is not enough for booking!");
            }

            currentUserInfo.AccountBalance -= entity.ReservationFee;
            _repository.UserInfo.UpdateInfo(currentUserInfo);

            await _repository.Ticket.CreateTicketAsync(entity);

            carPark.AvailableSlotsCount--;
            _repository.CarPark.UpdateCarPark(carPark);

            #region Check license plate exists, if not add to database

            var licensePlate = await _repository.LicensePlate.GetLicensePlate(entity.PlateNumber, currentUserId);
            if (licensePlate == null)
            {
                licensePlate = new LicensePlate
                {
                    PlateNumber = entity.PlateNumber,
                    AccountId = currentUserId,
                    UsageCount = 1
                };
                await _repository.LicensePlate.CreateLicensePlate(licensePlate);
            }
            else
            {
                licensePlate.UsageCount++;
                licensePlate = _repository.LicensePlate.UpdateLicensePlate(licensePlate);
            }

            #endregion Check license plate exists, if not add to database

            #region Create transaction

            Transaction transaction = new Transaction
            {
                AccountId = currentUserId,
                Amount = reservationFee,
                TicketId = entity.Id,
                Time = DateTime.UtcNow,
                TransactionType = TransactionType.Booking
            };

            await _repository.Transaction.CreateTransaction(transaction);

            #endregion Create transaction

            await _repository.Save();
            entity.Account = account;

            return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<TicketGetDTO>(entity), "Created ticket successfully");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    // [HttpGet("qr")]
    // public async Task<IActionResult> CheckTicketByPlateNumberAndCarParkId([Required] string plateNumber, [Required] Guid carParkId)
    // {
    //     try
    //     {
    //         var checkin = await _repository.Ticket.GetTickets()
    //             .AnyAsync(t => t.CarParkId.Equals(carParkId)
    //                 && t.PlateNumber.Equals(plateNumber)
    //                 && t.Status != TicketStatus.CheckedOut);
    //         return new SuccessResponse(SuccessStatusCode.Ok, checkin, "Checked successfully!");
    //     }
    //     catch (Exception ex)
    //     {
    //         // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
    //         return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
    //     }
    // }

    [HttpDelete("{id}"), Authorize(Roles = $"{AppConstants.StaffRole}, {AppConstants.CustomerRole}")]
    public async Task<IActionResult> CancelBooking(Guid id)
    {
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        var role = User.FindFirstValue(ClaimTypes.Role);
        if (role.Equals(AppConstants.StaffRole))
        {
            var account = await _repository.Account.GetAccountByIdAsync(userId);
            if (account == null || !account.Active.Value)
            {
                return Forbid();
            }
        }
        try
        {
            var booking = await _repository.Ticket.GetTicketByIdAsync(id, true);
            if (booking == null)
            {
                return NotFound();
            }
            switch (booking.Status)
            {
                case TicketStatus.CheckedIn:
                    return BadRequest("This ticket is already checked in!");
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This ticket is already checked in!", true);

                case TicketStatus.CheckedOut:
                    return BadRequest("This ticket is already checked out!");
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This ticket is already checked out!", true);

                case TicketStatus.Cancelled:
                    return BadRequest("This ticket is already cancelled!");
                // return new ErrorResponse(ErrorStatusCode.BadRequest, "This ticket is already cancelled!", true);

                default:
                    break;
            }
            if (User.IsInRole(AppConstants.CustomerRole))
            {
                if (!booking.AccountId.Equals(userId))
                {
                    return Forbid();
                }
            }
            booking.Status = TicketStatus.Cancelled;
            booking.CancellerAccountId = userId;
            _repository.Ticket.UpdateTicket(booking);

            #region Update car park slots

            var carPark = booking.CarPark;
            carPark.AvailableSlotsCount++;
            if (carPark.AvailableSlotsCount > carPark.MaxCapacity)
            {
                carPark.AvailableSlotsCount = carPark.MaxCapacity;
            }
            _repository.CarPark.UpdateCarPark(carPark);

            #endregion Update car park slots

            await _repository.Save();
            return NoContent();
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost("manual-create")]
    [Authorize(Roles = $"{AppConstants.StaffRole},{AppConstants.OwnerRole}")]
    public async Task<IActionResult> CreateTicketManually([FromBody] TicketManualCreateDTO dTO)
    {
        Ticket result;
        var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
        try
        {
            #region Check account allowed to edit

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentAccount = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentAccount == null || !currentAccount.Active.Value
                || !currentAccount.CanEdit || currentAccount.CarParkId == null)
                return Forbid();

            #endregion Check account allowed to edit

            #region Check if endTime.HasValue <-> totalFee.HasValue

            if ((dTO.EndTime.HasValue && !dTO.TotalFee.HasValue)
                || (!dTO.EndTime.HasValue && dTO.TotalFee.HasValue))
            {
                return BadRequest("If end time has value, total fee must have value also and vice versa!");
            }

            #endregion Check if endTime.HasValue <-> totalFee.HasValue

            #region Get car park

            var carPark = await _repository.CarPark.GetCarParkByIdAsync(currentAccount.CarParkId.Value, true);
            if (carPark == null)
                return BadRequest("Car park not found!");
            if (carPark.Status == CarParkStatus.Removed)
                return BadRequest("Car park is not active!");

            #endregion Get car park

            #region Get Customer account

            var customer = await _repository.Account.GetAccounts()
                .Include(acc => acc.Info)
                .SingleOrDefaultAsync(acc => acc.PhoneNumber.Equals(dTO.PhoneNumber) && acc.RoleId == AppConstants.CustomerRoleId);
            // .SingleOrDefaultAsync(acc => acc.Info.PhoneNumber.Equals(dTO.PhoneNumber) && acc.RoleId == AppConstants.CustomerRoleId);
            if (customer == null)
                return BadRequest("Customer account not found!");

            #endregion Get Customer account

            #region Check if customer or vehicle is checked in but not checked out

            bool checkedIn = await _repository.Ticket.GetTickets()
                .AnyAsync(t => t.AccountId.Equals(customer.Id) && t.Status == TicketStatus.CheckedIn);
            if (checkedIn)
                return BadRequest("This customer is checked in!");
            checkedIn = await _repository.Ticket.GetTickets()
                .AnyAsync(t => t.PlateNumber.Equals(dTO.PlateNumber) && t.Status == TicketStatus.CheckedIn);
            if (checkedIn)
                return BadRequest("This plate number is checked in!");

            #endregion Check if customer or vehicle is checked in but not checked out

            #region Check duplicated ticket

            bool duplicatedTicket = await _repository.Ticket.GetTickets()
                .AnyAsync(t => t.StartTime.Equals(dTO.StartTime.ToUniversalTime())
                    && t.AccountId.Equals(customer.Id) && t.PlateNumber.Equals(dTO.PlateNumber)
                    );
            if (duplicatedTicket)
                return BadRequest("Duplicated ticket!");

            #endregion Check duplicated ticket

            #region Check if customer and their plate number has booked at the car park

            var booking = await _repository.Ticket.GetTickets()
                .FirstOrDefaultAsync(t => t.CarParkId.Equals(carPark.Id) && t.AccountId.Equals(customer.Id)
                    && t.PlateNumber.Equals(dTO.PlateNumber)
                    && t.Status == TicketStatus.Pending
                    && t.BookTime.HasValue
                    && t.ArriveTime.HasValue);
            //if booking exists and arriveTime >= startTime:
            //Update booking ticket
            if (booking != null && booking.ArriveTime >= dTO.StartTime.ToUniversalTime())
            {
                booking.StartTime = dTO.StartTime.ToUniversalTime();
                booking.EndTime = dTO.EndTime.HasValue ? dTO.EndTime.Value.ToUniversalTime() : null;
                //if has endTime, set status = CheckedOut, calculate totalFee and make transaction
                if (booking.EndTime.HasValue)
                {
                    booking.Status = TicketStatus.CheckedOut;
                    booking.TotalFee = dTO.TotalFee.Value;
                }
                else
                    booking.Status = TicketStatus.CheckedIn;
                result = _repository.Ticket.UpdateTicket(booking);
            }

            #endregion Check if customer and their plate number has booked at the car park

            #region Create ticket

            else
            {
                var ticket = new Ticket
                {
                    AccountId = customer.Id,
                    CarParkId = carPark.Id,
                    // CarPark = carPark,
                    // Account = customer,
                    PlateNumber = dTO.PlateNumber,
                    StartTime = dTO.StartTime.ToUniversalTime(),
                    EndTime = dTO.EndTime.HasValue ? dTO.EndTime.Value.ToUniversalTime() : null,
                    PriceTable = carPark.PriceTable != null
                                    ? SerializationUtils.SerializeObject(carPark.PriceTable, typeof(PriceTable))
                                    : ""
                };
                if (ticket.EndTime.HasValue)
                {
                    ticket.Status = TicketStatus.CheckedOut;
                    ticket.TotalFee = dTO.TotalFee.Value;
                }
                else
                {
                    ticket.Status = TicketStatus.CheckedIn;
                    if (carPark.AvailableSlotsCount <= 0)
                    {
                        return BadRequest("There are no slots left!");
                    }
                    carPark.AvailableSlotsCount -= 1;
                    _repository.CarPark.UpdateCarPark(carPark);
                }
                result = await _repository.Ticket.CreateTicketAsync(ticket);
            }

            #endregion Create ticket

            #region Check license plate exists, if not add to database

            var licensePlate = await _repository.LicensePlate.GetLicensePlate(result.PlateNumber, result.AccountId);
            if (licensePlate == null)
            {
                licensePlate = new LicensePlate
                {
                    PlateNumber = result.PlateNumber,
                    AccountId = result.AccountId,
                    UsageCount = 1
                };
                await _repository.LicensePlate.CreateLicensePlate(licensePlate);
            }
            else
            {
                licensePlate.UsageCount++;
                _repository.LicensePlate.UpdateLicensePlate(licensePlate);
            }

            #endregion Check license plate exists, if not add to database

            await _repository.Save();
            result.Account = customer;
            result.CarPark = carPark;
            return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<TicketGetDTO>(result), "Created ticket successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            // return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPut("{id}"), Authorize(Roles = $"{AppConstants.StaffRole},{AppConstants.OwnerRole}")]
    public async Task<IActionResult> UpdateTicket(Guid id, [FromBody] TicketUpdateDTO dTO)
    {
        Ticket result;
        var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
        if (!id.Equals(dTO.Id))
            return BadRequest("Invalid ID!");
        try
        {
            #region Check account allowed to edit

            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var currentAccount = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentAccount == null || !currentAccount.Active.Value
                || !currentAccount.CanEdit || currentAccount.CarParkId == null)
                return Forbid();

            #endregion Check account allowed to edit

            #region Get ticket from ID

            var ticketToUpdate = await _repository.Ticket.GetTicketByIdAsync(id);
            if (ticketToUpdate == null)
                return NotFound();

            #endregion Get ticket from ID

            #region Check if endTime.HasValue <-> totalFee.HasValue

            if ((dTO.EndTime.HasValue && !dTO.TotalFee.HasValue)
                || (!dTO.EndTime.HasValue && dTO.TotalFee.HasValue))
            {
                return BadRequest("If end time has value, total fee must have value also and vice versa!");
            }

            #endregion Check if endTime.HasValue <-> totalFee.HasValue

            if (dTO.EndTime.HasValue && dTO.EndTime < dTO.StartTime)
                return BadRequest("End time cannot be earlier than start time!");

            #region Get car park

            var carPark = await _repository.CarPark.GetCarParkByIdAsync(currentAccount.CarParkId.Value, true);
            if (carPark == null)
                return BadRequest("Car park not found!");

            #endregion Get car park

            #region Check duplicated ticket

            bool duplicatedTicket = await _repository.Ticket.GetTickets()
                .AnyAsync(t => t.StartTime.Equals(dTO.StartTime.ToUniversalTime())
                    && t.AccountId.Equals(ticketToUpdate.AccountId)
                    && t.PlateNumber.Equals(dTO.PlateNumber)
                    && !t.Id.Equals(ticketToUpdate.Id));
            if (duplicatedTicket)
                return BadRequest("Duplicated ticket!");

            #endregion Check duplicated ticket

            #region Mapping entity

            ticketToUpdate.PlateNumber = dTO.PlateNumber;
            ticketToUpdate.EndTime = dTO.EndTime.HasValue ? dTO.EndTime.Value.ToUniversalTime() : ticketToUpdate.EndTime;
            ticketToUpdate.StartTime = dTO.StartTime;
            ticketToUpdate.TotalFee = dTO.TotalFee.HasValue ? dTO.TotalFee.Value : ticketToUpdate.TotalFee;
            if (ticketToUpdate.EndTime.HasValue)
                ticketToUpdate.Status = TicketStatus.CheckedOut;
            else
                ticketToUpdate.Status = TicketStatus.CheckedIn;

            #endregion Mapping entity

            #region Update ticket

            result = _repository.Ticket.UpdateTicket(ticketToUpdate);
            await _repository.Save();

            #endregion Update ticket

            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<TicketGetDTO>(result), "Updated ticket successfully!");
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            // return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}