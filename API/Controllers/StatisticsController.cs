﻿using API.Helpers;
using API.Responses.SuccessResponses;
using DTOs.StatisticDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Utilities;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatisticsController : ControllerBase
    {
        private readonly IRepositoryManager _repository;

        public StatisticsController(IRepositoryManager repository)
        {
            _repository = repository;
        }

        [HttpGet("income-by-car-park")]
        [Authorize(Roles = $"{AppConstants.OwnerRole}, {AppConstants.AdminRole}")]
        public async Task<IActionResult> GenerateIncomeByCarParkIdByYear([Required] int year, [Required] Guid carParkId)
        {
            Dictionary<int, decimal> result = new Dictionary<int, decimal>();
            try
            {
                var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId);
                if (carPark == null)
                {
                    return NotFound();
                }

                #region check if user is the owner of the car park

                if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.OwnerRole))
                {
                    var currentUserCarParkId = new Guid(User.FindFirstValue("CarParkId"));
                    if (!carPark.Id.Equals(currentUserCarParkId))
                    {
                        // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                        return Forbid();
                    }
                }

                #endregion check if user is the owner of the car park

                for (int i = 1; i <= 12; i++)
                {
                    decimal total = 0;
                    var firstDay = new DateTime(year, i, 1).ToUniversalTime();
                    var lastDay = firstDay.AddMonths(1).AddSeconds(-1).ToUniversalTime();
                    var tickets = await _repository.Ticket.GetTickets()
                        .Where(t => ((t.EndTime >= firstDay && t.EndTime <= lastDay)
                            || (t.BookTime >= firstDay && t.BookTime <= lastDay))
                            && t.CarParkId.Equals(carPark.Id))
                        .Select(t => t.TotalFee + t.ReservationFee).ToListAsync();
                    foreach (var fee in tickets)
                    {
                        total += fee;
                    }
                    result.Add(i, total);
                }
                return new SuccessResponse(SuccessStatusCode.Ok, result, "Successful");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("tickets-count")]
        [Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.OwnerRole}")]
        public async Task<IActionResult> GetTicketsCountByYear(int year, Guid? carParkId)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            try
            {
                CarPark carPark = null;

                if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.AdminRole))
                {
                    if (carParkId.HasValue)
                    {
                        carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId.Value);
                    }
                }
                else
                {
                    carPark = await _repository.CarPark.GetCarParkByIdAsync(new Guid(User.FindFirstValue("CarParkId")));
                    if (carPark == null)
                    {
                        return NotFound();
                    }
                }

                for (int i = 1; i <= 12; i++)
                {
                    var firstDay = new DateTime(year, i, 1).ToUniversalTime();
                    var lastDay = firstDay.AddMonths(1).AddSeconds(-1).ToUniversalTime();
                    var count = (carPark == null)
                        ? _repository.Ticket.GetTickets()
                            .Where(t => ((t.EndTime >= firstDay && t.EndTime <= lastDay)
                                || (t.BookTime >= firstDay && t.BookTime <= lastDay)))
                            .Count()
                        : _repository.Ticket.GetTickets()
                            .Where(t => ((t.EndTime >= firstDay && t.EndTime <= lastDay)
                                || (t.BookTime >= firstDay && t.BookTime <= lastDay))
                                && t.CarParkId.Equals(carPark.Id))
                            .Count();
                    result.Add(i, count);
                }
                return new SuccessResponse(SuccessStatusCode.Ok, result, "Successful");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("users-count")]
        [Authorize(Roles = $"{AppConstants.OwnerRole}, {AppConstants.AdminRole}")]
        public async Task<IActionResult> GetUsersCountByYearAndCarParkId([Required] int year, [Required] Guid carParkId)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            try
            {
                var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId);
                if (carPark == null)
                {
                    return NotFound();
                }

                #region check if user is the owner of the car park

                if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.OwnerRole))
                {
                    var currentUserCarParkId = new Guid(User.FindFirstValue("CarParkId"));
                    if (!carPark.Id.Equals(currentUserCarParkId))
                    {
                        // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                        return Forbid();
                    }
                }

                #endregion check if user is the owner of the car park

                for (int i = 1; i <= 12; i++)
                {
                    var firstDay = new DateTime(year, i, 1).ToUniversalTime();
                    var lastDay = firstDay.AddMonths(1).AddSeconds(-1).ToUniversalTime();
                    var count = _repository.Ticket.GetTickets()
                        .Where(t => ((t.EndTime >= firstDay && t.EndTime <= lastDay)
                            || (t.BookTime >= firstDay && t.BookTime <= lastDay))
                            && t.CarParkId.Equals(carPark.Id))
                        .Select(t => t.AccountId)
                        .Distinct()
                        .Count();
                    result.Add(i, count);
                }
                return new SuccessResponse(SuccessStatusCode.Ok, result, "Successful");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("transactions-count")]
        [Authorize(Roles = $"{AppConstants.AdminRole},{AppConstants.OwnerRole}")]
        public async Task<IActionResult> GetTransactionsCount([Required] int year, Guid? carParkId)
        {
            try
            {
                Dictionary<int, int> result = new Dictionary<int, int>();
                var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
                string role = User.FindFirstValue(ClaimTypes.Role);
                for (int i = 1; i <= 12; i++)
                {
                    var firstDay = new DateTime(year, i, 1).ToUniversalTime();
                    var lastDay = firstDay.AddMonths(1).AddSeconds(-1).ToUniversalTime();
                    int count = 0;
                    if (role.Equals(AppConstants.AdminRole))
                    {
                        if (carParkId.HasValue)
                        {
                            count = await _repository.Transaction.GetFromSql("SELECT * FROM public.\"Transaction\""
                                + " WHERE \"TicketId\" IN "
                                + $"(SELECT \"Id\" FROM public.\"Ticket\" WHERE \"CarParkId\" = '{carParkId.Value}')")
                                .Where(tr => tr.Time >= firstDay && tr.Time <= lastDay)
                                .Select(tr => tr.Id)
                                .CountAsync();
                        }
                        else
                        {
                            count = await _repository.Transaction.FindAll(false)
                                .Where(tr => tr.Time >= firstDay && tr.Time <= lastDay)
                                .Select(tr => tr.Id)
                                .CountAsync();
                        }
                    }
                    else
                    {
                        if (currentUser.CarParkId == null)
                            return Forbid();
                        count = await _repository.Transaction.GetFromSql("SELECT * FROM public.\"Transaction\""
                                 + " WHERE \"TicketId\" IN "
                                 + $"(SELECT \"Id\" FROM public.\"Ticket\" WHERE \"CarParkId\" = '{currentUser.CarParkId}')")
                                 .Where(tr => tr.Time >= firstDay && tr.Time <= lastDay)
                                 .Select(tr => tr.Id)
                                 .CountAsync();
                    }

                    result.Add(i, count);
                }
                return new SuccessResponse(SuccessStatusCode.Ok, result, "Successful");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("bookings-count")]
        [Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.OwnerRole}")]
        public async Task<IActionResult> BookingsStatistic(int? pageSize, int pageIndex = 1)
        {
            try
            {
                List<BookingStatisticDTO> bookingStatistics = new List<BookingStatisticDTO>();
                var bookings = await _repository.Ticket
                    .FindByCondition(t => t.BookTime.HasValue, false).ToListAsync();
                if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.AdminRole))
                {
                    var carParks = await _repository.CarPark.GetCarParks()
                                        .ToListAsync();
                    foreach (var park in carParks)
                    {
                        int totalBookings = bookings.Where(b => b.CarParkId.Equals(park.Id)).Count();
                        int arrivedBookings = bookings.Where(b => b.CarParkId.Equals(park.Id)
                            && (b.Status == Utilities.Enums.TicketStatus.CheckedIn
                                || b.Status == Utilities.Enums.TicketStatus.CheckedOut)).Count();
                        string address = $"{park.AddressNumber}, {park.Street}, {park.Ward}, {park.District}, {park.City}";
                        BookingStatisticDTO dto = new BookingStatisticDTO(park.Id, park.Name, address, totalBookings, arrivedBookings);
                        bookingStatistics.Add(dto);
                    }
                }
                else
                {
                    Guid carParkId = new(User.FindFirstValue("CarParkId"));
                    var carPark = await _repository.CarPark.GetCarParkByIdAsync(carParkId);
                    if (carPark == null)
                    {
                        return Forbid("User's car park not found!");
                    }
                    int totalBookings = bookings.Where(b => b.CarParkId.Equals(carParkId)).Count();
                    int arrivedBookings = bookings.Where(b => b.CarParkId.Equals(carParkId)
                        && (b.Status == Utilities.Enums.TicketStatus.CheckedIn
                            || b.Status == Utilities.Enums.TicketStatus.CheckedOut)).Count();
                    string address = $"{carPark.AddressNumber}, {carPark.Street}, {carPark.Ward}, {carPark.District}, {carPark.City}";
                    BookingStatisticDTO dto = new BookingStatisticDTO(carPark.Id, carPark.Name, address, totalBookings, arrivedBookings);
                    bookingStatistics.Add(dto);
                }

                //Pagination
                if (!pageSize.HasValue)
                    pageSize = bookingStatistics.Count;
                PaginatedList<BookingStatisticDTO> pagedList = PaginatedList<BookingStatisticDTO>.Create(bookingStatistics.AsQueryable(), pageSize.Value, pageIndex);

                return new SuccessResponse(SuccessStatusCode.Ok, new { statistics = pagedList, TotalPage = pagedList.TotalPage }, "Made bookings statistics successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("ticket-count-time-interval")]
        public async Task<IActionResult> GetTicketCounts(DateTime? from, DateTime? to, int? pageSize, int pageIndex = 1)
        {
            try
            {
                // Dictionary<Guid, int> result = new Dictionary<Guid, int>();
                List<TicketCountCarParkDTO> result = new List<TicketCountCarParkDTO>();
                var carParks = await _repository.CarPark.GetCarParks().ToListAsync();
                foreach (var park in carParks)
                {
                    var tickets = _repository.Ticket.FindByCondition(t => t.CarParkId.Equals(park.Id), false);
                    if (from.HasValue)
                    {
                        tickets = tickets.Where(t => t.StartTime.Value >= from.Value.ToUniversalTime()
                            || t.BookTime.Value >= from.Value.ToUniversalTime());
                    }
                    if (to.HasValue)
                    {
                        tickets = tickets.Where(t => t.StartTime.Value <= to.Value.ToUniversalTime()
                            || t.BookTime.Value <= to.Value.ToUniversalTime());
                    }
                    // result.Add(park.Id, await tickets.CountAsync());
                    string address = $"{park.AddressNumber}, {park.Street}, {park.Ward}, {park.District}, {park.City}";
                    TicketCountCarParkDTO dto
                        = new TicketCountCarParkDTO(park.Id, park.Name, address, await tickets.CountAsync());
                    result.Add(dto);
                }

                //Pagination
                PaginatedList<TicketCountCarParkDTO> pagedList = PaginatedList<TicketCountCarParkDTO>.Create(result.AsQueryable(), pageSize.HasValue ? pageSize.Value : result.Count, pageIndex);

                return new SuccessResponse(SuccessStatusCode.Ok, new { Result = pagedList, TotalPage = pagedList.TotalPage }, "Counted tickets successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("income")]
        public async Task<IActionResult> GetIncome(DateTime? from, DateTime? to, int? pageSize, int pageIndex = 1)
        {
            try
            {
                List<IncomeCarParkDTO> lst = new List<IncomeCarParkDTO>();
                var carParks = await _repository.CarPark.GetCarParks().ToListAsync();
                foreach (var park in carParks)
                {
                    var transactions = _repository.Transaction.GetFromSql("SELECT * FROM public.\"Transaction\""
                            + " WHERE \"TicketId\" IN "
                            + $"(SELECT \"Id\" FROM public.\"Ticket\" WHERE \"CarParkId\" = '{park.Id}')");
                    if (from.HasValue)
                    {
                        transactions = transactions.Where(tr => tr.Time >= from.Value.ToUniversalTime());
                    }
                    if (to.HasValue)
                    {
                        transactions = transactions.Where(tr => tr.Time <= to.Value.ToUniversalTime());
                    }

                    decimal income = await transactions.Select(tr => tr.Amount).SumAsync();
                    string address = $"{park.AddressNumber}, {park.Street}, {park.Ward}, {park.District}, {park.City}";
                    IncomeCarParkDTO dto = new IncomeCarParkDTO(park.Id, park.Name, address, income);
                    lst.Add(dto);
                }

                //Pagination
                PaginatedList<IncomeCarParkDTO> pagedList = PaginatedList<IncomeCarParkDTO>.Create(lst.AsQueryable(), pageSize.HasValue ? pageSize.Value : lst.Count, pageIndex);

                return new SuccessResponse(SuccessStatusCode.Ok, new { Result = pagedList, TotalPage = pagedList.TotalPage }, "Counted tickets successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}