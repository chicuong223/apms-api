﻿using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.CustomerDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Repositories.Interfaces;
using System.Security.Claims;
using Utilities;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private IMapper _mapper;

        public CustomersController(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = AppConstants.AdminRole)]
        public IActionResult GetCustomers(string fullName, int? pageSize, int pageIndex = 1)
        {
            var customersFromDb = _repository.Account.GetAccounts()
                .Include(acc => acc.Info)
                .Where(acc => acc.RoleId == AppConstants.CustomerRoleId);

            #region Filter Data

            if (!string.IsNullOrEmpty(fullName))
            {
                customersFromDb = customersFromDb
                    .Where(acc => acc.Info.FullName.ToLower().Contains(fullName.ToLower()));
            }

            #endregion Filter Data

            #region Pagination

            if (!pageSize.HasValue)
            {
                pageSize = customersFromDb.Count();
            }
            PaginatedList<Account> pagedList = PaginatedList<Account>.Create(customersFromDb, pageSize.Value, pageIndex);

            #endregion Pagination

            var result = _mapper.Map<IEnumerable<CustomerGetDTO>>(pagedList);
            return new SuccessResponse(SuccessStatusCode.Ok, new { customers = result, totalPage = pagedList.TotalPage }, "Customers loaded successfully!"); ;
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody] CustomerCreateDTO customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
            }
            try
            {
                if (await _repository.Account.GetAccountByPhoneNumber(customer.PhoneNumber, false) != null)
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "Phone number is used", true);
                    return BadRequest("Phone number is used!");
                }
                var entity = new Account
                {
                    Password = customer.Password,
                    RoleId = AppConstants.CustomerRoleId,
                    PhoneNumber = customer.PhoneNumber,
                    Info = new UserInfo
                    {
                        FullName = customer.FullName,
                        AccountBalance = 0,
                        // PhoneNumber = customer.PhoneNumber
                    }
                };

                #region Generate ID

                string id = AccountUtils.GenerateAccountID(entity.RoleId);
                var existAccountId = await _repository.Account.GetAccountByIdAsync(id);
                while (existAccountId != null)
                {
                    id = AccountUtils.GenerateAccountID(entity.RoleId);
                    existAccountId = await _repository.Account.GetAccountByIdAsync(id);
                }

                entity.Id = id;

                #endregion Generate ID

                await _repository.Account.CreateAccountAsync(entity);
                await _repository.Save();
                return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<CustomerGetDTO>(entity), "Created customer successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, "Error creating customer");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = $"{AppConstants.StaffRole}, {AppConstants.AdminRole}")]
        public async Task<IActionResult> GetCustomerByIdAsync(string id)
        {
            try
            {
                var acc = await _repository.Account.GetAccounts()
                    .Include(acc => acc.Info)
                    .SingleOrDefaultAsync(cus => cus.RoleId == AppConstants.CustomerRoleId && cus.Id.Equals(id));
                if (acc == null)
                {
                    return NotFound();
                }
                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<CustomerGetDTO>(acc), "Customer loaded successfully");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, $"Error getting customer with ID {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPatch("update-profile")]
        [Authorize(Roles = AppConstants.CustomerRole)]
        public async Task<IActionResult> UpdateCustomer(CustomerUpdateDTO customer)
        {
            if (!ModelState.IsValid)
            {
                // return new ErrorResponse(ErrorStatusCode.BadRequest, ModelState, true);
                return BadRequest(ModelState);
            }
            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var customerToUpdate = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (customerToUpdate == null || !customerToUpdate.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            if (!string.IsNullOrWhiteSpace(customer.PhoneNumber))
            {
                var existingPhoneNumberAccount = await _repository.Account.GetAccountByPhoneNumber(customer.PhoneNumber, false);
                if (existingPhoneNumberAccount != null && !existingPhoneNumberAccount.Id.Equals(customerToUpdate.Id))
                {
                    // return new ErrorResponse(ErrorStatusCode.BadRequest, "This phone number is already used", true);
                    return BadRequest("Phone number is used!");
                }
                customerToUpdate.PhoneNumber = customer.PhoneNumber;
            }
            // if (!string.IsNullOrEmpty(customer.Password))
            // {
            //     customerToUpdate.Password = AccountUtils.HashPasswordWithProvidedSalt(customer.Password, customerToUpdate.Salt);
            // }
            customerToUpdate.Info = new UserInfo
            {
                Id = customerToUpdate.Id,
                AccountBalance = customerToUpdate.Info.AccountBalance,
                FullName = !string.IsNullOrWhiteSpace(customer.FullName) ? customer.FullName : customerToUpdate.Info.FullName,
                // PhoneNumber = !string.IsNullOrWhiteSpace(customer.PhoneNumber) ? customer.PhoneNumber : customerToUpdate.Info.PhoneNumber
            };
            _repository.Account.UpdateAccount(customerToUpdate);
            await _repository.Save();
            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<CustomerGetDTO>(customerToUpdate), "Updated customer successfully!");
        }

        [HttpGet("profile")]
        [Authorize(Roles = AppConstants.CustomerRole)]
        public async Task<IActionResult> GetProfile()
        {
            var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            try
            {
                // var profile = await _repository.Account.GetAccountByIdAsync(currentUserId);
                var profile = await _repository.Account.GetAccounts()
                    .Include(acc => acc.Info)
                    .SingleOrDefaultAsync(acc => acc.Id.Equals(currentUserId));
                return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<CustomerGetDTO>(profile), "Loaded customer profile successfully!");
            }
            catch (Exception ex)
            {
                // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}