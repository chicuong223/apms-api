using API.Helpers;
using API.Responses.SuccessResponses;
using AutoMapper;
using DTOs.TransactionDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Repositories.Interfaces;
using System.Security.Claims;
using Utilities;
using Utilities.Enums;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TransactionsController : ControllerBase
{
    private readonly IRepositoryManager _repository;
    private readonly IMapper _mapper;

    public TransactionsController(IRepositoryManager repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet, Authorize]
    public async Task<IActionResult> GetTransactions(Guid? carParkId, DateTime? from, DateTime? to, TransactionType? type, int? pageSize, int pageIndex = 1)
    {
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            IQueryable<Transaction> transactions;
            switch (User.FindFirstValue(ClaimTypes.Role))
            {
                case AppConstants.AdminRole:
                    transactions = carParkId.HasValue
                        ? _repository.Transaction.GetFromSql("SELECT * FROM public.\"Transaction\""
                                + " WHERE \"TicketId\" IN "
                                + $"(SELECT \"Id\" FROM public.\"Ticket\" WHERE \"CarParkId\" = '{carParkId.Value}')")
                            // .Include(t => t.Account)
                            .OrderByDescending(t => t.Time)
                        : _repository.Transaction.GetTransactions(includeAccount: false)
                            .OrderByDescending(t => t.Time);
                    break;

                case AppConstants.CustomerRole:
                    transactions = (carParkId.HasValue
                        ? _repository.Transaction.GetFromSql("SELECT * FROM public.\"Transaction\""
                                + " WHERE \"TicketId\" IN "
                                + $"(SELECT \"Id\" FROM public.\"Ticket\" WHERE \"CarParkId\" = '{carParkId.Value}')")
                            // .Include(t => t.Account)
                            .OrderByDescending(t => t.Time)
                        : _repository.Transaction.GetTransactions(includeAccount: false))
                            .Where(t => t.AccountId.Equals(currentUserId))
                            .OrderByDescending(t => t.Time);
                    break;

                default:
                    if (currentUser.CarParkId == null)
                        return Forbid();
                    transactions = _repository.Transaction.GetFromSql("SELECT * FROM public.\"Transaction\""
                                + " WHERE \"TicketId\" IN "
                                + $"(SELECT \"Id\" FROM public.\"Ticket\" WHERE \"CarParkId\" = '{currentUser.CarParkId}')")
                            // .Include(t => t.Account)
                            .OrderByDescending(t => t.Time);
                    break;
            }

            #region Filter Data

            if (from.HasValue)
            {
                transactions = transactions.Where(t => t.Time >= from.Value.ToUniversalTime());
            }
            if (to.HasValue)
            {
                transactions = transactions.Where(t => t.Time <= to.Value.ToUniversalTime());
            }

            if (type.HasValue)
            {
                transactions = transactions.Where(t => t.TransactionType == type.Value);
            }

            #endregion Filter Data

            #region Pagination

            if (!pageSize.HasValue)
            {
                pageSize = transactions.Count();
                // pageSize = 50;
            }
            var pagedList = PaginatedList<Transaction>.Create(transactions, pageSize.Value, pageIndex);

            #endregion Pagination

            return new SuccessResponse(SuccessStatusCode.Ok, new { Transactions = _mapper.Map<IEnumerable<TransactionGetDTO>>(pagedList), TotalPage = pagedList.TotalPage }, "Loaded transactions successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpGet("{id}"), Authorize(Roles = $"{AppConstants.AdminRole}, {AppConstants.CustomerRole}")]
    public async Task<IActionResult> GetTransactionByIdAsync(Guid id)
    {
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }
            var transaction = await _repository.Transaction.GetTransactionByIdAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            if (User.FindFirstValue(ClaimTypes.Role).Equals(AppConstants.CustomerRole))
            {
                if (!transaction.AccountId.Equals(currentUserId))
                {
                    // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                    return Forbid();
                }
            }
            return new SuccessResponse(SuccessStatusCode.Ok, _mapper.Map<TransactionGetDTO>(transaction), "Loaded transaction successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    [HttpPost, Authorize(Roles = AppConstants.CustomerRole)]
    public async Task<IActionResult> CreateTransaction([FromBody] TransactionCreateDTO dto)
    {
        var currentUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        try
        {
            var currentUser = await _repository.Account.GetAccountByIdAsync(currentUserId);
            if (currentUser == null || !currentUser.Active.Value)
            {
                // return new ErrorResponse(ErrorStatusCode.Forbidden, "", true);
                return Forbid();
            }

            #region Map entity

            var entity = new Transaction
            {
                AccountId = currentUserId,
                Amount = dto.Amount,
                // Success = true,
                TransactionType = Utilities.Enums.TransactionType.TopUp,
                Time = DateTime.UtcNow
            };

            #endregion Map entity

            entity = await _repository.Transaction.CreateTransaction(entity);

            #region Increase account balance

            var userInfo = currentUser.Info;
            userInfo.AccountBalance += entity.Amount;
            _repository.UserInfo.UpdateInfo(userInfo);

            #endregion Increase account balance

            await _repository.Save();
            entity.Account = currentUser;
            return new SuccessResponse(SuccessStatusCode.Created, _mapper.Map<TransactionGetDTO>(entity), "Created transaction successfully!");
        }
        catch (Exception ex)
        {
            // return new ErrorResponse(ErrorStatusCode.InternalServerError, ex.Message);
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}