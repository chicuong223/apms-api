﻿using Microsoft.EntityFrameworkCore;
using Repositories.Interfaces;
using Utilities.Enums;

namespace API.Middlewares
{
    public class CancelExpiredBookingsMiddleware
    {
        private readonly RequestDelegate _next;

        public CancelExpiredBookingsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, IRepositoryManager repository)
        {
            DateTime now = DateTime.UtcNow;
            DateTime dueTime = now.AddMinutes(-30);
            var expiredBookings = await repository.Ticket
                .FindByCondition(b => b.ArriveTime <= dueTime && b.Status == TicketStatus.Pending, true)
                .ToListAsync();
            foreach (var booking in expiredBookings)
            {
                booking.Status = TicketStatus.Cancelled;
                // repository.Ticket.UpdateTicket(booking);
                var carPark = await repository.CarPark.GetCarParkByIdAsync(booking.CarParkId, trackChanges: true);
                carPark.AvailableSlotsCount++;
                if (carPark.AvailableSlotsCount > carPark.MaxCapacity)
                {
                    carPark.AvailableSlotsCount = carPark.MaxCapacity;
                }
                // repository.CarPark.UpdateCarPark(carPark);
            }
            await repository.Save();
            await _next(httpContext);
        }
    }
}