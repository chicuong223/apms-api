using Microsoft.AspNetCore.SignalR;

namespace API.SignalRHubs;

public class CheckinHub : Hub
{
    public async Task SendCheckoutMessage(string plateNumber, Guid carParkId, string picInUrl, string picOutUrl)
    {
        await Clients.All.SendAsync("NewCheckout", plateNumber, carParkId, picInUrl, picOutUrl);
    }

    public async Task SendCheckinMessage(string plateNumber, Guid carParkId, string picInUrl)
    {
        await Clients.All.SendAsync("NewCheckin", plateNumber, carParkId, picInUrl);
    }
}