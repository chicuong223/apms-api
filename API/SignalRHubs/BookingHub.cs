using DTOs.TicketDTOs;
using Microsoft.AspNetCore.SignalR;

namespace API.SignalRHubs;

public class BookingHub : Hub
{
    public async Task NewBooking(TicketGetDTO booking)
    {
        await Clients.All.SendAsync("NewBooking", booking);
    }

    public async Task BookingCancelled(Guid bookingId, Guid carParkId)
    {
        await Clients.All.SendAsync("BookingCancelled", bookingId, carParkId);
    }
}