﻿using API.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace API.Responses.ErrorResponses
{
    public class ErrorResponse : ObjectResult
    {
        public ErrorResponse(ErrorStatusCode statusCode, object message, bool isOperational = false, object value = null) : base(value)
        {
            StatusCode = 200;
            Value = new ErrorResponseObject(
                statusCode: statusCode,
                message: message,
                isOperational: isOperational);
        }
    }
}