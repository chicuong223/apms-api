﻿using API.Helpers;

namespace API.Responses.ErrorResponses
{
    public record ErrorResponseObject(
        ErrorStatusCode statusCode,
        object message,
        bool isOperational);
}