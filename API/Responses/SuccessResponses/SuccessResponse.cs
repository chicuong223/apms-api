﻿using API.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace API.Responses.SuccessResponses
{
    public class SuccessResponse : ObjectResult
    {
        public SuccessResponse(SuccessStatusCode statusCode, object value, string message) : base(value)
        {
            StatusCode = (int?)statusCode;
            Value = new SuccessResponseObject(
                statusCode: statusCode,
                message: message,
                data: value);
        }
    }
}