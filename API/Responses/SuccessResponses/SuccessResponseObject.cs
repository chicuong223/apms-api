﻿using API.Helpers;

namespace API.Responses.SuccessResponses;
public record SuccessResponseObject(
    SuccessStatusCode statusCode,
    string message,
    object data
    );