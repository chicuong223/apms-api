﻿using AutoMapper;
using DTOs.AccountDTOs;
using DTOs.CarParkDTOs;
using DTOs.CustomerDTOs;
using DTOs.FeedbackDTOs;
using DTOs.PriceTableDTO;
using DTOs.TicketDTOs;
using DTOs.TransactionDTOs;
using Models;
using System.Text.Json;

namespace API.Helpers
{
    public class EntityMapper : Profile
    {
        public EntityMapper()
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            CreateMap<Account, AccountGetDTO>()
                .ForMember(dto => dto.RoleTitle,
                opt =>
                {
                    opt.PreCondition(acc => acc.Role != null);
                    opt.MapFrom(acc => acc.Role.Title);
                })
                .ForMember(dto => dto.CarPark, opt =>
                {
                    opt.PreCondition(src => src.CarPark != null);
                    opt.MapFrom(src => src.CarPark);
                })
                .ForMember(dto => dto.CanEditTicket, opt => opt.MapFrom(src => src.CanEdit));
            CreateMap<Account, AccountGetMinimumDTO>()
                .ForMember(dto => dto.RoleTitle, opt =>
                {
                    opt.PreCondition(src => src.Role != null);
                    opt.MapFrom(src => src.Role.Title);
                })
                .ForMember(dto => dto.CanEditTicket, opt => opt.MapFrom(src => src.CanEdit));

            CreateMap<UserInfo, UserInfoDTO>();
            CreateMap<AccountCreateDTO, Account>();
            CreateMap<AccountUpdateDTO, Account>();
            CreateMap<Account, CustomerGetDTO>()
                .ForMember(d => d.FullName,
                opt => opt.MapFrom(acc => acc.Info.FullName))
                .ForMember(d => d.AccountBalance, opt
                => opt.MapFrom(acc => acc.Info.AccountBalance));
            // .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(acc => acc.Info.PhoneNumber));
            CreateMap<CarPark, CarParkGetDTO>();
            // .ForMember(dto => dto.ParkingFeePerHour, opt =>
            // {
            //     opt.PreCondition(src => src.Config != null);
            //     opt.MapFrom(src => src.Config.ParkingFeePerHour);
            // })
            // .ForMember(dto => dto.ReservationFee, opt =>
            // {
            //     opt.PreCondition(src => src.Config != null);
            //     opt.MapFrom(src => src.Config.ReservationFee);
            // });
            CreateMap<CarPark, CarParkGetWithDistanceDTO>();
            CreateMap<CarParkUpdateDTO, CarPark>()
                .ReverseMap();
            CreateMap<CarPark, CarParkNameAddressDTO>();
            CreateMap<Ticket, TicketGetDTO>()
                .ForMember(dto => dto.EndTime, opt =>
                {
                    opt.PreCondition((src) => src.EndTime.HasValue);
                    opt.MapFrom(e => TimeZoneInfo.ConvertTimeFromUtc(e.EndTime.Value, timeZone));
                })
                .ForMember(dto => dto.StartTime, opt =>
                {
                    opt.PreCondition((src) => src.StartTime.HasValue);
                    opt.MapFrom(e => TimeZoneInfo.ConvertTimeFromUtc(e.StartTime.Value, timeZone));
                })
                .ForMember(dto => dto.BookTime, opt =>
                {
                    opt.PreCondition((src) => src.BookTime.HasValue);
                    opt.MapFrom(e => TimeZoneInfo.ConvertTimeFromUtc(e.BookTime.Value, timeZone));
                })
                .ForMember(dto => dto.ArriveTime, opt =>
                {
                    opt.PreCondition((src) => src.ArriveTime.HasValue);
                    opt.MapFrom(e => TimeZoneInfo.ConvertTimeFromUtc(e.ArriveTime.Value, timeZone));
                })
                // .ForMember(dto => dto.PhoneNumber, opt =>
                // {
                //     opt.PreCondition(src => src.Account != null && src.Account.Info != null);
                //     opt.MapFrom(src => src.Account.Info.PhoneNumber);
                // })
                .ForMember(dto => dto.FullName, opt =>
                {
                    opt.PreCondition(src => src.Account != null && src.Account.Info != null);
                    opt.MapFrom(src => src.Account.Info.FullName);
                })
                .ForMember(dto => dto.PhoneNumber, opt =>
                {
                    opt.PreCondition(src => src.Account != null);
                    opt.MapFrom(src => src.Account.PhoneNumber);
                })
                .ForMember(dto => dto.CarPark, opt =>
                {
                    opt.PreCondition(src => src.CarPark != null);
                    opt.MapFrom(src => src.CarPark);
                })
                .ForMember(dto => dto.PriceTable, opt =>
                {
                    opt.PreCondition(src => !string.IsNullOrEmpty(src.PriceTable));
                    opt.MapFrom((src, dest) => JsonSerializer.Deserialize<PriceTable>(src.PriceTable).Details);
                })
                .ForMember(dto => dto.FeePerHour, opt =>
                {
                    opt.PreCondition(src => !string.IsNullOrEmpty(src.PriceTable));
                    opt.MapFrom((src, dest) => JsonSerializer.Deserialize<PriceTable>(src.PriceTable).FeePerHour);
                });

            CreateMap<Ticket, TicketPreviewDTO>()
                .ForMember(dto => dto.CarParkAddress, opt =>
                {
                    opt.PreCondition(src => src.CarPark != null);
                    opt.MapFrom(src => $"{src.CarPark.AddressNumber}, {src.CarPark.Street}, {src.CarPark.Ward}, {src.CarPark.District}, {src.CarPark.City}");
                })
                // .ForMember(dto => dto.PhoneNumber, opt =>
                // {
                //     opt.PreCondition(src => src.Account != null && src.Account.Info != null);
                //     opt.MapFrom(src => src.Account.Info.PhoneNumber);
                // })
                .ForMember(dto => dto.ArriveTime,
                     opt => opt.MapFrom(src => TimeZoneInfo.ConvertTimeFromUtc(src.ArriveTime.Value, timeZone)))
                .ForMember(dto => dto.CarParkName, opt =>
                {
                    opt.PreCondition(src => src.CarPark != null);
                    opt.MapFrom(src => src.CarPark.Name);
                })
                .ForMember(dto => dto.PriceTable, opt =>
                {
                    opt.PreCondition(src => !string.IsNullOrEmpty(src.PriceTable));
                    opt.MapFrom((src, dest) => JsonSerializer.Deserialize<PriceTable>(src.PriceTable).Details);
                })
                .ForMember(dto => dto.FeePerHour, opt =>
                {
                    opt.PreCondition(src => !string.IsNullOrEmpty(src.PriceTable));
                    opt.MapFrom((src, dest) => JsonSerializer.Deserialize<PriceTable>(src.PriceTable).FeePerHour);
                });
            CreateMap<Feedback, FeedbackGetDTO>()
                .ForMember(dto => dto.Time, opt =>
                {
                    opt.PreCondition(src => src.Time.HasValue);
                    opt.MapFrom(src => TimeZoneInfo.ConvertTimeFromUtc(src.Time.Value, timeZone));
                });
            // .ForMember(dto => dto.PhoneNumber, opt =>
            // {
            //     opt.PreCondition(src => src.Account != null && src.Account.Info != null);
            //     opt.MapFrom(src => src.Account.Info.PhoneNumber);
            // });
            CreateMap<Transaction, TransactionGetDTO>()
                // .ForMember(dto => dto.PhoneNumber, opt =>
                // {
                //     opt.PreCondition(src => src.Account != null && src.Account.Info != null);
                //     opt.MapFrom(src => src.Account.Info.PhoneNumber);
                // })
                .ForMember(dto => dto.FullName, opt =>
                {
                    opt.PreCondition(src => src.Account != null && src.Account.Info != null);
                    opt.MapFrom(src => src.Account.Info.FullName);
                })
                .ForMember(dto => dto.PhoneNumber, opt =>
                {
                    opt.PreCondition(src => src.Account != null);
                    opt.MapFrom(src => src.Account.PhoneNumber);
                })
                .ForMember(dto => dto.Time, opt =>
                {
                    // opt.PreCondition(src => src.Time.HasValue);
                    opt.MapFrom(src => TimeZoneInfo.ConvertTimeFromUtc(src.Time, timeZone));
                });
            CreateMap<PriceTableDetail, PriceTableDetailDTO>();
            CreateMap<PriceTable, PriceTableGetDTO>()
                .ForMember(dto => dto.LastModified, opt =>
                {
                    opt.MapFrom(src => TimeZoneInfo.ConvertTimeFromUtc(src.LastModified, timeZone));
                });
            CreateMap<CarPark, ParkingCountDTO>();
        }
    }
}