using Models;

namespace API.Helpers;

public static class ParkingFeeUtilities
{
    public static decimal CalculateParkingFee(IEnumerable<PriceTableDetail> priceTable, DateTime from, DateTime to, string timeZone = "SE Asia Standard Time")
    {
        var zone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
        from = TimeZoneInfo.ConvertTimeFromUtc(from, zone);
        to = TimeZoneInfo.ConvertTimeFromUtc(to, zone);
        var time = from;
        decimal total = 0;
        for (time = from; time <= to; time = time.AddHours(1))
        {
            if (time == to)
            {
                if (time.Minute < 5)
                {
                    break;
                }
            }
            foreach (var fee in priceTable)
            {
                if (CheckTimeBetweenAnInterval(fee.FromTime, fee.ToTime, TimeSpan.FromHours(Math.Floor(time.TimeOfDay.TotalHours))))
                {
                    total += fee.Fee;
                    break;
                }
            }
        }
        return total;
    }

    public static bool CheckTimeBetweenAnInterval(TimeSpan start, TimeSpan end, TimeSpan time)
    {
        if (start <= end)
        {
            if (time >= start && time <= end)
            {
                return true;
            }
        }
        else
        {
            //start and end are in different days
            if (time >= start || time <= end)
            {
                return true;
            }
        }
        return false;
    }
}