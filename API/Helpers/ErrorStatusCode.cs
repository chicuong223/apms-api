﻿namespace API.Helpers
{
    public enum ErrorStatusCode
    {
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        InternalServerError = 500
    }
}