namespace API.Helpers;

public class JwtSettings
{
    public string SecurityKey { get; set; }
    public int ExpireTime { get; set; }
    public string DefaultAuthenticateScheme { get; set; }
}