namespace API.Helpers;

public class MathUtils
{
    private static double ConvertDegreeToRadian(double deg)
    {
        return (Math.PI / 180) * deg;
    }

    public static double CalculateDistanctFromCoordinationInKm(double lat1, double long1, double lat2, double long2)
    {
        const double R = 6371; //Radius of the Earth in km
        var degLat = ConvertDegreeToRadian(lat2 - lat1);
        var degLong = ConvertDegreeToRadian(long2 - long1);
        var a =
            Math.Sin(degLat / 2) * Math.Sin(degLat / 2) +
            Math.Cos(ConvertDegreeToRadian(lat1)) * Math.Cos(ConvertDegreeToRadian(lat2)) *
            Math.Sin(degLong / 2) * Math.Sin(degLong / 2);
        var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        var d = R * c;
        return d;
    }
}