namespace API.Helpers;

public record CheckinModel(
    Guid CarParkId,
    string PlateNumber,
    IFormFile Pic
);