using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;

namespace API.Helpers;

// public interface IFirebaseService
// {
//     void OpenServo(string servoName);
// }

public class FirebaseService
{
    private readonly IConfiguration _config;
    private readonly IFirebaseConfig _firebaseConfig;
    private readonly IFirebaseClient _client;

    public FirebaseService(IConfiguration config)
    {
        _config = config;
        _firebaseConfig = new FirebaseConfig
        {
            AuthSecret = config["Firebase:AuthSecret"],
            BasePath = config["Firebase:BasePath"]
        };
        _client = new FirebaseClient(_firebaseConfig);
    }

    public void OpenServo(string servoName)
    {
        _client.Set(servoName, "OPEN");
    }

    public string GetServoValue(string servoName)
    {
        var result = _client.Get(servoName).Body;
        return result;
    }
}