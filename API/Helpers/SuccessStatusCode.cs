﻿namespace API.Helpers
{
    public enum SuccessStatusCode
    {
        Ok = 200,
        Created = 201,
        NoContent = 204
    }
}