﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Models.Migrations
{
    public partial class ChangeUserIDAndMovePhoneNumberToUserInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CarPark",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    AddressNumber = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Street = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    City = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    District = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Ward = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    Latitude = table.Column<double>(type: "double precision", nullable: false),
                    Longitude = table.Column<double>(type: "double precision", nullable: false),
                    ProvinceId = table.Column<int>(type: "integer", nullable: false),
                    AvailableSlotsCount = table.Column<int>(type: "integer", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false, defaultValue: 0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarPark", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PriceTable",
                columns: table => new
                {
                    CarParkId = table.Column<Guid>(type: "uuid", nullable: false),
                    LastModified = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    ReservationFeePercentage = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceTable", x => x.CarParkId);
                    table.ForeignKey(
                        name: "FK_PriceTable_CarPark_CarParkId",
                        column: x => x.CarParkId,
                        principalTable: "CarPark",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Password = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Salt = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    RoleId = table.Column<int>(type: "integer", nullable: false),
                    CarParkId = table.Column<Guid>(type: "uuid", nullable: true),
                    Active = table.Column<bool>(type: "boolean", nullable: true, defaultValue: true),
                    CanEdit = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Account_CarPark_CarParkId",
                        column: x => x.CarParkId,
                        principalTable: "CarPark",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Account_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PriceTableDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FromTime = table.Column<TimeSpan>(type: "interval", nullable: false),
                    ToTime = table.Column<TimeSpan>(type: "interval", nullable: false),
                    Fee = table.Column<decimal>(type: "numeric", nullable: false),
                    CarParkId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceTableDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PriceTableDetail_PriceTable_CarParkId",
                        column: x => x.CarParkId,
                        principalTable: "PriceTable",
                        principalColumn: "CarParkId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Feedback",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Description = table.Column<string>(type: "character varying(10000)", maxLength: 10000, nullable: false),
                    Time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValue: new DateTime(2022, 11, 17, 7, 6, 20, 479, DateTimeKind.Utc).AddTicks(3854)),
                    AccountId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedback", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feedback_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LicensePlate",
                columns: table => new
                {
                    PlateNumber = table.Column<string>(type: "text", nullable: false),
                    AccountId = table.Column<string>(type: "text", nullable: false),
                    UsageCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicensePlate", x => new { x.PlateNumber, x.AccountId });
                    table.ForeignKey(
                        name: "FK_LicensePlate_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ticket",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    TotalFee = table.Column<decimal>(type: "numeric", nullable: false),
                    PicInUrl = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    PicOutUrl = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    CarParkId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlateNumber = table.Column<string>(type: "text", nullable: false),
                    ReservationFee = table.Column<decimal>(type: "numeric", nullable: false),
                    BookTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    ArriveTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    CancellerAccountId = table.Column<string>(type: "text", nullable: true),
                    AccountId = table.Column<string>(type: "text", nullable: false),
                    PriceTable = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ticket", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ticket_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ticket_Account_CancellerAccountId",
                        column: x => x.CancellerAccountId,
                        principalTable: "Account",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Ticket_CarPark_CarParkId",
                        column: x => x.CarParkId,
                        principalTable: "CarPark",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Amount = table.Column<decimal>(type: "numeric", nullable: false),
                    Time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValue: new DateTime(2022, 11, 17, 7, 6, 20, 480, DateTimeKind.Utc).AddTicks(9896)),
                    Success = table.Column<bool>(type: "boolean", nullable: false),
                    AccountId = table.Column<string>(type: "text", nullable: false),
                    TransactionTypeId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transaction_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserInfo",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    FullName = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    PhoneNumber = table.Column<string>(type: "text", nullable: false),
                    AccountBalance = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserInfo_Account_Id",
                        column: x => x.Id,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "CarPark",
                columns: new[] { "Id", "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "Name", "PhoneNumber", "ProvinceId", "Status", "Street", "Ward" },
                values: new object[] { new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "500", 100, "Ho Chi Minh City", "1", 123.0, 123.0, "Test", "0902589751", 2, 0, "Test street", "Test ward" });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "Title" },
                values: new object[,]
                {
                    { 1, "Admin" },
                    { 2, "Owner" },
                    { 3, "Staff" },
                    { 4, "Customer" }
                });

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Active", "CanEdit", "CarParkId", "Password", "RoleId", "Salt" },
                values: new object[,]
                {
                    { "adm000001", true, false, null, "DZqyD4OGrIk8s9pKiILMr8EwrZ6F+NTOOomc9HZAOjc=", 1, "YqwwZvimkBfmk6kTm9TGew==" },
                    { "cus000001", true, false, null, "Ty/ktv+WcIcqSM0HxihwD451hEcv/caYKMfF/FQbmMI=", 4, "yufFz52cbJy1YIgW5UeLNA==" },
                    { "cus000002", true, false, null, "TNBKnNd5mENWqul/yk5wAPZyBrqj6ppny1TCOEcC1x4=", 4, "GqnsBkI17QN/ZBXUKZ5lPw==" },
                    { "own000001", true, false, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "mQYy0t4QTsTldMZwv9Hs/ofbRAf/lsHBUYDjtHx3OHQ=", 2, "mE9QELW8ogh3fb/MXNkc9A==" },
                    { "stf000001", true, false, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "UrjDg8HxI/6n3qo4JC5tL7te0SVXDuK9cgs6soNDmIs=", 3, "iP43j9SlIiGTaUX4bUMhOw==" }
                });

            migrationBuilder.InsertData(
                table: "PriceTable",
                columns: new[] { "CarParkId", "LastModified", "ReservationFeePercentage" },
                values: new object[] { new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), new DateTime(2022, 11, 17, 7, 6, 20, 481, DateTimeKind.Utc).AddTicks(5886), 30m });

            migrationBuilder.InsertData(
                table: "LicensePlate",
                columns: new[] { "AccountId", "PlateNumber", "UsageCount" },
                values: new object[,]
                {
                    { "cus000001", "51H-68329", 0 },
                    { "cus000002", "51H-68329", 0 },
                    { "cus000001", "51X-5740", 0 },
                    { "cus000002", "51X-87634", 0 }
                });

            migrationBuilder.InsertData(
                table: "PriceTableDetail",
                columns: new[] { "Id", "CarParkId", "Fee", "FromTime", "ToTime" },
                values: new object[,]
                {
                    { 1, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), 30000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 2, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), 35000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) }
                });

            migrationBuilder.InsertData(
                table: "UserInfo",
                columns: new[] { "Id", "AccountBalance", "FullName", "PhoneNumber" },
                values: new object[,]
                {
                    { "cus000001", 1000000m, "Ho Huu Phat", "0932781745" },
                    { "cus000002", 500000m, "Khuc Ngoc Thai", "0949936732" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Account_CarParkId",
                table: "Account",
                column: "CarParkId");

            migrationBuilder.CreateIndex(
                name: "IX_Account_RoleId",
                table: "Account",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Account_Salt",
                table: "Account",
                column: "Salt",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Feedback_AccountId",
                table: "Feedback",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_LicensePlate_AccountId",
                table: "LicensePlate",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceTableDetail_CarParkId",
                table: "PriceTableDetail",
                column: "CarParkId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_Title",
                table: "Role",
                column: "Title",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_AccountId",
                table: "Ticket",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_CancellerAccountId",
                table: "Ticket",
                column: "CancellerAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_CarParkId",
                table: "Ticket",
                column: "CarParkId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_StartTime_PlateNumber_Status",
                table: "Ticket",
                columns: new[] { "StartTime", "PlateNumber", "Status" });

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_AccountId",
                table: "Transaction",
                column: "AccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Feedback");

            migrationBuilder.DropTable(
                name: "LicensePlate");

            migrationBuilder.DropTable(
                name: "PriceTableDetail");

            migrationBuilder.DropTable(
                name: "Ticket");

            migrationBuilder.DropTable(
                name: "Transaction");

            migrationBuilder.DropTable(
                name: "UserInfo");

            migrationBuilder.DropTable(
                name: "PriceTable");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "CarPark");

            migrationBuilder.DropTable(
                name: "Role");
        }
    }
}