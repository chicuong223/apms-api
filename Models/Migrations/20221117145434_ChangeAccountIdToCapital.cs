﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class ChangeAccountIdToCapital : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "adm000001");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "cus000001");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "cus000002");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "own000001");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "stf000001");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 186, DateTimeKind.Utc).AddTicks(3220),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 17, 7, 6, 20, 480, DateTimeKind.Utc).AddTicks(9896));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 184, DateTimeKind.Utc).AddTicks(9244),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 17, 7, 6, 20, 479, DateTimeKind.Utc).AddTicks(3854));

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Active", "CanEdit", "CarParkId", "Password", "RoleId", "Salt" },
                values: new object[,]
                {
                    { "ADM000001", true, false, null, "ojth72ivG+TLbNKCZ9oniNx2FqSuayD2QK8UqnpwE9c=", 1, "ronnc+Fa5m72iSmp+/HmSQ==" },
                    { "CUS000001", true, false, null, "1sWqkprBe6WoA/stoMi+0usQEW9r8MVI/GSC/4M3rLE=", 4, "WUooaxu2VIt/sUx4qVlDNw==" },
                    { "CUS000002", true, false, null, "M8dLhaTfWk9Mg1ePX38PBbrFTUxCXp7GC7OlOaSyNaI=", 4, "ll7oEz6JXxxIQZAHYqnDfA==" },
                    { "OWN000001", true, false, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "GUTcLZRvchtUK5b0Z+TGzN+d5n8aKog5czQsBVTxpSc=", 2, "5lbgZum79IP/mRo0fuHe9g==" },
                    { "STF000001", true, false, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "d4D3TJqqQ98jFTVe5LPiT5BNYeb56PXtHLQQShHbFAE=", 3, "xYlpE26oodQbKtkvXJ1V4Q==" }
                });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 17, 14, 54, 34, 186, DateTimeKind.Utc).AddTicks(8440));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 17, 7, 6, 20, 480, DateTimeKind.Utc).AddTicks(9896),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 186, DateTimeKind.Utc).AddTicks(3220));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 17, 7, 6, 20, 479, DateTimeKind.Utc).AddTicks(3854),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 184, DateTimeKind.Utc).AddTicks(9244));

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Active", "CanEdit", "CarParkId", "Password", "RoleId", "Salt" },
                values: new object[,]
                {
                    { "adm000001", true, false, null, "DZqyD4OGrIk8s9pKiILMr8EwrZ6F+NTOOomc9HZAOjc=", 1, "YqwwZvimkBfmk6kTm9TGew==" },
                    { "cus000001", true, false, null, "Ty/ktv+WcIcqSM0HxihwD451hEcv/caYKMfF/FQbmMI=", 4, "yufFz52cbJy1YIgW5UeLNA==" },
                    { "cus000002", true, false, null, "TNBKnNd5mENWqul/yk5wAPZyBrqj6ppny1TCOEcC1x4=", 4, "GqnsBkI17QN/ZBXUKZ5lPw==" },
                    { "own000001", true, false, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "mQYy0t4QTsTldMZwv9Hs/ofbRAf/lsHBUYDjtHx3OHQ=", 2, "mE9QELW8ogh3fb/MXNkc9A==" },
                    { "stf000001", true, false, new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), "UrjDg8HxI/6n3qo4JC5tL7te0SVXDuK9cgs6soNDmIs=", 3, "iP43j9SlIiGTaUX4bUMhOw==" }
                });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 17, 7, 6, 20, 481, DateTimeKind.Utc).AddTicks(5886));
        }
    }
}