﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class ParkingCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 278, DateTimeKind.Utc).AddTicks(1775),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 781, DateTimeKind.Utc).AddTicks(3387));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 277, DateTimeKind.Utc).AddTicks(359),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 780, DateTimeKind.Utc).AddTicks(3070));

            migrationBuilder.CreateTable(
                name: "Parking",
                columns: table => new
                {
                    AccountId = table.Column<string>(type: "text", nullable: false),
                    CarParkId = table.Column<Guid>(type: "uuid", nullable: false),
                    UseCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parking", x => new { x.AccountId, x.CarParkId });
                    table.ForeignKey(
                        name: "FK_Parking_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Parking_CarPark_CarParkId",
                        column: x => x.CarParkId,
                        principalTable: "CarPark",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "20H4JKC+8IWrC/Mn82lOXyWMp4HvVT2Ie3f/6szALkA=", "L78pGKKnTgCyMUQTRLsZ1w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "CdesSkiakzeAmSTWFvr0kKuiMizlutZtK8TD8x6vwpI=", "Q4egekWWfFiIR1vzwk6mpg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "frJzaNXd3eRR+bgt+yqxMDOLdzwX4M6GbsKJSRZEQ8k=", "iII2Yf44ezF6Z1U3J0oN2g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "NNQchqhpFo4T9ztI5rXZK8vYOJsdp4Q9Sr/3IngnDEo=", "LhFM/xY7Er29iq9h7flz2g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hZGt+efvdwh0TWtdd+fVyLJTiPsUo5XctTNcMbRFrO8=", "rwwmOrCr4zz5smDWUkUOjA==" });

            migrationBuilder.InsertData(
                table: "Parking",
                columns: new[] { "AccountId", "CarParkId", "UseCount" },
                values: new object[,]
                {
                    { "CUS000001", new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), 60 },
                    { "CUS000001", new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), 25 },
                    { "CUS000001", new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), 100 },
                    { "CUS000001", new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), 1 }
                });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 7, 35, 17, 278, DateTimeKind.Utc).AddTicks(8511));

            migrationBuilder.CreateIndex(
                name: "IX_Parking_CarParkId",
                table: "Parking",
                column: "CarParkId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Parking");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 781, DateTimeKind.Utc).AddTicks(3387),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 278, DateTimeKind.Utc).AddTicks(1775));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 780, DateTimeKind.Utc).AddTicks(3070),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 277, DateTimeKind.Utc).AddTicks(359));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "B1e+014v4WuaErBfNXTS7Tp616ONwbKzj6l58jtPeZE=", "77tT4tnCTOPwB3EWytxZqw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HzF/aEgvIz9dZME7U73od4cq4jsP+s8TY40V4gPCSRs=", "RTDed+w/eBa9GHnxLOC6pw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "u8cYPkh6VMlSBm3NJipq26kwmecbtB61Cl6gTWFAcok=", "814ZVUBgV/6DkkgZ++9X/Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "1nmzc9KSEYsBlG0rB+4IOEn/qKyDIWDBhNZrOdLoTyE=", "+aewFPoZegQSqxRAT8mi/g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "EPrEqS88waESUU6FuUT6oFwjzI3EpxV16dB3s0yHL9I=", "65isiWk+iAfhyFF2jLJuzw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 7, 15, 29, 781, DateTimeKind.Utc).AddTicks(7956));
        }
    }
}