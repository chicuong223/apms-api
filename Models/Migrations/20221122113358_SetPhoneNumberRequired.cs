﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class SetPhoneNumberRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 11, 33, 57, 801, DateTimeKind.Utc).AddTicks(8677),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(674));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 11, 33, 57, 800, DateTimeKind.Utc).AddTicks(8515),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 884, DateTimeKind.Utc).AddTicks(1436));

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "Account",
                type: "character varying(14)",
                maxLength: 14,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "9cheazreWfDemaHonNEkrqk9SHdx81jFq2csn84TK3Y=", "9siV/1VmRCPtsPKi0XRtgQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Bm/eti5+yZhe71qQs3WTXlw1ozcoZ4DCA6PI0cy2daI=", "NLimp0oqK3nNbgi4EjBNMA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "5jN4hhukZUs+DMLKFE9d8eCqJvws7I6aEMYJqASyCro=", "p5VB1+qZZSarnmy/hb0EYA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "9jo6/4P9GknDibkBsRewersgFwX1bFB+jBYqwoxc9B4=", "7jtTKxPH/zzziWZ0xwb82g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "qrBMo0UnbzIC/StDuKNst/dCdMa1A5iDyJFhngQsY9A=", "BOlFReVeEX403v+ESgK64A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "0TWT9/D31E2cMvTX2FoF/d4pIjmTrJJDGIhX/KJvNwc=", "WYsDxG22efEwG/pPCb1ttw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3728));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3710));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3725));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3717));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3715));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3732));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3721));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3719));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3731));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 33, 57, 802, DateTimeKind.Utc).AddTicks(3723));

            migrationBuilder.CreateIndex(
                name: "IX_Account_PhoneNumber",
                table: "Account",
                column: "PhoneNumber",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Account_PhoneNumber",
                table: "Account");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(674),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 11, 33, 57, 801, DateTimeKind.Utc).AddTicks(8677));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 884, DateTimeKind.Utc).AddTicks(1436),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 11, 33, 57, 800, DateTimeKind.Utc).AddTicks(8515));

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "Account",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(14)",
                oldMaxLength: 14);

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HR5e/zaWHlWadaHRd6patAprAWEbqVMpNPuM2ykvU0U=", "D6pcDsHzaKFBgiPOc32kBg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "3udi1qzLuu/HS/hLiodAzqlqYmzAKhPIynRjcD1nofM=", "n0FVPxgtMicz5cFIZSz4Zw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "9BuGrCVP7wQcPDY25ykdnMF9/lVVTfg8uJdARr7WAis=", "Xz5fmlDJ5smMi+5Knii2ng==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BptyWcV6mKn/dFLoS5aUU5ZTw9WL13ztmnrNg6RxLyk=", "xCV31LXh4iH7ljHa6+ogkw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "h105xIhVqM55wD0I0cBT1QVbhTcoVGkBWvejWhx88pA=", "o4fjCj9xnu943oGnDJj2bw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "JnF1ILLBhl4AtwhNRLrKwZEPkOm/geomn9Oa4u4F5Dk=", "gKo9rgUPRCSrFuqcgmqDVg==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5049));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5031));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5047));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5038));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5036));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5053));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5043));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5041));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5051));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5045));
        }
    }
}