﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class SeedMoreCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(674),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(4177));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 884, DateTimeKind.Utc).AddTicks(1436),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 312, DateTimeKind.Utc).AddTicks(1386));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HR5e/zaWHlWadaHRd6patAprAWEbqVMpNPuM2ykvU0U=", "D6pcDsHzaKFBgiPOc32kBg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "3udi1qzLuu/HS/hLiodAzqlqYmzAKhPIynRjcD1nofM=", "n0FVPxgtMicz5cFIZSz4Zw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "9BuGrCVP7wQcPDY25ykdnMF9/lVVTfg8uJdARr7WAis=", "Xz5fmlDJ5smMi+5Knii2ng==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "h105xIhVqM55wD0I0cBT1QVbhTcoVGkBWvejWhx88pA=", "0902879231", "o4fjCj9xnu943oGnDJj2bw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "JnF1ILLBhl4AtwhNRLrKwZEPkOm/geomn9Oa4u4F5Dk=", "gKo9rgUPRCSrFuqcgmqDVg==" });

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Active", "CanEdit", "CarParkId", "Password", "PhoneNumber", "RoleId", "Salt" },
                values: new object[] { "CUS000003", true, false, null, "BptyWcV6mKn/dFLoS5aUU5ZTw9WL13ztmnrNg6RxLyk=", "0908436393", 4, "xCV31LXh4iH7ljHa6+ogkw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5049));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5031));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5047));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5038));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5036));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5053));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5043));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5041));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5051));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(5045));

            migrationBuilder.InsertData(
                table: "Parking",
                columns: new[] { "AccountId", "CarParkId", "UseCount" },
                values: new object[,]
                {
                    { "CUS000003", new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), 60 },
                    { "CUS000003", new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), 25 },
                    { "CUS000003", new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), 100 },
                    { "CUS000003", new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1") });

            migrationBuilder.DeleteData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc") });

            migrationBuilder.DeleteData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b") });

            migrationBuilder.DeleteData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef") });

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(4177),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 885, DateTimeKind.Utc).AddTicks(674));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 312, DateTimeKind.Utc).AddTicks(1386),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 11, 7, 1, 884, DateTimeKind.Utc).AddTicks(1436));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BEUAswCNSzjK8T6s1OC0BRg7EPhJUlwNWAPNVw5NYNk=", "YAYQjstXW1DqN6BIIfNIpQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "a7v7wJeMZ1tvsm+lz0PpnTVjSvv75Ygl+rNaeksxvE4=", "EYy63ssrnZk1beBNMNcxZQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "FQL1Q5HXweolIRecrtHn1ZxgYAZhnXizgaV+UFaE4aU=", "W5LH+LLMthsaS+PFC3Nk9w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "nhOUNaVK9+nXwDA4MEY5OSnPm24luYY6CO+merFfuOE=", "0908436393", "VmTialuV3ayfya1bXQEnVA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Tu2c5TL4rU66T+1KnxiOP5G5LKJ/HA3yCt7NrkLXf+o=", "45JGah7EO/o6DxfGNfYIeg==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9873));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9853));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9871));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9861));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9859));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9878));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9866));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9864));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9875));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9868));
        }
    }
}