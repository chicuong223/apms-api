﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class AddCanEditCarParkIntoAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 667, DateTimeKind.Utc).AddTicks(6233),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(1660));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 666, DateTimeKind.Utc).AddTicks(1921),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 842, DateTimeKind.Utc).AddTicks(6409));

            migrationBuilder.AddColumn<bool>(
                name: "CanEditCarPark",
                table: "Account",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "kX9KT1pmuUCTRTXBHuPUHoXQEtHv+bfPAsB09kP+0s8=", "XD/KwW4aaoZhuBGGoL3M+g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "PnlHZ9Acf9Z3ZWi9ofnYp82hEwrneLhGpFdy9hsQy/o=", "HNxR3sJz7b//hy3sffk+mA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "S4o2sHQZuwx2WWqJ5KGPyEalyOPi7wzs5AQ4IK7SBNs=", "vwu0RnCZWSorS5HbsHnnbA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BjULwC+Utbg/WZKSOPcsXjdQK+3ZqDtiszwKwCzVmMY=", "V0ge0CIGmzSOopESh0817w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Yw8VUwNDA1818Pwp9mW08zWGRPH8NjOgzm8mHWDc3DQ=", "ZC3JYNCGxBtFYizj5iYqYQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "weCd8sPN8oKUWHB5tzQYIjNySchny78ENWjxIiTLJus=", "YuOdx7DEe0U04ZsDMa1SgQ==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1339));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1316));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1337));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1328));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1324));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1344));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1332));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1330));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1352));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1342));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1333));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanEditCarPark",
                table: "Account");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(1660),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 667, DateTimeKind.Utc).AddTicks(6233));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 842, DateTimeKind.Utc).AddTicks(6409),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 666, DateTimeKind.Utc).AddTicks(1921));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "EN2PK0edBhrEn1Pbs3mLzc6SExroEJVbqz6rlPUjb6Q=", "kdECMzBgg6l8aUFfDVQs+A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "mybV23nEc2OGnKy0MEnfjuzLx0Xe/QwmNDCVqzy+KZA=", "0faA6Y/e9u45o34BxjG2oA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "G99lDsKdO/2g53/0XKiEgwQKf4bNw7YmKDUv4PS1NQw=", "wCU6MZ9cUZCm5AgIhiVEmQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "uFKlNjSi+KdinGLu9NcC0hTSTETKCzqPXq9mWSz09es=", "tLBo2buMF9l2uurpGNAecw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "VNS8W3xmPUD53os5NmWCZdF4FqW4NlGTNWPnlFRGf2M=", "LAHqSW2YzjoQhRlZpOv0JA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "LYKJP+bAtf51bB3nZbTUb/G0LNdz1LxaszlkTZBw47w=", "hRhGeF6SISme8iz3V0bkpw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8138));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8115));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8135));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8126));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8123));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8142));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8131));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8129));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8149));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8140));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8133));
        }
    }
}