﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class AddFeePerHourInPriceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 239, DateTimeKind.Utc).AddTicks(7763),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 13, 12, 15, 467, DateTimeKind.Utc).AddTicks(8323));

            migrationBuilder.AddColumn<decimal>(
                name: "FeePerHour",
                table: "PriceTable",
                type: "numeric",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 238, DateTimeKind.Utc).AddTicks(7522),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 13, 12, 15, 466, DateTimeKind.Utc).AddTicks(9016));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "r3uRR9fzO4/APJEaYgLUfAzUZHPEaHC2wMKGQQq6q3g=", "pYjsHYH0b2iyfZJg5h1ssw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "jvnIUWEaOjQ1f4jxrHL+WNkGuh7qgF2Mw3OjrEYbknA=", "5+cJjyhsZfvr4eWtS6AFDA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "pw5ufoavH/B2MPHksVzOEq+gFDoaI387ZYQjdQvgn1U=", "YcIWIMCnZlsdVyfFzoxSmw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "vkj0jF0S0migNe6E35Cqp8z6Znu6CK4GTvAREd4HCQA=", "yK5Xhbr1zJ5MEYCTG7em1Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "S0DJfbGnDvTvcOnjnwGp3k05iOqYEzdJsM670bzyvHw=", "3b0iMrnn2RB2Z3Ah+Yw5bg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cVNCA3RFK/3BTce9xlyTmRCloELZSM3j38ZZMTQ/Htk=", "eLmGvq1IBY4560jG4k3Eug==" });

            migrationBuilder.InsertData(
                table: "CarPark",
                columns: new[] { "Id", "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "Name", "PhoneNumber", "ProvinceId", "Status", "Street", "Ward" },
                values: new object[] { new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"), "333", 3000, "Bình Dương", "Thuận An", 10.94816, 106.70626, "City Tower Bình Dương", "0915592179", 76, 0, "Đại lộ Bình Dương", "" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2872));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2811));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2870));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2823));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2820));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2877));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2865));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2828));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2875));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2868));

            migrationBuilder.InsertData(
                table: "PriceTable",
                columns: new[] { "CarParkId", "FeePerHour", "LastModified", "ReservationFeePercentage" },
                values: new object[] { new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"), 35000m, new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2879), 30m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"));

            migrationBuilder.DropColumn(
                name: "FeePerHour",
                table: "PriceTable");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 13, 12, 15, 467, DateTimeKind.Utc).AddTicks(8323),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 239, DateTimeKind.Utc).AddTicks(7763));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 13, 12, 15, 466, DateTimeKind.Utc).AddTicks(9016),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 238, DateTimeKind.Utc).AddTicks(7522));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Cj4fevQVQgKhzijsESC7Qdpu46Idey1ZWMe95x8TS3k=", "WNuKlMSxtNoRhgzpCZdHUA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "gqFnIJIISatMA4eMpDkUlCh/e9VERwZEU3/z43kWSdw=", "NcsxjYxBfFcHzCtyzZkfyQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ypXHv40+26NQugdaamFwBGAbIalcXdBpQAi9Vi0m2I8=", "pwtSu+js8fddp3I1L/ONOw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "iAdKLd89zfxabk0Ga9fA9h3vl3vKMrhz5n/OVcjCwuI=", "T9vgDle56R6N3O2Ji/kpfA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "FDKXcK1spZFpym2suomNBIkxRcIW2HsN5mEpO94fvgE=", "80JJWT30a5vduaiP0Brbcg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "wZ19qQIwJ40Tfg900kRwSbdbrns23mZ76Jm2E0nFN5E=", "oS5EeWNVm1ueWDXzejJhng==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2564));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2544));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2561));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2554));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2550));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2568));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2558));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2556));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2566));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 13, 12, 15, 468, DateTimeKind.Utc).AddTicks(2560));
        }
    }
}