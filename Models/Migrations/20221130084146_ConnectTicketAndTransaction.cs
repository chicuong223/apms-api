﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class ConnectTicketAndTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransactionTypeId",
                table: "Transaction",
                newName: "TransactionType");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(1655),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(3750));

            migrationBuilder.AddColumn<Guid>(
                name: "TicketId",
                table: "Transaction",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 30, 8, 41, 45, 732, DateTimeKind.Utc).AddTicks(3241),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 180, DateTimeKind.Utc).AddTicks(4190));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "rr7eWJKPEA2dAie8ksQ1IxdlPAy3qEGIoYZFXLemySc=", "k8pIFJg1T+aYjqEmeCKpDg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "zPDemn9npxZZGRMjXDUF+zQoUmVEprOJHmtLPSUg1B8=", "hgC8lJUD9pcLs/btteTBZw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "TsbMLsGDRusuQoZTttU7dcmKdC591iNhI9NcvFU1+xI=", "KsB8mb6Bg6uxKv2HclpyMQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "8cCLvdrLoFbKWg7ry/HT0D39oaq3wSswCa6WL7EAA8o=", "Yp2piSb7Sf9TMMHPO657Wg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "nzlRmAmA+gIHlt6LFQAtVn/Cu/R6DI1c3cSHd2f/cmI=", "GZI4k4X8MPXOGMYeSTRjVg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "x1PFMuLHKPpjP0TKdbBuAuqR/FgSWubGEM9dyPwUbkw=", "5YIacDyW2L8KyiLi4TH6mQ==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                columns: new[] { "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "MaxCapacity", "Name", "Street", "Ward" },
                values: new object[] { "25", 200, "Thành phố Hồ Chí Minh", "Thành phố Thủ Đức", 10.76534, 106.75936, 200, "Bãi đậu xe 25 Quách Giai", "Quách Giai", "Thạnh Mỹ Lợi" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "City",
                value: "Thành phố Hồ Chí Minh");

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6478));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6460));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6476));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6469));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6466));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6482));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6473));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6471));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6489));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6480));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(6475));

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_TicketId",
                table: "Transaction",
                column: "TicketId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_Ticket_TicketId",
                table: "Transaction",
                column: "TicketId",
                principalTable: "Ticket",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_Ticket_TicketId",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_TicketId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "TicketId",
                table: "Transaction");

            migrationBuilder.RenameColumn(
                name: "TransactionType",
                table: "Transaction",
                newName: "TransactionTypeId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(3750),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 30, 8, 41, 45, 734, DateTimeKind.Utc).AddTicks(1655));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 180, DateTimeKind.Utc).AddTicks(4190),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 30, 8, 41, 45, 732, DateTimeKind.Utc).AddTicks(3241));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "WCQmF68+EAxkPY87qPgR+LeKWD83Hr3N3R0MS93+r7g=", "+GJUE8XlL7yf/t+SeX11kg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "p2tS3sD2YdgnAdMLKGy55YXfSfElSSmiE3EF+Mc35XA=", "2Ce4d4zJCiXuyKFQwAfwZg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "bCmoEyRHq0Whv+cWVOZKRoUg9rG6Rl1+IDLSlUcnGso=", "pWErU2PoYN/dqtHw4NSWaw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "DTy475GGac8hDKx2vng5ZA1wQEURgTBR1RJKnKGmIjk=", "j5NE6kTni+spYVu3zKejdQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cKQbPXqDsgOf1eXG71kKlHzLP7Fco5bNLj2PTGTmN30=", "CLfZTH67dDbpBTsXgU3YVg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cF23uGGBgZJJ+8i9whEejbMdshzsA3FrduZJWSSpz88=", "VBokyzVrtb5TVd5PUr48WQ==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                columns: new[] { "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "MaxCapacity", "Name", "Street", "Ward" },
                values: new object[] { "45A", 3000, "Ho Chi Minh City", "Quận 1", 10.778460000000001, 106.70187, 3000, "Vincom Đồng Khởi", "Lý Tự Trọng", "Bến Nghé" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "City",
                value: "Ho Chi Minh City");

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8328));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8305));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8326));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8313));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8311));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8332));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8318));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8315));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8343));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8330));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8322));
        }
    }
}