﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class LastVisitedInParking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 891, DateTimeKind.Utc).AddTicks(3748),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 239, DateTimeKind.Utc).AddTicks(7763));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastVisited",
                table: "Parking",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 889, DateTimeKind.Utc).AddTicks(6975),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 238, DateTimeKind.Utc).AddTicks(7522));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SxjZeBulI/ZpsArmzRuBrP1MPFe8R0sUBhrCzLUNht0=", "f9hgz9MzNdhe1IPUzqI3iQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "LCwBL4YYkcLLRlBot1su2RpT1cN9vZQPnwyTkGeTdz4=", "c+LVdi5xDKtDbb0ekvjVqg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "nf2ZlHQJl2SGJZZIM32Y446l4mce2Dv+u6sRGC3+Jc0=", "5EEQX5tUF3WZWrpFK0mFGg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Fwnx7Tl+IFMwJ7uXDCMUzaWVyq36h+YJW3ZSm5tHtG8=", "8mNAlOqFL71TW7YfPmaQwg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Dzcn6yfmtmb2+2HVOFN+X0c2RHM5yi+YYeAcdjkTLcY=", "4H2W+v9C+Hrhej7PgALYUw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "B87COHQXi3ylhofuSIHzdcGtm3vZ7dZoq/CrEiSzz4s=", "1/KIjDnwiY1YxF2QNyKmrA==" });

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000001", new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 20, 0, 28, 51, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000001", new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 20, 5, 27, 16, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000001", new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 11, 12, 35, 25, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000001", new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 21, 1, 48, 27, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 20, 5, 27, 16, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 20, 0, 28, 51, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 20, 5, 27, 16, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "Parking",
                keyColumns: new[] { "AccountId", "CarParkId" },
                keyValues: new object[] { "CUS000003", new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef") },
                column: "LastVisited",
                value: new DateTime(2022, 11, 23, 7, 35, 25, 0, DateTimeKind.Utc));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(750));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(718));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(746));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(733));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(730));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(757));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(740));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(737));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(770));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(753));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(744));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastVisited",
                table: "Parking");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 239, DateTimeKind.Utc).AddTicks(7763),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 891, DateTimeKind.Utc).AddTicks(3748));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 7, 40, 45, 238, DateTimeKind.Utc).AddTicks(7522),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 889, DateTimeKind.Utc).AddTicks(6975));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "r3uRR9fzO4/APJEaYgLUfAzUZHPEaHC2wMKGQQq6q3g=", "pYjsHYH0b2iyfZJg5h1ssw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "jvnIUWEaOjQ1f4jxrHL+WNkGuh7qgF2Mw3OjrEYbknA=", "5+cJjyhsZfvr4eWtS6AFDA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "pw5ufoavH/B2MPHksVzOEq+gFDoaI387ZYQjdQvgn1U=", "YcIWIMCnZlsdVyfFzoxSmw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "vkj0jF0S0migNe6E35Cqp8z6Znu6CK4GTvAREd4HCQA=", "yK5Xhbr1zJ5MEYCTG7em1Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "S0DJfbGnDvTvcOnjnwGp3k05iOqYEzdJsM670bzyvHw=", "3b0iMrnn2RB2Z3Ah+Yw5bg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cVNCA3RFK/3BTce9xlyTmRCloELZSM3j38ZZMTQ/Htk=", "eLmGvq1IBY4560jG4k3Eug==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2872));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2811));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2870));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2823));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2820));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2877));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2865));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2828));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2879));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2875));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 7, 40, 45, 240, DateTimeKind.Utc).AddTicks(2868));
        }
    }
}