﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class OwnerCanEditTrue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(3750),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 680, DateTimeKind.Utc).AddTicks(5612));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 180, DateTimeKind.Utc).AddTicks(4190),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 679, DateTimeKind.Utc).AddTicks(47));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "WCQmF68+EAxkPY87qPgR+LeKWD83Hr3N3R0MS93+r7g=", "+GJUE8XlL7yf/t+SeX11kg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "p2tS3sD2YdgnAdMLKGy55YXfSfElSSmiE3EF+Mc35XA=", "2Ce4d4zJCiXuyKFQwAfwZg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "bCmoEyRHq0Whv+cWVOZKRoUg9rG6Rl1+IDLSlUcnGso=", "pWErU2PoYN/dqtHw4NSWaw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "DTy475GGac8hDKx2vng5ZA1wQEURgTBR1RJKnKGmIjk=", "j5NE6kTni+spYVu3zKejdQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "CanEdit", "Password", "Salt" },
                values: new object[] { true, "cKQbPXqDsgOf1eXG71kKlHzLP7Fco5bNLj2PTGTmN30=", "CLfZTH67dDbpBTsXgU3YVg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cF23uGGBgZJJ+8i9whEejbMdshzsA3FrduZJWSSpz88=", "VBokyzVrtb5TVd5PUr48WQ==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8328));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8305));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8326));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8313));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8311));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8332));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8318));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8315));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8343));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8330));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(8322));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 680, DateTimeKind.Utc).AddTicks(5612),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 181, DateTimeKind.Utc).AddTicks(3750));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 679, DateTimeKind.Utc).AddTicks(47),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 57, 54, 180, DateTimeKind.Utc).AddTicks(4190));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "G+vz6WFCOwo5hD5W1ElnhYzRsaA1F9vAhIjAsYRMMP0=", "7SZCR5JFyNbX2kXpCcxJmA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ZpmUR+N07zU8UHfLTiJmG50ktoBeRn/MXCcG5OfMDVc=", "5s413ZlHCmVAEikFYUGwcQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "yUBoHOWOUc2ORQ5GB3lkvzSeTk3917HJEaWWMv+uDWc=", "CDVA0A8COn6MvsbJECq+mQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "OLo4f3nskdgWreuH8k/m5wjQWXoJgOFW3NoxC2qLHws=", "Jh+4igHNxaWr2ApLuBFfWA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "CanEdit", "Password", "Salt" },
                values: new object[] { false, "1RxLeu3OyRcRPz37hHjYpjWnsy+dfAK/5F+SoOFxGXk=", "4UtQ5i0agh+aZ/O93xmlZA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ymH8UkKQ6V5w0EJk2v9Gsyw28fzq/GkZrwxo0BYk/bk=", "jyGceVkOW5N8b1sWHaq2xw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3559));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3525));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3556));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3542));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3537));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3569));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3549));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3546));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3584));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3565));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3553));
        }
    }
}