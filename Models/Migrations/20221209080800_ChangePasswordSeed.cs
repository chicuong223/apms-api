﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class ChangePasswordSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(787),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(1002));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 9, 8, 7, 59, 740, DateTimeKind.Utc).AddTicks(9782),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 361, DateTimeKind.Utc).AddTicks(3790));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Nq15hQAdtfoMFXb6N9Eyfx3Cq7hzbt4W6AQdvGr+03s=", "XerxQohe8VNHaSJ4ck+mxg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Lhz5Ifa5IQhwmGH90YN+dtcwY7gGtSVOwJLeGi3SFgY=", "9fZDE8KsfgQIx1gXd/2zfg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Ux6aCpeW+tWNjDB9s8tAePmeAqLvf3agIsB7XZaRiKk=", "Cr4bfgCha8e4gadLoNBwkg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "YgbovV70s6/snA0iHVMjXkFQGaXVpynQN8jK8mfaTSg=", "1nWo8gIj8Sb07DHO2HJa+A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "TaGxwC1puA8Zrom+nmxxAZ7yLivrKH6+yMluuwsr0Ec=", "kpq0JN8LRYsqNMfxQSE3IQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "6FWqLsX9Q2dMcRUDUHK68bkUYT8Kwp3AtOq8eFCj/qw=", "xASrJsNp4ss0o+6n5WVSOQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "IG/ooJHF0I5PGP/8pAk7jYHPxpjpQ/DfoH4DpWKm5io=", "EEvinN8+Wtqv0G07b16vyA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SaC03q2fEb2rj+CBfEQJ3Oz0QcwfPneTAHVmr4tRqIE=", "dmNnKjVD7913gbHqTY9CZw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "UD6KK6Ueob46SaXaD7XIt/F3xIgkB5JPF2vxvf07ljE=", "CaiEdsMQZB5OhN1ydCuUFQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "O8SyDvAavadDJiRbAFUmm0T9qLXKOF1QgIcFyqc5/Cc=", "wDarP97HJ0w7KeF/VUq0wQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "z2MV7TzpR6MSVzVJoBJnIWMZp3r2Z/d7ExMh/evkoPI=", "Q5GiejID4cnqAQ0iJwZxZA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "gM2evEql62t5usmkl5FjcNSJebQZJds3eFNMvH+lGLA=", "W0WkbSnhSmQLOLmvN0zTjg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "PEpQMNUhlN+iLBxU35dLq/cPpS6czxtDZQo22L+D6Cs=", "TAtLinDJ1IAqtQzXK+rqBg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Jr0Y6+EdgEt6pEYgNghUBbdZHYI3AKry2eWdiB/7VjI=", "cGlXQZc/Vr229a5yQ5gckA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "KbzQp5ste6MwqNhj9UvyJHyreoxWaYEfi0Ij6+a5CRo=", "+u5OqNIibPK/1HiY93EzYw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hot2BubnU4hkBGj+LY7PTlTkozU/kKG9Qaya4I99qg0=", "0gI2FSCtrzi9z44KaILeuQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ni/L0b1oLyy0SySuwzoS0BnhcNg8V/B6XOTFAQKl2Vs=", "wIcUOsGbgOi4rNyvaHJ8Ng==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ainGDPN7DCGlrX8Um5yEHexvZAeEI5qqvaeWMtGKsxU=", "l5CDbgNSLQJ57SL7JLwRNg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "uOQyGToq9J7m/3mlWTaXD9XxwrIkrQmqRDadWILz6/A=", "B2+AsIfKQ9S6pyiBegw/Rw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "13SfDOdu8qkCLH/dPyyfu4eLhnaXcCwjKbYVosy643c=", "66iVgmzJSAg3pvEqhjM4Aw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ZjRSTE9dRb5KTChjV6asl0r/L3BKWclArvg7sB3ZATw=", "zSYHIHEpz/Mf4SmWa5L6Kw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "begSHqgb96o3tRAoxW+mmBrl2GDJwuoAFY9LvQSADYU=", "5jMzgbNH2gW1tkkmzyZFig==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "K3yar/9OSWWdmcjD/yyb1h54klNr8kBy3RzQmSd64qs=", "Q9fLJ8O39VH1/bJMcIFnXg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "yqOMGostIIlQoaR9meRRM2RWDV3Zv6FSoatbCRJRAiY=", "EZBufP8mt0MV/XkDxI8Ldg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "jh+5V3Q52tgf1L4NFZs96MTF3+P28Rtl8JdL3ZtGunM=", "I0P1dLeMIeu3G+GFeAYT8A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "j0FKzv1HmJw+5JWO71M4x2Cj6ghfL4Erpk/tOB6/k+M=", "7C3eUMExiWkVpFuxUYiEug==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Idk9RCl/pUfU5u+WHjOs5jHr26EQ9p/W5nn6ca9Ya8k=", "pCpfZRFeqYeHyNLLji9k4w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "awUAv9LDYvvLlRta1kRsOgb66mWbF7lW/2brfB3jwpU=", "eu8WOODe3jM4ezwSMvDbSA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "+DkMtlw0eH8KppevAMudf5OVFBV4BwPEIeMInfO5Zmk=", "sFv+3JQUClud8S9nF0TRcg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "XoS0X6tTusIogM6ukgY+DaSiITtBb7dHQnAWCNlcdZo=", "VKKSe9Ks+rnmwU5UmzGpbw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "3Hek+e6/E4rpjxkSzH3IkhGfm1Zi95k5Mvh3uWZjbXI=", "tsf8+kno81Eg9A9iR3Y0/g==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4937));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4927));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4936));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4931));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4930));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4934));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4932));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4940));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(4935));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(1002),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 9, 8, 7, 59, 742, DateTimeKind.Utc).AddTicks(787));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 361, DateTimeKind.Utc).AddTicks(3790),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 9, 8, 7, 59, 740, DateTimeKind.Utc).AddTicks(9782));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "B6WU6uswynL1vy6ER+9VLZ+cxQe1+8D6uSUD8jIvGGY=", "Ira80lmFpOpgJ+upMrmQSw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "QNzc2t1O19sY1gAllHAlt7ZRW5uO8Z+f5QGG73RsBsg=", "bcb9DzyKswSz7manGMuStg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "oMDBsalD/W+VyjXoHW67U/Q5ysZj5OpHKexXu5vS4J8=", "t6OXEnTmf99bkcBfzSfrlg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "buL06fi23jQ/bRG6L1MR+yZB+f57k7xXkZc1I6jq3zg=", "H9hmjzcYcPMPHv7iyJJq2w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "GzwQ3TuFEv/Ya0SKdgSJfY+UeWH99MR0qW9UEpS/soc=", "ygu7X2jJsgFX/jE4eKxc6A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hKiGoDo0sCrqJw7On5CD0LNA0EVQBicuGzAmYQ5wJLc=", "qMaV0k4YWrvdxY2oGaSJiw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "FXS/p0d3TooR6XQKKeoBQEmKfpwMAyLiIZ6JYF0yjEA=", "2jHfN7FYO/T3Iu0YUF42GQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HyNyNIa39R7iN9vIdI27nelrHFI+MUwO82LM5gZA/pU=", "TGZAKqT+Q3WfbRld5csEtQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "/jFkFrguEZVDXyTDi4ssx1HfoPnj/2a+KzdpBj5Hcd4=", "9tQyTGln26M9Ah3ZIE7/3w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "z2bHgdkMJ7gdVXlGUMyHexb+CmCVoxnlqHPaQ+CroBk=", "0Doctk1doeXDSGjqP7zrXw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "RyVD0Ln4HIm1NOOpISIy8A6UjpWvNvp+PqNFwS4FRAc=", "LhmZgfmF0IkeM3sJAWW8yA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hZhmwZ2Bwa5fs3PIzLNXXOproepZD+9HeRd3Rnqq2Sk=", "PJLoClPXr+cCqE0kCsc92Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "owST1wZiUcjOkR+JoyGBCC9iJc61EQlXiXGFa0l91SE=", "RYKt9ZMeFj5nlWO6jT2gYw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "b0K1FMEoHhNiR24trPoEg3g4KB+pQQGF2uDsVjTHRQg=", "LmRVnxCVPJC3YSmNcVPsDg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "3IaPCOa8OIFLvsdkXIRjIiy08YsRFQT+8zUqmHaUsgA=", "SxxY9Z5ooGnSilBSev7TUQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "7wQikZML1cy65xXL/VbeygxODE+5FAQh4NkSFmigtA4=", "lseoRMKbaTgvOXWYs0K/Ng==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "vQSYQsGj2BBk87pR7tokuLX08moUed/os7OSNQ0Lm/s=", "uZlJ64Boayelua26Gh3Fcg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "1d8FXE1l5W0j8TKLNX7m98MWD2trTP9eQBrq3mN1g18=", "m93sMgi3Au20GTih5X1AKA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "rslQOJp1XXQfF7wpfuzOUBy4cUXfJ7nMSVR1Tsrby8Y=", "Nbhei0PmbxaEhPc/pVcFFQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "yzLoelb1VJnu+8d6Yy1aM/wbn+bzxM/ysXB8X6x9I2I=", "s1t85o7axInp3tHh5LRd7g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "zELa47PLPL2cHQU8A+Di/aYEt4tVpXWcp6b09Q73ZeE=", "d+oMWG7M/QEQzs4zM8X+jA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cn3VWSKJlhnirvN81Aqwhi7W8E/ScRIPThoyU3dl+aA=", "BXxz8eK8pHLXQiP6ETvROw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "TxrbMXbM8/SzOv8fb1bnvNChVLlt0IMhZ83BkKh7mxk=", "ceKpynLB6KoSpri0fswt8Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cN82M44dRZKeIddOaF4LMXugvilywvQ5mpfBExU1cGE=", "O2IeKghRhoeW8j3rvVkSgg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "tG3QnaTk3jj/IB4cxeoTT6tX16D6cpzZmHkp2Spyu0c=", "V4pNuG9XrLsp6x2J050XAA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "EhHiUqPxauB7Aif/0euktNjhmSTjcapcdX2tUofSJD8=", "xUZznBvhRfPbGTBaUvLjkg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Viu6eQ8+4+srL8AqKHIrlZwVfOvfjfbAOaDjr+9wVFk=", "384AsEP3DD5RLHcXcFsdqQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "NMBneG62+0G84pT/Fuxyb9JgAa++E+Wn/W3y6HmkkpY=", "gQFnJyLQsLAor2ukkO0cPA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "UbX6gvujBLXLkzn7q+Kmh8xBrNGaVNEmv5VdI4g6i2Y=", "jhdE39obKhTC3GEZdd73Ng==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "zbTXn9IyACoaJIUf5/ToBDJi6GNTN8frFulLs2VFe4k=", "q+qSelQu7TjggxSMPEvBxQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "2+Sc4t8IZYq0Gtxg5/cY+3sT43RSerm4BkI6Jl87bWc=", "thMdjD5j9OxMQXrejp0yog==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5996));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5979));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5994));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5986));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5999));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5990));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5988));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(6005));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5997));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5992));
        }
    }
}