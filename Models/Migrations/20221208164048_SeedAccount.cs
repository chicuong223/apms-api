﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class SeedAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(1002),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(693));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 361, DateTimeKind.Utc).AddTicks(3790),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 740, DateTimeKind.Utc).AddTicks(7599));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "B6WU6uswynL1vy6ER+9VLZ+cxQe1+8D6uSUD8jIvGGY=", "Ira80lmFpOpgJ+upMrmQSw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "QNzc2t1O19sY1gAllHAlt7ZRW5uO8Z+f5QGG73RsBsg=", "bcb9DzyKswSz7manGMuStg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "oMDBsalD/W+VyjXoHW67U/Q5ysZj5OpHKexXu5vS4J8=", "t6OXEnTmf99bkcBfzSfrlg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "buL06fi23jQ/bRG6L1MR+yZB+f57k7xXkZc1I6jq3zg=", "H9hmjzcYcPMPHv7iyJJq2w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "GzwQ3TuFEv/Ya0SKdgSJfY+UeWH99MR0qW9UEpS/soc=", "ygu7X2jJsgFX/jE4eKxc6A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hKiGoDo0sCrqJw7On5CD0LNA0EVQBicuGzAmYQ5wJLc=", "qMaV0k4YWrvdxY2oGaSJiw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "FXS/p0d3TooR6XQKKeoBQEmKfpwMAyLiIZ6JYF0yjEA=", "2jHfN7FYO/T3Iu0YUF42GQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HyNyNIa39R7iN9vIdI27nelrHFI+MUwO82LM5gZA/pU=", "TGZAKqT+Q3WfbRld5csEtQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "/jFkFrguEZVDXyTDi4ssx1HfoPnj/2a+KzdpBj5Hcd4=", "9tQyTGln26M9Ah3ZIE7/3w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "z2bHgdkMJ7gdVXlGUMyHexb+CmCVoxnlqHPaQ+CroBk=", "0Doctk1doeXDSGjqP7zrXw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "RyVD0Ln4HIm1NOOpISIy8A6UjpWvNvp+PqNFwS4FRAc=", "LhmZgfmF0IkeM3sJAWW8yA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hZhmwZ2Bwa5fs3PIzLNXXOproepZD+9HeRd3Rnqq2Sk=", "PJLoClPXr+cCqE0kCsc92Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "cn3VWSKJlhnirvN81Aqwhi7W8E/ScRIPThoyU3dl+aA=", "BXxz8eK8pHLXQiP6ETvROw==" });

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Active", "CanEdit", "CanEditCarPark", "CarParkId", "Password", "PhoneNumber", "RoleId", "Salt" },
                values: new object[,]
                {
                    { "OWN000002", true, true, true, new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), "owST1wZiUcjOkR+JoyGBCC9iJc61EQlXiXGFa0l91SE=", "0727589658", 2, "RYKt9ZMeFj5nlWO6jT2gYw==" },
                    { "OWN000003", true, true, true, new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), "b0K1FMEoHhNiR24trPoEg3g4KB+pQQGF2uDsVjTHRQg=", "0902555662", 2, "LmRVnxCVPJC3YSmNcVPsDg==" },
                    { "OWN000004", true, true, true, new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), "3IaPCOa8OIFLvsdkXIRjIiy08YsRFQT+8zUqmHaUsgA=", "0125666879", 2, "SxxY9Z5ooGnSilBSev7TUQ==" },
                    { "OWN000005", true, true, true, new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), "7wQikZML1cy65xXL/VbeygxODE+5FAQh4NkSFmigtA4=", "0903650750", 2, "lseoRMKbaTgvOXWYs0K/Ng==" },
                    { "OWN000006", true, true, true, new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), "vQSYQsGj2BBk87pR7tokuLX08moUed/os7OSNQ0Lm/s=", "0905125685", 2, "uZlJ64Boayelua26Gh3Fcg==" },
                    { "OWN000007", true, true, true, new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), "1d8FXE1l5W0j8TKLNX7m98MWD2trTP9eQBrq3mN1g18=", "0909159259", 2, "m93sMgi3Au20GTih5X1AKA==" },
                    { "OWN000008", true, true, true, new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), "rslQOJp1XXQfF7wpfuzOUBy4cUXfJ7nMSVR1Tsrby8Y=", "0372658456", 2, "Nbhei0PmbxaEhPc/pVcFFQ==" },
                    { "OWN000009", true, true, true, new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), "yzLoelb1VJnu+8d6Yy1aM/wbn+bzxM/ysXB8X6x9I2I=", "0937546587", 2, "s1t85o7axInp3tHh5LRd7g==" },
                    { "OWN000010", true, true, true, new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), "zELa47PLPL2cHQU8A+Di/aYEt4tVpXWcp6b09Q73ZeE=", "0902357159", 2, "d+oMWG7M/QEQzs4zM8X+jA==" },
                    { "STF000002", true, false, false, new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), "TxrbMXbM8/SzOv8fb1bnvNChVLlt0IMhZ83BkKh7mxk=", "0909578951", 3, "ceKpynLB6KoSpri0fswt8Q==" },
                    { "STF000003", true, false, false, new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), "cN82M44dRZKeIddOaF4LMXugvilywvQ5mpfBExU1cGE=", "0902348750", 3, "O2IeKghRhoeW8j3rvVkSgg==" },
                    { "STF000004", true, false, false, new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), "tG3QnaTk3jj/IB4cxeoTT6tX16D6cpzZmHkp2Spyu0c=", "0902595590", 3, "V4pNuG9XrLsp6x2J050XAA==" },
                    { "STF000005", true, false, false, new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), "EhHiUqPxauB7Aif/0euktNjhmSTjcapcdX2tUofSJD8=", "0906546873", 3, "xUZznBvhRfPbGTBaUvLjkg==" },
                    { "STF000006", true, false, false, new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), "Viu6eQ8+4+srL8AqKHIrlZwVfOvfjfbAOaDjr+9wVFk=", "0967678123", 3, "384AsEP3DD5RLHcXcFsdqQ==" },
                    { "STF000007", true, false, false, new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), "NMBneG62+0G84pT/Fuxyb9JgAa++E+Wn/W3y6HmkkpY=", "0965666235", 3, "gQFnJyLQsLAor2ukkO0cPA==" },
                    { "STF000008", true, false, false, new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), "UbX6gvujBLXLkzn7q+Kmh8xBrNGaVNEmv5VdI4g6i2Y=", "0677123951", 3, "jhdE39obKhTC3GEZdd73Ng==" },
                    { "STF000009", true, false, false, new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), "zbTXn9IyACoaJIUf5/ToBDJi6GNTN8frFulLs2VFe4k=", "0909654875", 3, "q+qSelQu7TjggxSMPEvBxQ==" },
                    { "STF000010", true, false, false, new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), "2+Sc4t8IZYq0Gtxg5/cY+3sT43RSerm4BkI6Jl87bWc=", "0222678678", 3, "thMdjD5j9OxMQXrejp0yog==" }
                });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "Status",
                value: -1);

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5996));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5979));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5994));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5986));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5999));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5990));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5988));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(6005));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5997));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(5992));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000002");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000003");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000004");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000005");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000006");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000007");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000008");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000009");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000010");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000002");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000003");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000004");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000005");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000006");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000007");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000008");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000009");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000010");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(693),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 363, DateTimeKind.Utc).AddTicks(1002));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 740, DateTimeKind.Utc).AddTicks(7599),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 8, 16, 40, 48, 361, DateTimeKind.Utc).AddTicks(3790));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "KWzF5o5Hndm+q8giuw1fZ3VhJ0D88URAJSeZeKyIXso=", "/2ojDwR7XyqxjtMZ9axxRg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "0Vf3w+dL39uoYP2RxzXuB2KHYpxxDiASrO2Z3EQmUt0=", "EepS33MnfjQl3nNLq1CoQQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "WZoHgL98gJxeS2rNzSEnlx6tKYusCJtm20wJFOrJ/y0=", "tZZirM7HCOnaLfzOflkyaw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "kguGijueqdPG70WwO52phuyzzQntp+Uv12pH68pGuJM=", "VAzP94UqaguGKJ0EeQri+g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000004",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "k/IZu+SGUaxtM5Pon4FLbUeCfKZ1wasLsP2qUf2dWow=", "aNBke1YZHQqaRHk12lEs2w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000005",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ASmYwhVeNkmd/2+rS3c2vxpVm0Up5gmyP2bp7/iNLys=", "I9LZVti1OMe21l7DNU+WOg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000006",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "kK8tAlq0YKp1HxVRckYI1i9yV11kdAKESnTfXFqD37g=", "9GYIC8KJmT+DdJehP9k+iA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000007",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SH4EUHzijBljvi5F6LBXZncfBz2xt/Ovuq7prO9jna0=", "3k+pcZkkorrvLKkURLbS8w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000008",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "32rueh4mertcFrjlJHtcl5lGUS4h0F2Kkgz3lKzAJZ4=", "t3zvioD/vAgP/MSx3O9MMw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000009",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "y+cBgfmuedh/Uce7pjPu8qpsKxhWeTxF4YcrNruhzw4=", "HMprViI9ITD2tivS0dVA0g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000010",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "b/mliUfLcjyF1j9vOOMDTd216WnO7Lf+ESachToS5CA=", "LLhyykuB/pGv/BzI7w6HPw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "XICFEJk/lAA4DCBOd8mNwnuoFdGhFZ18BjgH8XTpZYA=", "eIinRehhIcU+auNXJ9f83A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "3zJlvKBv6MleCtvkOKBe7159BLQN24wq+QSJ4yWR6mY=", "71PFD1tr154rm6uXiPcmXg==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "Status",
                value: 0);

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5642));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5512));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5640));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5632));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5626));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5647));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5636));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5634));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5654));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5645));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5638));
        }
    }
}