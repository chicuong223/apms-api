﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class SeedMoreCustomers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(693),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 667, DateTimeKind.Utc).AddTicks(6233));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 740, DateTimeKind.Utc).AddTicks(7599),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 666, DateTimeKind.Utc).AddTicks(1921));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "KWzF5o5Hndm+q8giuw1fZ3VhJ0D88URAJSeZeKyIXso=", "0903356123", "/2ojDwR7XyqxjtMZ9axxRg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "0Vf3w+dL39uoYP2RxzXuB2KHYpxxDiASrO2Z3EQmUt0=", "EepS33MnfjQl3nNLq1CoQQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "WZoHgL98gJxeS2rNzSEnlx6tKYusCJtm20wJFOrJ/y0=", "tZZirM7HCOnaLfzOflkyaw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "kguGijueqdPG70WwO52phuyzzQntp+Uv12pH68pGuJM=", "VAzP94UqaguGKJ0EeQri+g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "XICFEJk/lAA4DCBOd8mNwnuoFdGhFZ18BjgH8XTpZYA=", "eIinRehhIcU+auNXJ9f83A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "3zJlvKBv6MleCtvkOKBe7159BLQN24wq+QSJ4yWR6mY=", "71PFD1tr154rm6uXiPcmXg==" });

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Active", "CanEdit", "CanEditCarPark", "CarParkId", "Password", "PhoneNumber", "RoleId", "Salt" },
                values: new object[,]
                {
                    { "CUS000004", true, false, false, null, "k/IZu+SGUaxtM5Pon4FLbUeCfKZ1wasLsP2qUf2dWow=", "0937632595", 4, "aNBke1YZHQqaRHk12lEs2w==" },
                    { "CUS000005", true, false, false, null, "ASmYwhVeNkmd/2+rS3c2vxpVm0Up5gmyP2bp7/iNLys=", "0909254787", 4, "I9LZVti1OMe21l7DNU+WOg==" },
                    { "CUS000006", true, false, false, null, "kK8tAlq0YKp1HxVRckYI1i9yV11kdAKESnTfXFqD37g=", "0902345590", 4, "9GYIC8KJmT+DdJehP9k+iA==" },
                    { "CUS000007", true, false, false, null, "SH4EUHzijBljvi5F6LBXZncfBz2xt/Ovuq7prO9jna0=", "0123456168", 4, "3k+pcZkkorrvLKkURLbS8w==" },
                    { "CUS000008", true, false, false, null, "32rueh4mertcFrjlJHtcl5lGUS4h0F2Kkgz3lKzAJZ4=", "0772776677", 4, "t3zvioD/vAgP/MSx3O9MMw==" },
                    { "CUS000009", true, false, false, null, "y+cBgfmuedh/Uce7pjPu8qpsKxhWeTxF4YcrNruhzw4=", "0932852653", 4, "HMprViI9ITD2tivS0dVA0g==" },
                    { "CUS000010", true, false, false, null, "b/mliUfLcjyF1j9vOOMDTd216WnO7Lf+ESachToS5CA=", "0941976105", 4, "LLhyykuB/pGv/BzI7w6HPw==" }
                });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5642));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5512));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5640));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5632));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5626));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5647));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5636));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5634));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5654));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5645));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(5638));

            migrationBuilder.InsertData(
                table: "UserInfo",
                columns: new[] { "Id", "AccountBalance", "FullName" },
                values: new object[,]
                {
                    { "CUS000004", 500000m, "Huỳnh Mẫn Huy" },
                    { "CUS000005", 500000m, "Châu Vĩnh Cơ" },
                    { "CUS000006", 500000m, "Triệu Văn Vĩnh" },
                    { "CUS000007", 500000m, "Lê Xuân Mai" },
                    { "CUS000008", 500000m, "Nguyễn Thị Ánh Nguyệt" },
                    { "CUS000009", 500000m, "Hồ Võ Kim Ngân" },
                    { "CUS000010", 500000m, "Lương Vệ Thành" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000004");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000005");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000006");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000007");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000008");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000009");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000010");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000004");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000005");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000006");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000007");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000008");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000009");

            migrationBuilder.DeleteData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000010");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 667, DateTimeKind.Utc).AddTicks(6233),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 742, DateTimeKind.Utc).AddTicks(693));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 4, 6, 21, 19, 666, DateTimeKind.Utc).AddTicks(1921),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 7, 18, 38, 9, 740, DateTimeKind.Utc).AddTicks(7599));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "kX9KT1pmuUCTRTXBHuPUHoXQEtHv+bfPAsB09kP+0s8=", "1111111111", "XD/KwW4aaoZhuBGGoL3M+g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "PnlHZ9Acf9Z3ZWi9ofnYp82hEwrneLhGpFdy9hsQy/o=", "HNxR3sJz7b//hy3sffk+mA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "S4o2sHQZuwx2WWqJ5KGPyEalyOPi7wzs5AQ4IK7SBNs=", "vwu0RnCZWSorS5HbsHnnbA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BjULwC+Utbg/WZKSOPcsXjdQK+3ZqDtiszwKwCzVmMY=", "V0ge0CIGmzSOopESh0817w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Yw8VUwNDA1818Pwp9mW08zWGRPH8NjOgzm8mHWDc3DQ=", "ZC3JYNCGxBtFYizj5iYqYQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "weCd8sPN8oKUWHB5tzQYIjNySchny78ENWjxIiTLJus=", "YuOdx7DEe0U04ZsDMa1SgQ==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1339));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1316));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1337));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1328));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1324));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1344));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1332));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1330));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1352));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1342));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 4, 6, 21, 19, 668, DateTimeKind.Utc).AddTicks(1333));
        }
    }
}