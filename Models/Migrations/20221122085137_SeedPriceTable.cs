﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class SeedPriceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(4177),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 278, DateTimeKind.Utc).AddTicks(1775));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 312, DateTimeKind.Utc).AddTicks(1386),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 277, DateTimeKind.Utc).AddTicks(359));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BEUAswCNSzjK8T6s1OC0BRg7EPhJUlwNWAPNVw5NYNk=", "YAYQjstXW1DqN6BIIfNIpQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "a7v7wJeMZ1tvsm+lz0PpnTVjSvv75Ygl+rNaeksxvE4=", "EYy63ssrnZk1beBNMNcxZQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "FQL1Q5HXweolIRecrtHn1ZxgYAZhnXizgaV+UFaE4aU=", "W5LH+LLMthsaS+PFC3Nk9w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "nhOUNaVK9+nXwDA4MEY5OSnPm24luYY6CO+merFfuOE=", "VmTialuV3ayfya1bXQEnVA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Tu2c5TL4rU66T+1KnxiOP5G5LKJ/HA3yCt7NrkLXf+o=", "45JGah7EO/o6DxfGNfYIeg==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9853));

            migrationBuilder.InsertData(
                table: "PriceTable",
                columns: new[] { "CarParkId", "LastModified", "ReservationFeePercentage" },
                values: new object[,]
                {
                    { new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9873), 30m },
                    { new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9871), 50m },
                    { new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9861), 30m },
                    { new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9859), 50m },
                    { new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9878), 20m },
                    { new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9866), 10m },
                    { new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9864), 20m },
                    { new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9875), 10m },
                    { new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(9868), 5m }
                });

            migrationBuilder.InsertData(
                table: "PriceTableDetail",
                columns: new[] { "Id", "CarParkId", "Fee", "FromTime", "ToTime" },
                values: new object[,]
                {
                    { 3, new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), 35000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 4, new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), 60000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 5, new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), 25000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 6, new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), 35000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 7, new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), 30000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 8, new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), 50000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 23, 59, 0, 0) },
                    { 9, new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), 60000m, new TimeSpan(0, 0, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 10, new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), 30000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 11, new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), 35000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 12, new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), 12000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 13, new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), 20000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 14, new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), 20000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 15, new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), 40000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 16, new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), 30000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 17, new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), 35000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 18, new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), 50000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 17, 59, 0, 0) },
                    { 19, new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), 60000m, new TimeSpan(0, 18, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) },
                    { 20, new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), 30000m, new TimeSpan(0, 6, 0, 0, 0), new TimeSpan(0, 5, 59, 0, 0) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PriceTableDetail",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"));

            migrationBuilder.DeleteData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 278, DateTimeKind.Utc).AddTicks(1775),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 313, DateTimeKind.Utc).AddTicks(4177));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 35, 17, 277, DateTimeKind.Utc).AddTicks(359),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 8, 51, 37, 312, DateTimeKind.Utc).AddTicks(1386));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "20H4JKC+8IWrC/Mn82lOXyWMp4HvVT2Ie3f/6szALkA=", "L78pGKKnTgCyMUQTRLsZ1w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "CdesSkiakzeAmSTWFvr0kKuiMizlutZtK8TD8x6vwpI=", "Q4egekWWfFiIR1vzwk6mpg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "frJzaNXd3eRR+bgt+yqxMDOLdzwX4M6GbsKJSRZEQ8k=", "iII2Yf44ezF6Z1U3J0oN2g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "NNQchqhpFo4T9ztI5rXZK8vYOJsdp4Q9Sr/3IngnDEo=", "LhFM/xY7Er29iq9h7flz2g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "hZGt+efvdwh0TWtdd+fVyLJTiPsUo5XctTNcMbRFrO8=", "rwwmOrCr4zz5smDWUkUOjA==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 7, 35, 17, 278, DateTimeKind.Utc).AddTicks(8511));
        }
    }
}