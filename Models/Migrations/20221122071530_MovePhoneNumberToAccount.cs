﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class MovePhoneNumberToAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "UserInfo");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 781, DateTimeKind.Utc).AddTicks(3387),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 988, DateTimeKind.Utc).AddTicks(4029));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 780, DateTimeKind.Utc).AddTicks(3070),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 986, DateTimeKind.Utc).AddTicks(9099));

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Account",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "B1e+014v4WuaErBfNXTS7Tp616ONwbKzj6l58jtPeZE=", "1111111111", "77tT4tnCTOPwB3EWytxZqw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "HzF/aEgvIz9dZME7U73od4cq4jsP+s8TY40V4gPCSRs=", "0932781745", "RTDed+w/eBa9GHnxLOC6pw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "u8cYPkh6VMlSBm3NJipq26kwmecbtB61Cl6gTWFAcok=", "0949936732", "814ZVUBgV/6DkkgZ++9X/Q==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "1nmzc9KSEYsBlG0rB+4IOEn/qKyDIWDBhNZrOdLoTyE=", "0908436393", "+aewFPoZegQSqxRAT8mi/g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "PhoneNumber", "Salt" },
                values: new object[] { "EPrEqS88waESUU6FuUT6oFwjzI3EpxV16dB3s0yHL9I=", "0377162315", "65isiWk+iAfhyFF2jLJuzw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 22, 7, 15, 29, 781, DateTimeKind.Utc).AddTicks(7956));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Account");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "UserInfo",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 988, DateTimeKind.Utc).AddTicks(4029),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 781, DateTimeKind.Utc).AddTicks(3387));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 986, DateTimeKind.Utc).AddTicks(9099),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 22, 7, 15, 29, 780, DateTimeKind.Utc).AddTicks(3070));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "wmQdYU+PnK5OF3MDWLU+zeN43lx7wOtoCF2ThrcOEdU=", "4V2AcpIntBVMXTDt/tqO8g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "6q+WHEeu7GmLuRY51yr0UhxhCu8LUgUtsvaWWT4Omvw=", "oHIv5ZUzHsluh45OlBKBIw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HWqga3r6zGbYcvyr4h9NaVwd9uo516qqRsxB7Wnky+Q=", "ap5kVEqA1MEM4srDgsie8w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "TuNlgp353XSsOzPAnP/vFS+uN2WSWjlC09n2w5rH884=", "kmCt3uN6xzheSEfZd1nCAQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "2dyvj8rMtriSJzVVrtohl4Fg7l4i1h2XCyW1DsvCdf0=", "mzA/htwgoFCJjweuN2t+9A==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 21, 12, 6, 44, 989, DateTimeKind.Utc).AddTicks(44));

            migrationBuilder.UpdateData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000001",
                column: "PhoneNumber",
                value: "0932781745");

            migrationBuilder.UpdateData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000002",
                column: "PhoneNumber",
                value: "0949936732");
        }
    }
}