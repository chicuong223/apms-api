﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class AddMaxCapacityInCarPark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Account_PhoneNumber",
                table: "Account");

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 680, DateTimeKind.Utc).AddTicks(5612),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 891, DateTimeKind.Utc).AddTicks(3748));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 679, DateTimeKind.Utc).AddTicks(47),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 889, DateTimeKind.Utc).AddTicks(6975));

            migrationBuilder.AddColumn<int>(
                name: "MaxCapacity",
                table: "CarPark",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "G+vz6WFCOwo5hD5W1ElnhYzRsaA1F9vAhIjAsYRMMP0=", "7SZCR5JFyNbX2kXpCcxJmA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ZpmUR+N07zU8UHfLTiJmG50ktoBeRn/MXCcG5OfMDVc=", "5s413ZlHCmVAEikFYUGwcQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "yUBoHOWOUc2ORQ5GB3lkvzSeTk3917HJEaWWMv+uDWc=", "CDVA0A8COn6MvsbJECq+mQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "OLo4f3nskdgWreuH8k/m5wjQWXoJgOFW3NoxC2qLHws=", "Jh+4igHNxaWr2ApLuBFfWA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "1RxLeu3OyRcRPz37hHjYpjWnsy+dfAK/5F+SoOFxGXk=", "4UtQ5i0agh+aZ/O93xmlZA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ymH8UkKQ6V5w0EJk2v9Gsyw28fzq/GkZrwxo0BYk/bk=", "jyGceVkOW5N8b1sWHaq2xw==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 1200, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                columns: new[] { "AvailableSlotsCount", "MaxCapacity", "ProvinceId" },
                values: new object[] { 3000, 3000, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                columns: new[] { "Latitude", "MaxCapacity", "ProvinceId" },
                values: new object[] { 10.79702, 2000, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 200, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 200, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 600, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                columns: new[] { "MaxCapacity", "PhoneNumber", "ProvinceId" },
                values: new object[] { 600, "02837791018", 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 600, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 3000, 79 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                columns: new[] { "MaxCapacity", "ProvinceId" },
                values: new object[] { 600, 79 });

            migrationBuilder.InsertData(
                table: "CarPark",
                columns: new[] { "Id", "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "MaxCapacity", "Name", "PhoneNumber", "ProvinceId", "Status", "Street", "Ward" },
                values: new object[] { new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"), "333", 3000, "Bình Dương", "Thuận An", 10.94816, 106.70626, 3000, "City Tower Bình Dương", "0915592179", 74, 0, "Đại lộ Bình Dương", "" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3559));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3525));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3556));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3542));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3537));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3569));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3549));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3546));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3584));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3565));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 24, 16, 34, 12, 681, DateTimeKind.Utc).AddTicks(3553));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"));

            migrationBuilder.DropColumn(
                name: "MaxCapacity",
                table: "CarPark");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 891, DateTimeKind.Utc).AddTicks(3748),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 680, DateTimeKind.Utc).AddTicks(5612));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 23, 8, 59, 54, 889, DateTimeKind.Utc).AddTicks(6975),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 24, 16, 34, 12, 679, DateTimeKind.Utc).AddTicks(47));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SxjZeBulI/ZpsArmzRuBrP1MPFe8R0sUBhrCzLUNht0=", "f9hgz9MzNdhe1IPUzqI3iQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "LCwBL4YYkcLLRlBot1su2RpT1cN9vZQPnwyTkGeTdz4=", "c+LVdi5xDKtDbb0ekvjVqg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "nf2ZlHQJl2SGJZZIM32Y446l4mce2Dv+u6sRGC3+Jc0=", "5EEQX5tUF3WZWrpFK0mFGg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Fwnx7Tl+IFMwJ7uXDCMUzaWVyq36h+YJW3ZSm5tHtG8=", "8mNAlOqFL71TW7YfPmaQwg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Dzcn6yfmtmb2+2HVOFN+X0c2RHM5yi+YYeAcdjkTLcY=", "4H2W+v9C+Hrhej7PgALYUw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "B87COHQXi3ylhofuSIHzdcGtm3vZ7dZoq/CrEiSzz4s=", "1/KIjDnwiY1YxF2QNyKmrA==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                columns: new[] { "AvailableSlotsCount", "ProvinceId" },
                values: new object[] { 200, 76 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                columns: new[] { "Latitude", "ProvinceId" },
                values: new object[] { 10.76702, 76 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                columns: new[] { "PhoneNumber", "ProvinceId" },
                values: new object[] { "02837761018", 76 });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "ProvinceId",
                value: 76);

            migrationBuilder.InsertData(
                table: "CarPark",
                columns: new[] { "Id", "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "Name", "PhoneNumber", "ProvinceId", "Status", "Street", "Ward" },
                values: new object[] { new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"), "333", 3000, "Bình Dương", "Thuận An", 10.94816, 106.70626, "City Tower Bình Dương", "0915592179", 76, 0, "Đại lộ Bình Dương", "" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(750));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(718));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(746));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(733));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(730));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(757));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(740));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(737));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(770));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(753));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 23, 8, 59, 54, 892, DateTimeKind.Utc).AddTicks(744));

            migrationBuilder.CreateIndex(
                name: "IX_Account_PhoneNumber",
                table: "Account",
                column: "PhoneNumber",
                unique: true);
        }
    }
}