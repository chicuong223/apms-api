﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class CorrectUserInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "cus000001");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "cus000002");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 559, DateTimeKind.Utc).AddTicks(7163),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 186, DateTimeKind.Utc).AddTicks(3220));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 558, DateTimeKind.Utc).AddTicks(4965),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 184, DateTimeKind.Utc).AddTicks(9244));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BCEKEjSWuOrMI4PySM2yC0KfZY3AQl9Pp7PZFsuWR8Y=", "+FFsFL6Gml5D3Eg+KFKX6g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "W0Zvm1gm3X3DjEnzAffwi8z7o7ItrL4X5k8GBNzi29c=", "Ngij/zexPuUy7W78NBhqGw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "bxzDr7urZ8CXq90wjOcclQGAqE6LN+xsbaJ4dVj1T+U=", "0eKK35+da6J7WAIAWYL0BQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "wg98cJ+MNK6U1hn+WHGxe1H9/54kKVSTiuF9/lubLOY=", "ted6x+WZjZx3TEp4xBkE8w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "rnWe//iLOcvybmUlCU4Y1SXn3OUma/6nF+6WwUeI46g=", "/EkUmwE6P8OUblrZoQwrqQ==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 21, 5, 44, 51, 560, DateTimeKind.Utc).AddTicks(2178));

            migrationBuilder.InsertData(
                table: "UserInfo",
                columns: new[] { "Id", "AccountBalance", "FullName", "PhoneNumber" },
                values: new object[,]
                {
                    { "CUS000001", 1000000m, "Ho Huu Phat", "0932781745" },
                    { "CUS000002", 500000m, "Khuc Ngoc Thai", "0949936732" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000001");

            migrationBuilder.DeleteData(
                table: "UserInfo",
                keyColumn: "Id",
                keyValue: "CUS000002");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 186, DateTimeKind.Utc).AddTicks(3220),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 559, DateTimeKind.Utc).AddTicks(7163));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 17, 14, 54, 34, 184, DateTimeKind.Utc).AddTicks(9244),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 558, DateTimeKind.Utc).AddTicks(4965));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "ojth72ivG+TLbNKCZ9oniNx2FqSuayD2QK8UqnpwE9c=", "ronnc+Fa5m72iSmp+/HmSQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "1sWqkprBe6WoA/stoMi+0usQEW9r8MVI/GSC/4M3rLE=", "WUooaxu2VIt/sUx4qVlDNw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "M8dLhaTfWk9Mg1ePX38PBbrFTUxCXp7GC7OlOaSyNaI=", "ll7oEz6JXxxIQZAHYqnDfA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "GUTcLZRvchtUK5b0Z+TGzN+d5n8aKog5czQsBVTxpSc=", "5lbgZum79IP/mRo0fuHe9g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "d4D3TJqqQ98jFTVe5LPiT5BNYeb56PXtHLQQShHbFAE=", "xYlpE26oodQbKtkvXJ1V4Q==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 17, 14, 54, 34, 186, DateTimeKind.Utc).AddTicks(8440));

            migrationBuilder.InsertData(
                table: "UserInfo",
                columns: new[] { "Id", "AccountBalance", "FullName", "PhoneNumber" },
                values: new object[,]
                {
                    { "cus000001", 1000000m, "Ho Huu Phat", "0932781745" },
                    { "cus000002", 500000m, "Khuc Ngoc Thai", "0949936732" }
                });
        }
    }
}