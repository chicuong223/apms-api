﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class FixSeedCarPark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 988, DateTimeKind.Utc).AddTicks(4029),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 501, DateTimeKind.Utc).AddTicks(1788));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 986, DateTimeKind.Utc).AddTicks(9099),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 499, DateTimeKind.Utc).AddTicks(3153));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "wmQdYU+PnK5OF3MDWLU+zeN43lx7wOtoCF2ThrcOEdU=", "4V2AcpIntBVMXTDt/tqO8g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "6q+WHEeu7GmLuRY51yr0UhxhCu8LUgUtsvaWWT4Omvw=", "oHIv5ZUzHsluh45OlBKBIw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "HWqga3r6zGbYcvyr4h9NaVwd9uo516qqRsxB7Wnky+Q=", "ap5kVEqA1MEM4srDgsie8w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "TuNlgp353XSsOzPAnP/vFS+uN2WSWjlC09n2w5rH884=", "kmCt3uN6xzheSEfZd1nCAQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "2dyvj8rMtriSJzVVrtohl4Fg7l4i1h2XCyW1DsvCdf0=", "mzA/htwgoFCJjweuN2t+9A==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                columns: new[] { "AddressNumber", "District", "Ward" },
                values: new object[] { "Lô E2a-7", "Thành phố Thủ Đức", "Long Thạnh Mỹ" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 21, 12, 6, 44, 989, DateTimeKind.Utc).AddTicks(44));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 501, DateTimeKind.Utc).AddTicks(1788),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 988, DateTimeKind.Utc).AddTicks(4029));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 499, DateTimeKind.Utc).AddTicks(3153),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 6, 44, 986, DateTimeKind.Utc).AddTicks(9099));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "NtEWYOy2f+01OKDR3PsRs3+YXuhWKRP7Vzk6jrkxlEA=", "IaqJSdKKYql25n2T33Vy6g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "RQtZVMCGRvRebPYEMMmrLm1ludry2Lq+tO5ovKwPS7s=", "PmsteOpU/J/uKr5Thsg0Yg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SGV189wJhUKaYZ4vis54G5CUqlkT9Q1GqzMq4fm+sOA=", "d1svShyVjHUmPxp35tU5tg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "K+2Du0EuRvMAvSR7os916e+2P4idCHnYx/eZ+LCpz10=", "zWEB5SWBDSonVWVsqWNduA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Pzj+yRDMW6hBcKVgdfcEqtwcSa+CO0dfvOD3ktMRyF8=", "e4vtoJo3fGaBZhbOCeBi4A==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                columns: new[] { "AddressNumber", "District", "Ward" },
                values: new object[] { "215", "Lô E2a-7", "Thành phố Thủ Đức" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 21, 12, 4, 32, 501, DateTimeKind.Utc).AddTicks(9437));
        }
    }
}