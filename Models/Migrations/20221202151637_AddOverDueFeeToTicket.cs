﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class AddOverDueFeeToTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(1660),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 30, 8, 45, 24, 762, DateTimeKind.Utc).AddTicks(4994));

            migrationBuilder.AddColumn<decimal>(
                name: "OverdueFee",
                table: "Ticket",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 842, DateTimeKind.Utc).AddTicks(6409),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 30, 8, 45, 24, 761, DateTimeKind.Utc).AddTicks(200));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "EN2PK0edBhrEn1Pbs3mLzc6SExroEJVbqz6rlPUjb6Q=", "kdECMzBgg6l8aUFfDVQs+A==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "mybV23nEc2OGnKy0MEnfjuzLx0Xe/QwmNDCVqzy+KZA=", "0faA6Y/e9u45o34BxjG2oA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "G99lDsKdO/2g53/0XKiEgwQKf4bNw7YmKDUv4PS1NQw=", "wCU6MZ9cUZCm5AgIhiVEmQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "uFKlNjSi+KdinGLu9NcC0hTSTETKCzqPXq9mWSz09es=", "tLBo2buMF9l2uurpGNAecw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "VNS8W3xmPUD53os5NmWCZdF4FqW4NlGTNWPnlFRGf2M=", "LAHqSW2YzjoQhRlZpOv0JA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "LYKJP+bAtf51bB3nZbTUb/G0LNdz1LxaszlkTZBw47w=", "hRhGeF6SISme8iz3V0bkpw==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8138));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8115));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8135));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8126));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8123));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8142));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8131));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8129));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8149));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8140));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(8133));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OverdueFee",
                table: "Ticket");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 30, 8, 45, 24, 762, DateTimeKind.Utc).AddTicks(4994),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 844, DateTimeKind.Utc).AddTicks(1660));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 30, 8, 45, 24, 761, DateTimeKind.Utc).AddTicks(200),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 12, 2, 15, 16, 36, 842, DateTimeKind.Utc).AddTicks(6409));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Ru6A6HrUEpEygrZjfMo5DKtDP8/Ug1U1WMXWkAOlqL8=", "azd6l5G5qc+4DjDKgdVPkA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "aSucR4UvNtX1p6TV9ckXDIEEzQuTGBDCsmeGh4Vwe+k=", "7czxzgFyR5cEIwfomlUTsw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "v0/yL4xpxr2fx+uKweVnt00UqQp/IHJwaFT9U+fToqY=", "/e5uGaX5AdUkgPk8cFopSw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000003",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "RulirSCeqRTAueIgN3dV46960OcdDNVFbzKdCuP79rw=", "xrCcyYomaqg6Gg+zwdlOLw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SaCLsvJH6/YSxHUJVDxvTEHvV1yBM+0c5LBAmBkqUA8=", "tXUAZu0QsjxTFshoPLbHEQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Q9tIo2Tv4gtLpl2XMbYVgeEL+mTx6fvx/MA/z8M6siw=", "2vWh8NlB5aN/mpNqjkrquA==" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2296));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2273));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2293));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2286));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2284));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2299));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2290));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2288));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2307));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2297));

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                column: "LastModified",
                value: new DateTime(2022, 11, 30, 8, 45, 24, 763, DateTimeKind.Utc).AddTicks(2292));
        }
    }
}