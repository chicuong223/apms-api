﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Models.Migrations
{
    public partial class SeedCarPark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 501, DateTimeKind.Utc).AddTicks(1788),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 559, DateTimeKind.Utc).AddTicks(7163));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 499, DateTimeKind.Utc).AddTicks(3153),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 558, DateTimeKind.Utc).AddTicks(4965));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "NtEWYOy2f+01OKDR3PsRs3+YXuhWKRP7Vzk6jrkxlEA=", "IaqJSdKKYql25n2T33Vy6g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "RQtZVMCGRvRebPYEMMmrLm1ludry2Lq+tO5ovKwPS7s=", "PmsteOpU/J/uKr5Thsg0Yg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "SGV189wJhUKaYZ4vis54G5CUqlkT9Q1GqzMq4fm+sOA=", "d1svShyVjHUmPxp35tU5tg==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "K+2Du0EuRvMAvSR7os916e+2P4idCHnYx/eZ+LCpz10=", "zWEB5SWBDSonVWVsqWNduA==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Pzj+yRDMW6hBcKVgdfcEqtwcSa+CO0dfvOD3ktMRyF8=", "e4vtoJo3fGaBZhbOCeBi4A==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                columns: new[] { "AddressNumber", "AvailableSlotsCount", "District", "Latitude", "Longitude", "Name", "PhoneNumber", "ProvinceId", "Street", "Ward" },
                values: new object[] { "45A", 200, "Quận 1", 10.778460000000001, 106.70187, "Vincom Đồng Khởi", "0975033288", 76, "Lý Tự Trọng", "Bến Nghé" });

            migrationBuilder.InsertData(
                table: "CarPark",
                columns: new[] { "Id", "AddressNumber", "AvailableSlotsCount", "City", "District", "Latitude", "Longitude", "Name", "PhoneNumber", "ProvinceId", "Status", "Street", "Ward" },
                values: new object[,]
                {
                    { new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), "180", 1200, "Ho Chi Minh City", "Quận 8", 10.73903, 106.67778, "Trường Đại học Công nghệ Sài Gòn", "02838505520", 76, 0, "Cao Lỗ", "Phường 4" },
                    { new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), "262", 2000, "Ho Chi Minh City", "Quận 11", 10.76702, 106.64274, "Công viên Văn hóa Đầm Sen", "02839634963", 76, 0, "Lạc Long Quân", "Phường 5" },
                    { new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), "30", 200, "Ho Chi Minh City", "Tân Phú", 10.801259999999999, 106.61837, "AEON MALL Tân Phú Celadon", "02862887733", 76, 0, "Bờ Bao Tân Thắng", "Sơn Kỳ" },
                    { new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), "59C", 200, "Ho Chi Minh City", "Quận 3", 10.78313, 106.69474, "Đại học Kinh tế TP.HCM (UEH)", "02838295299", 76, 0, "Nguyễn Đình Chiểu", "Phường 6" },
                    { new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), "215", 600, "Ho Chi Minh City", "Lô E2a-7", 10.84135, 106.81103, "Đại học FPT TP.HCM", "02873005588", 76, 0, "D1", "Thành phố Thủ Đức" },
                    { new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), "1058", 600, "Ho Chi Minh City", "Quận 7", 10.73138, 106.70439, "SC VivoCity", "02837761018", 76, 0, "Nguyễn Văn Linh", "Tân Phong" },
                    { new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), "101", 600, "Ho Chi Minh City", "Quận 7", 10.729100000000001, 106.7195, "Crescent Mall", "02854133333", 76, 0, "Tôn Dật Tiên", "Tân Phú" },
                    { new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), "1", 3000, "Ho Chi Minh City", "Bình Tân", 10.74292, 106.61296, "AEON MALL Bình Tân", "01900636922", 76, 0, "17A", "Bình Trị Đông B" },
                    { new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), "215", 600, "Ho Chi Minh City", "Quận 5", 10.756, 106.6669, "Bệnh viện Đại học Y Dược TP.HCM", "02838554269", 76, 0, "Hồng Bàng", "Phường 11" }
                });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 21, 12, 4, 32, 501, DateTimeKind.Utc).AddTicks(9437));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"));

            migrationBuilder.DeleteData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Transaction",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 559, DateTimeKind.Utc).AddTicks(7163),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 501, DateTimeKind.Utc).AddTicks(1788));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Feedback",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 21, 5, 44, 51, 558, DateTimeKind.Utc).AddTicks(4965),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 21, 12, 4, 32, 499, DateTimeKind.Utc).AddTicks(3153));

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "ADM000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "BCEKEjSWuOrMI4PySM2yC0KfZY3AQl9Pp7PZFsuWR8Y=", "+FFsFL6Gml5D3Eg+KFKX6g==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "W0Zvm1gm3X3DjEnzAffwi8z7o7ItrL4X5k8GBNzi29c=", "Ngij/zexPuUy7W78NBhqGw==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "CUS000002",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "bxzDr7urZ8CXq90wjOcclQGAqE6LN+xsbaJ4dVj1T+U=", "0eKK35+da6J7WAIAWYL0BQ==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "OWN000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "wg98cJ+MNK6U1hn+WHGxe1H9/54kKVSTiuF9/lubLOY=", "ted6x+WZjZx3TEp4xBkE8w==" });

            migrationBuilder.UpdateData(
                table: "Account",
                keyColumn: "Id",
                keyValue: "STF000001",
                columns: new[] { "Password", "Salt" },
                values: new object[] { "rnWe//iLOcvybmUlCU4Y1SXn3OUma/6nF+6WwUeI46g=", "/EkUmwE6P8OUblrZoQwrqQ==" });

            migrationBuilder.UpdateData(
                table: "CarPark",
                keyColumn: "Id",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                columns: new[] { "AddressNumber", "AvailableSlotsCount", "District", "Latitude", "Longitude", "Name", "PhoneNumber", "ProvinceId", "Street", "Ward" },
                values: new object[] { "500", 100, "1", 123.0, 123.0, "Test", "0902589751", 2, "Test street", "Test ward" });

            migrationBuilder.UpdateData(
                table: "PriceTable",
                keyColumn: "CarParkId",
                keyValue: new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                column: "LastModified",
                value: new DateTime(2022, 11, 21, 5, 44, 51, 560, DateTimeKind.Utc).AddTicks(2178));
        }
    }
}