using System.ComponentModel.DataAnnotations;

namespace Models;

public class UserInfo
{
    public string Id { get; set; }
    public string FullName { get; set; }

    [Required]
    [RegularExpression("^\\d{10,14}$")]
    public decimal AccountBalance { get; set; }

    public Account Account { get; set; }
}