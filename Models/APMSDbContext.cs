﻿using Microsoft.EntityFrameworkCore;
using Models.Configurations;

namespace Models;

public sealed class APMSDbContext : DbContext
{
    public APMSDbContext(DbContextOptions options) : base(options)
    { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfiguration(new AccountConfiguration());
        builder.ApplyConfiguration(new UserInfoConfiguration());
        builder.ApplyConfiguration(new CarParkConfiguration());
        builder.ApplyConfiguration(new RoleConfiguration());
        builder.ApplyConfiguration(new TicketConfiguration());
        builder.ApplyConfiguration(new FeedbackConfiguration());
        builder.ApplyConfiguration(new TransactionConfiguration());
        builder.ApplyConfiguration(new PriceTableConfiguration());
        builder.ApplyConfiguration(new PriceTableDetailConfiguration());
        builder.ApplyConfiguration(new LicensePlateConfiguration());
        builder.ApplyConfiguration(new ParkingConfiguration());
    }
}