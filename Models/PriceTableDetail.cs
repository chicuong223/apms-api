namespace Models;

public class PriceTableDetail
{
    public int Id { get; set; }
    public TimeSpan FromTime { get; set; }
    public TimeSpan ToTime { get; set; }
    public decimal Fee { get; set; }
    public Guid CarParkId { get; set; }
    public PriceTable PriceTable { get; set; }
}