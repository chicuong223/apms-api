using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class ParkingConfiguration : IEntityTypeConfiguration<Parking>
{
    public void Configure(EntityTypeBuilder<Parking> builder)
    {
        builder.HasKey(p => new { p.AccountId, p.CarParkId });
        builder.HasOne(p => p.CarPark)
            .WithMany()
            .HasForeignKey(p => p.CarParkId)
            .IsRequired();
        builder.HasOne(p => p.Account)
            .WithMany()
            .HasForeignKey(p => p.AccountId)
            .IsRequired();
        builder.Property(p => p.LastVisited)
            .IsRequired();
        builder.HasData(
            new Parking
            {
                CarParkId = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                AccountId = "CUS000001",
                LastVisited = (new DateTime(2022, 11, 11, 19, 35, 25)).ToUniversalTime(),
                UseCount = 100
            },
            new Parking
            {
                CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                AccountId = "CUS000001",
                LastVisited = (new DateTime(2022, 11, 20, 7, 28, 51)).ToUniversalTime(),
                UseCount = 60
            },
            new Parking
            {
                CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                AccountId = "CUS000001",
                LastVisited = (new DateTime(2022, 11, 20, 12, 27, 16)).ToUniversalTime(),
                UseCount = 25
            },
            new Parking
            {
                CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                AccountId = "CUS000001",
                LastVisited = (new DateTime(2022, 11, 21, 08, 48, 27)).ToUniversalTime(),
                UseCount = 1
            },
            new Parking
            {
                CarParkId = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                AccountId = "CUS000003",
                LastVisited = (new DateTime(2022, 11, 20, 12, 27, 16)).ToUniversalTime(),
                UseCount = 100
            },
            new Parking
            {
                CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                AccountId = "CUS000003",
                LastVisited = (new DateTime(2022, 11, 20, 12, 27, 16)).ToUniversalTime(),
                UseCount = 60
            },
            new Parking
            {
                CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                AccountId = "CUS000003",
                LastVisited = (new DateTime(2022, 11, 20, 7, 28, 51)).ToUniversalTime(),
                UseCount = 25
            },
            new Parking
            {
                CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                AccountId = "CUS000003",
                LastVisited = (new DateTime(2022, 11, 23, 14, 35, 25)).ToUniversalTime(),
                UseCount = 1
            }
        );
    }
}