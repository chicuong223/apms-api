using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Utilities.Enums;

namespace Models.Configurations;

public class CarParkConfiguration : IEntityTypeConfiguration<CarPark>
{
    public void Configure(EntityTypeBuilder<CarPark> builder)
    {
        builder.HasKey(lot => lot.Id);
        builder.Property(lot => lot.AddressNumber)
            .HasMaxLength(50)
            .IsRequired();
        builder.Property(lot => lot.Street)
            .HasMaxLength(200)
            .IsRequired();
        builder.Property(lot => lot.Ward)
            .HasMaxLength(200)
            .IsRequired();
        builder.Property(lot => lot.District)
            .HasMaxLength(100)
            .IsRequired();
        builder.Property(lot => lot.City)
            .HasMaxLength(200)
            .IsRequired();
        builder.Property(lot => lot.Status)
            .HasDefaultValue(CarParkStatus.Active)
            .IsRequired();
        builder.HasData(
            new CarPark
            {
                Id = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                District = "Thành phố Thủ Đức",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "25",
                AvailableSlotsCount = 200,
                MaxCapacity = 200,
                Latitude = 10.76534,
                Longitude = 106.75936,
                Name = "Bãi đậu xe 25 Quách Giai",
                PhoneNumber = "0975033288",
                Street = "Quách Giai",
                Status = CarParkStatus.Active,
                Ward = "Thạnh Mỹ Lợi",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                District = "Quận 3",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "59C",
                AvailableSlotsCount = 200,
                MaxCapacity = 200,
                Latitude = 10.78313,
                Longitude = 106.69474,
                Name = "Đại học Kinh tế TP.HCM (UEH)",
                PhoneNumber = "02838295299",
                Street = "Nguyễn Đình Chiểu",
                Status = CarParkStatus.Active,
                Ward = "Phường 6",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                District = "Tân Phú",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "30",
                AvailableSlotsCount = 200,
                MaxCapacity = 200,
                Latitude = 10.80126,
                Longitude = 106.61837,
                Name = "AEON MALL Tân Phú Celadon",
                PhoneNumber = "02862887733",
                Street = "Bờ Bao Tân Thắng",
                Status = CarParkStatus.Active,
                Ward = "Sơn Kỳ",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                District = "Quận 7",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "101",
                AvailableSlotsCount = 600,
                MaxCapacity = 600,
                Latitude = 10.72910,
                Longitude = 106.71950,
                Name = "Crescent Mall",
                PhoneNumber = "02854133333",
                Street = "Tôn Dật Tiên",
                Status = CarParkStatus.Active,
                Ward = "Tân Phú",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                District = "Quận 7",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "1058",
                AvailableSlotsCount = 600,
                MaxCapacity = 600,
                Latitude = 10.73138,
                Longitude = 106.70439,
                Name = "SC VivoCity",
                PhoneNumber = "02837791018",
                Street = "Nguyễn Văn Linh",
                Status = CarParkStatus.Active,
                Ward = "Tân Phong",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                District = "Quận 5",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "215",
                AvailableSlotsCount = 600,
                MaxCapacity = 600,
                Latitude = 10.75600,
                Longitude = 106.66690,
                Name = "Bệnh viện Đại học Y Dược TP.HCM",
                PhoneNumber = "02838554269",
                Street = "Hồng Bàng",
                Status = CarParkStatus.Active,
                Ward = "Phường 11",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                District = "Quận 11",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "262",
                AvailableSlotsCount = 2000,
                MaxCapacity = 2000,
                Latitude = 10.79702,
                Longitude = 106.64274,
                Name = "Công viên Văn hóa Đầm Sen",
                PhoneNumber = "02839634963",
                Street = "Lạc Long Quân",
                Status = CarParkStatus.Active,
                Ward = "Phường 5",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                District = "Quận 8",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "180",
                AvailableSlotsCount = 1200,
                MaxCapacity = 1200,
                Latitude = 10.73903,
                Longitude = 106.67778,
                Name = "Trường Đại học Công nghệ Sài Gòn",
                PhoneNumber = "02838505520",
                Street = "Cao Lỗ",
                Status = CarParkStatus.Active,
                Ward = "Phường 4",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                District = "Bình Tân",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "1",
                AvailableSlotsCount = 3000,
                MaxCapacity = 3000,
                Latitude = 10.74292,
                Longitude = 106.61296,
                Name = "AEON MALL Bình Tân",
                PhoneNumber = "01900636922",
                Street = "17A",
                Status = CarParkStatus.Active,
                Ward = "Bình Trị Đông B",
                ProvinceId = 79
            },
            new CarPark
            {
                Id = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                District = "Thành phố Thủ Đức",
                City = "Thành phố Hồ Chí Minh",
                AddressNumber = "Lô E2a-7",
                AvailableSlotsCount = 600,
                Latitude = 10.84135,
                Longitude = 106.81103,
                Name = "Đại học FPT TP.HCM",
                PhoneNumber = "02873005588",
                Street = "D1",
                Status = CarParkStatus.Active,
                Ward = "Long Thạnh Mỹ",
                ProvinceId = 79,
                MaxCapacity = 600
            },
            new CarPark
            {
                Id = new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"),
                District = "Thuận An",
                City = "Bình Dương",
                AddressNumber = "333",
                AvailableSlotsCount = 3000,
                MaxCapacity = 3000,
                Latitude = 10.94816,
                Longitude = 106.70626,
                Name = "City Tower Bình Dương",
                PhoneNumber = "0915592179",
                Street = "Đại lộ Bình Dương",
                Status = CarParkStatus.Removed,
                Ward = "",
                ProvinceId = 74
            });
    }
}