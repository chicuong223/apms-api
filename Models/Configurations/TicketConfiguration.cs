using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class TicketConfiguration : IEntityTypeConfiguration<Ticket>
{
    public void Configure(EntityTypeBuilder<Ticket> builder)
    {
        builder.HasKey(ticket => ticket.Id);
        builder.Property(ticket => ticket.StartTime);
        // builder.Property(ticket => ticket.FeePerHour)
        //     .HasColumnType("numeric");
        builder.Property(ticket => ticket.TotalFee)
            .HasColumnType("numeric");
        builder.Property(ticket => ticket.PicInUrl)
            .HasMaxLength(256);
        builder.Property(ticket => ticket.PicOutUrl)
            .HasMaxLength(256);
        builder.HasOne(ticket => ticket.CarPark)
            .WithMany()
            .HasForeignKey(ticket => ticket.CarParkId)
            .IsRequired();
        builder.HasOne(ticket => ticket.Account)
            .WithMany()
            .HasForeignKey(ticket => ticket.AccountId)
            .IsRequired();
        builder.HasIndex(ticket => new { ticket.StartTime, ticket.PlateNumber, ticket.Status });
        builder.Property(ticket => ticket.ReservationFee)
            .HasColumnType("numeric");
        builder.HasOne(ticket => ticket.Canceller)
            .WithMany()
            .HasForeignKey(ticket => ticket.CancellerAccountId);
        builder.Property(ticket => ticket.PlateNumber)
            .IsRequired();
        builder.Property(ticket => ticket.OverdueFee)
            .HasColumnType("numeric");
    }
}