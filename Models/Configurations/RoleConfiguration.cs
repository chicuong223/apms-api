using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class RoleConfiguration : IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.HasKey(role => role.Id);
        builder.HasIndex(role => role.Title)
            .IsUnique();
        builder.Property(role => role.Title)
            .IsRequired();
        builder.HasData(
            new Role { Id = 1, Title = "Admin" },
            new Role { Id = 2, Title = "Owner" },
            new Role { Id = 3, Title = "Staff" },
            new Role { Id = 4, Title = "Customer" }
        );
    }
}