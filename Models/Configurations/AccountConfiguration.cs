using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Utilities;

namespace Models.Configurations;

public class AccountConfiguration : IEntityTypeConfiguration<Account>
{
    public void Configure(EntityTypeBuilder<Account> builder)
    {
        Tuple<string, string> adminPassword = AccountUtils.CreateHashedPassword("apmsadmin");
        Tuple<string, string> ownerPassword = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> staffPassword = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> ownerPassword2 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword3 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword4 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword5 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> staffPassword2 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword3 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword4 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword5 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> ownerPassword6 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword7 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword8 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword9 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> ownerPassword10 = AccountUtils.CreateHashedPassword("apmsowner");
        Tuple<string, string> staffPassword6 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword7 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword8 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword9 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> staffPassword10 = AccountUtils.CreateHashedPassword("apmsstaff");
        Tuple<string, string> customerPassword = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword1 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword2 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword4 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword5 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword6 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword7 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword8 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> customerPassword3 = AccountUtils.CreateHashedPassword("123456789");
        Tuple<string, string> testuserPassword = AccountUtils.CreateHashedPassword("123456789");
        builder.HasKey(u => u.Id);
        builder.Property(u => u.Password)
            .HasMaxLength(128)
            .IsRequired();
        builder.Property(u => u.Active)
            .HasDefaultValue(true);
        builder.HasIndex(u => u.Salt)
            .IsUnique();
        builder.Property(u => u.Salt)
            .IsRequired()
            .HasMaxLength(128);
        builder.HasOne(u => u.Role)
            .WithMany()
            .HasForeignKey(u => u.RoleId)
            .IsRequired();
        // builder.HasIndex(u => u.PhoneNumber)
        //     .IsUnique();
        builder.Property(u => u.PhoneNumber)
            .HasMaxLength(14)
            .IsRequired();
        builder.HasOne(u => u.CarPark)
            .WithMany(c => c.Accounts)
            .HasForeignKey(u => u.CarParkId)
            .IsRequired(false);
        builder.HasData(
            new Account
            {
                Id = "ADM000001",
                Active = true,
                Password = adminPassword.Item1,
                Salt = adminPassword.Item2,
                PhoneNumber = "0903356123",
                RoleId = AppConstants.AdminRoleId
            },
            new Account
            {
                Id = "OWN000001",
                Active = true,
                Password = ownerPassword.Item1,
                Salt = ownerPassword.Item2,
                PhoneNumber = "0902879231",
                RoleId = AppConstants.OwnerRoleId,
                CarParkId = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                CanEdit = true
            },
            new Account
            {
                Id = "STF000001",
                Active = true,
                Password = staffPassword.Item1,
                Salt = staffPassword.Item2,
                PhoneNumber = "0377162315",
                RoleId = AppConstants.StaffRoleId,
                CarParkId = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
            },
            new Account
            {
                Id = "CUS000010",
                Active = true,
                Password = customerPassword2.Item1,
                Salt = customerPassword2.Item2,
                PhoneNumber = "0941976105",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000004",
                Active = true,
                Password = customerPassword4.Item1,
                Salt = customerPassword4.Item2,
                PhoneNumber = "0937632595",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000005",
                Active = true,
                Password = customerPassword5.Item1,
                Salt = customerPassword5.Item2,
                PhoneNumber = "0909254787",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000006",
                Active = true,
                Password = customerPassword6.Item1,
                Salt = customerPassword6.Item2,
                PhoneNumber = "0902345590",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000007",
                Active = true,
                Password = customerPassword7.Item1,
                Salt = customerPassword7.Item2,
                PhoneNumber = "0123456168",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000008",
                Active = true,
                Password = customerPassword8.Item1,
                Salt = customerPassword8.Item2,
                PhoneNumber = "0772776677",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000009",
                Active = true,
                Password = customerPassword1.Item1,
                Salt = customerPassword1.Item2,
                PhoneNumber = "0932852653",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000001",
                Active = true,
                Password = customerPassword.Item1,
                Salt = customerPassword.Item2,
                PhoneNumber = "0932781745",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000002",
                Active = true,
                Password = testuserPassword.Item1,
                Salt = testuserPassword.Item2,
                PhoneNumber = "0949936732",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "CUS000003",
                Active = true,
                Password = customerPassword3.Item1,
                Salt = customerPassword3.Item2,
                PhoneNumber = "0908436393",
                RoleId = AppConstants.CustomerRoleId
            },
            new Account
            {
                Id = "OWN000002",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword2.Item1,
                Salt = ownerPassword2.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0727589658",
                CarParkId = new Guid("819fb006-8468-4d70-843b-4cfa5035c24a")
            },
            new Account
            {
                Id = "OWN000003",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword3.Item1,
                Salt = ownerPassword3.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0902555662",
                CarParkId = new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f")
            },
            new Account
            {
                Id = "OWN000004",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword4.Item1,
                Salt = ownerPassword4.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0125666879",
                CarParkId = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a")
            },
            new Account
            {
                Id = "OWN000005",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword5.Item1,
                Salt = ownerPassword5.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0903650750",
                CarParkId = new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2")
            },
            new Account
            {
                Id = "OWN000006",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword6.Item1,
                Salt = ownerPassword6.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0905125685",
                CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef")
            },
            new Account
            {
                Id = "OWN000007",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword7.Item1,
                Salt = ownerPassword7.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0909159259",
                CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc")
            },
            new Account
            {
                Id = "OWN000008",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword8.Item1,
                Salt = ownerPassword8.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0372658456",
                CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1")
            },
            new Account
            {
                Id = "OWN000009",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword9.Item1,
                Salt = ownerPassword9.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0937546587",
                CarParkId = new Guid("f107d568-d9b0-4848-badc-bc2f872d580c")
            },
            new Account
            {
                Id = "OWN000010",
                Active = true,
                CanEdit = true,
                CanEditCarPark = true,
                Password = ownerPassword10.Item1,
                Salt = ownerPassword10.Item2,
                RoleId = AppConstants.OwnerRoleId,
                PhoneNumber = "0902357159",
                CarParkId = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b")
            },
            new Account
            {
                Id = "STF000002",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword2.Item1,
                Salt = staffPassword2.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0909578951",
                CarParkId = new Guid("819fb006-8468-4d70-843b-4cfa5035c24a")
            },
            new Account
            {
                Id = "STF000003",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword3.Item1,
                Salt = staffPassword3.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0902348750",
                CarParkId = new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f")
            },
            new Account
            {
                Id = "STF000004",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword4.Item1,
                Salt = staffPassword4.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0902595590",
                CarParkId = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a")
            },
            new Account
            {
                Id = "STF000005",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword5.Item1,
                Salt = staffPassword5.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0906546873",
                CarParkId = new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2")
            },
            new Account
            {
                Id = "STF000006",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword6.Item1,
                Salt = staffPassword6.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0967678123",
                CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef")
            },
            new Account
            {
                Id = "STF000007",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword7.Item1,
                Salt = staffPassword7.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0965666235",
                CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc")
            },
            new Account
            {
                Id = "STF000008",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword8.Item1,
                Salt = staffPassword8.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0677123951",
                CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1")
            },
            new Account
            {
                Id = "STF000009",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword9.Item1,
                Salt = staffPassword9.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0909654875",
                CarParkId = new Guid("f107d568-d9b0-4848-badc-bc2f872d580c")
            },
            new Account
            {
                Id = "STF000010",
                Active = true,
                CanEdit = false,
                CanEditCarPark = false,
                Password = staffPassword10.Item1,
                Salt = staffPassword10.Item2,
                RoleId = AppConstants.StaffRoleId,
                PhoneNumber = "0222678678",
                CarParkId = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b")
            }
            );
    }
}