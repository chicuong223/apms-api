using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
{
    public void Configure(EntityTypeBuilder<Transaction> builder)
    {
        builder.HasKey(t => t.Id);
        builder.Property(t => t.Amount)
            .IsRequired()
            .HasColumnType("numeric");
        builder.Property(t => t.Time)
            .HasDefaultValue(DateTime.UtcNow)
            .IsRequired();
        builder.HasOne(t => t.Account)
            .WithMany()
            .HasForeignKey(t => t.AccountId)
            .IsRequired();
        builder.HasOne(t => t.Ticket)
            .WithMany()
            .HasForeignKey(t => t.TicketId);
    }
}