using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class UserInfoConfiguration : IEntityTypeConfiguration<UserInfo>
{
    public void Configure(EntityTypeBuilder<UserInfo> builder)
    {
        builder.HasKey(info => info.Id);
        builder.Property(info => info.FullName)
            .HasMaxLength(250)
            .IsRequired();
        builder.Property(info => info.AccountBalance)
            .HasColumnType("numeric");
        builder.HasOne(info => info.Account)
            .WithOne(acc => acc.Info)
            .HasForeignKey<UserInfo>(info => info.Id)
            .IsRequired();
        builder.HasData(
            new UserInfo
            {
                Id = "CUS000001",
                // PhoneNumber = "0932781745",
                FullName = "Ho Huu Phat",
                AccountBalance = 1_000_000
            },
            new UserInfo
            {
                Id = "CUS000002",
                FullName = "Khuc Ngoc Thai",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000003",
                FullName = "Tăng Chí Cường",
                AccountBalance = 500_000
            },

            new UserInfo
            {
                Id = "CUS000004",
                FullName = "Huỳnh Mẫn Huy",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000005",
                FullName = "Châu Vĩnh Cơ",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000006",
                FullName = "Triệu Văn Vĩnh",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000007",
                FullName = "Lê Xuân Mai",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000008",
                FullName = "Nguyễn Thị Ánh Nguyệt",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000009",
                FullName = "Hồ Võ Kim Ngân",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            },
            new UserInfo
            {
                Id = "CUS000010",
                FullName = "Lương Vệ Thành",
                // PhoneNumber = "0949936732",
                AccountBalance = 500_000
            }
        );
    }
}