using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class PriceTableConfiguration : IEntityTypeConfiguration<PriceTable>
{
    public void Configure(EntityTypeBuilder<PriceTable> builder)
    {
        builder.HasKey(table => table.CarParkId);
        builder.HasOne(table => table.CarPark)
            .WithOne(park => park.PriceTable)
            .HasForeignKey<PriceTable>(table => table.CarParkId)
            .IsRequired();
        builder.HasData(
            new PriceTable { CarParkId = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 30 },
            new PriceTable { CarParkId = new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 50 },
            new PriceTable { CarParkId = new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 30 },
            new PriceTable { CarParkId = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 20 },
            new PriceTable { CarParkId = new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 10 },
            new PriceTable { CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 5 },
            new PriceTable { CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 50 },
            new PriceTable { CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 30 },
            new PriceTable { CarParkId = new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 10 },
            new PriceTable { CarParkId = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 20 },
            new PriceTable { CarParkId = new Guid("de075256-acf9-4679-8e8d-d60ab9cde12e"), LastModified = DateTime.UtcNow, ReservationFeePercentage = 30, FeePerHour = 35000 }
        );
    }
}