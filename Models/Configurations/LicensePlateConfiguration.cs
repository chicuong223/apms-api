using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class LicensePlateConfiguration : IEntityTypeConfiguration<LicensePlate>
{
    public void Configure(EntityTypeBuilder<LicensePlate> builder)
    {
        builder.HasKey(plate => new { plate.PlateNumber, plate.AccountId });
        builder.HasOne(plate => plate.Account)
            .WithMany()
            .HasForeignKey(plate => plate.AccountId)
            .IsRequired();
        builder.HasData(
            new LicensePlate { PlateNumber = "51H-68329", AccountId = "cus000001" },
            new LicensePlate { PlateNumber = "51H-68329", AccountId = "cus000002" },
            new LicensePlate { PlateNumber = "51X-5740", AccountId = "cus000001" },
            new LicensePlate { PlateNumber = "51X-87634", AccountId = "cus000002" }
        );
    }
}