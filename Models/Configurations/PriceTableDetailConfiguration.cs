using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Configurations;

public class PriceTableDetailConfiguration : IEntityTypeConfiguration<PriceTableDetail>
{
    public void Configure(EntityTypeBuilder<PriceTableDetail> builder)
    {
        builder.HasKey(d => d.Id);
        builder.HasOne(d => d.PriceTable)
            .WithMany(table => table.Details)
            .HasForeignKey(d => d.CarParkId)
            .IsRequired();
        builder.Property(d => d.Fee)
            .HasColumnType("numeric")
            .IsRequired();
        builder.Property(d => d.FromTime)
            .IsRequired();
        builder.Property(d => d.ToTime)
            .IsRequired();
        builder.HasData(
            new PriceTableDetail
            {
                Id = 1,
                CarParkId = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 30000
            },
            new PriceTableDetail
            {
                Id = 2,
                CarParkId = new Guid("5a181b77-3ff6-4686-8520-a83b06a34454"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 35000
            },
            new PriceTableDetail
            {
                Id = 3,
                CarParkId = new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 35000
            },
            new PriceTableDetail
            {
                Id = 4,
                CarParkId = new Guid("819fb006-8468-4d70-843b-4cfa5035c24a"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 60000
            },
            new PriceTableDetail
            {
                Id = 5,
                CarParkId = new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 25000
            },
            new PriceTableDetail
            {
                Id = 6,
                CarParkId = new Guid("7d5f6629-0cf6-480c-a6a4-1223e5a2919f"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 35000
            },
            new PriceTableDetail
            {
                Id = 7,
                CarParkId = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 30000
            },
            new PriceTableDetail
            {
                Id = 8,
                CarParkId = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(23, 59, 0),
                Fee = 50000
            },
            new PriceTableDetail
            {
                Id = 9,
                CarParkId = new Guid("d9fc5ad5-8346-4b51-b5e2-110d260f463a"),
                FromTime = new TimeSpan(0, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 60000
            },
            new PriceTableDetail
            {
                Id = 10,
                CarParkId = new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 30000
            },
            new PriceTableDetail
            {
                Id = 11,
                CarParkId = new Guid("bd38df3b-71f2-4419-a495-22a1eb410df2"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 35000
            },
            new PriceTableDetail
            {
                Id = 12,
                CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 12000
            },
            new PriceTableDetail
            {
                Id = 13,
                CarParkId = new Guid("f29695c9-f838-4f66-bf98-ef64e7e96aef"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 20000
            },
            new PriceTableDetail
            {
                Id = 14,
                CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 20000
            },
            new PriceTableDetail
            {
                Id = 15,
                CarParkId = new Guid("6c3af684-254e-4861-a1a6-89325ee8e5bc"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 40000
            },
            new PriceTableDetail
            {
                Id = 16,
                CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 30000
            },
            new PriceTableDetail
            {
                Id = 17,
                CarParkId = new Guid("4a04b950-1788-4d2b-8d62-28157757b4a1"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 35000
            },
            new PriceTableDetail
            {
                Id = 18,
                CarParkId = new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(17, 59, 00),
                Fee = 50000
            },
            new PriceTableDetail
            {
                Id = 19,
                CarParkId = new Guid("f107d568-d9b0-4848-badc-bc2f872d580c"),
                FromTime = new TimeSpan(18, 0, 0),
                ToTime = new TimeSpan(5, 59, 0),
                Fee = 60000
            },
            new PriceTableDetail
            {
                Id = 20,
                CarParkId = new Guid("9a93494c-6469-4b23-a0e7-9db72cae597b"),
                FromTime = new TimeSpan(6, 0, 0),
                ToTime = new TimeSpan(5, 59, 00),
                Fee = 30000
            }
        );
    }
}