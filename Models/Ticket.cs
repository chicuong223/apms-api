using Utilities.Enums;

namespace Models;

public class Ticket
{
    public Guid Id { get; set; }

    // public decimal FeePerHour { get; set; }
    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }
    public decimal TotalFee { get; set; }
    public string PicInUrl { get; set; }
    public string PicOutUrl { get; set; }
    public Guid CarParkId { get; set; }
    public string PlateNumber { get; set; }
    public decimal ReservationFee { get; set; }
    public decimal OverdueFee { get; set; }
    public DateTime? BookTime { get; set; }
    public DateTime? ArriveTime { get; set; }
    public string CancellerAccountId { get; set; }
    public string AccountId { get; set; }
    public string PriceTable { get; set; }
    public CarPark CarPark { get; set; }
    public Account Canceller { get; set; }
    public Account Account { get; set; }
    public TicketStatus Status { get; set; }
}