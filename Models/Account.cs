﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Account
    {
        [RegularExpression("^([A-Z]{3})(\\d{6})$")]
        public string Id { get; set; }

        public string Password { get; set; }
        public string Salt { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }

        [RegularExpression("^\\d{10,14}$")]
        public string PhoneNumber { get; set; }

        public Guid? CarParkId { get; set; }
        public CarPark CarPark { get; set; }
        public UserInfo Info { get; set; }
        public bool? Active { get; set; }
        public bool CanEdit { get; set; }
        public bool CanEditCarPark { get; set; }
    }
}