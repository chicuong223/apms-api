using System.ComponentModel.DataAnnotations;
using Utilities.Enums;

namespace Models;

public class CarPark
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string AddressNumber { get; set; }
    public string Street { get; set; }
    public string City { get; set; }
    public string District { get; set; }
    public string Ward { get; set; }
    [RegularExpression("^[0-9]{10,14}$")] public string PhoneNumber { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public int ProvinceId { get; set; }

    [Range(0, int.MaxValue)]
    public int AvailableSlotsCount { get; set; }

    [Range(0, int.MaxValue)]
    public int MaxCapacity { get; set; }

    public PriceTable PriceTable { get; set; }
    public CarParkStatus? Status { get; set; }
    public ICollection<Account> Accounts { get; set; }
}