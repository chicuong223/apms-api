namespace Models;

public class Feedback
{
    public Guid Id { get; set; }
    public string Description { get; set; }
    public DateTime? Time { get; set; }
    public string AccountId { get; set; }
    public Account Account { get; set; }
}