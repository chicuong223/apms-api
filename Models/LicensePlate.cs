namespace Models;

public class LicensePlate
{
    public string PlateNumber { get; set; }
    public string AccountId { get; set; }
    public int UsageCount { get; set; }
    public Account Account { get; set; }
}