namespace Models;

public class Parking
{
    public string AccountId { get; set; }
    public Guid CarParkId { get; set; }
    public int UseCount { get; set; }
    public DateTime LastVisited { get; set; }
    public Account Account { get; set; }
    public CarPark CarPark { get; set; }
}