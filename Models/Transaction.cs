using Utilities.Enums;

namespace Models;

public class Transaction
{
    public Guid Id { get; set; }
    public decimal Amount { get; set; }
    public DateTime Time { get; set; }

    // public bool Success { get; set; }
    public string AccountId { get; set; }

    public TransactionType TransactionType { get; set; }
    public Guid? TicketId { get; set; }
    public Account Account { get; set; }
    public Ticket Ticket { get; set; }
}