using System.ComponentModel.DataAnnotations;

namespace Models;

public class PriceTable
{
    public Guid CarParkId { get; set; }
    public DateTime LastModified { get; set; }

    [Range(0, 100)]
    public decimal ReservationFeePercentage { get; set; }

    [Range(0, double.MaxValue)]
    public decimal? FeePerHour { get; set; }

    public CarPark CarPark { get; set; }
    public ICollection<PriceTableDetail> Details { get; set; }
}